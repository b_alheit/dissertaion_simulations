      Program Eigenvalue
! finding the eigenvalues of a complex matrix using LAPACK
      Implicit none
! declarations, notice double precision
      real*16 A(3,3), d(3,3), Z(3,3), b(3), c(3), DUMMY(1,1), WORK(6)
      integer i, j, ok
! define matrix A
      d(1,1)=3.1
      d(1,2)=1.3
      d(1,3)=-5.7
      d(2,1)=1.0
      d(2,2)=-6.9
      d(2,3)=5.8
      d(3,1)=3.4
      d(3,2)=7.2
      d(3,3)=-8.8

      print *, "d"
      print *, d
      print *, ""


      do i=1,3
        do j=1,3
          A(i,j) = d(i,j) + d(j,i)
        enddo
      enddo
      print *, "a"
      print *, A
      print *, ""
!
! find the solution using the LAPACK routine ZGEEV
      call ZGEEV('N', 'N', 3, A, 3, b, DUMMY, 1, DUMMY, 1, WORK, 6, &
       WORK, ok)

       !call DGEEV('N', 'V', 3, A, 3, b, c, DUMMY, 1, Z, 3, WORK, 6, &
      !WORK, ok)
!
! parameters in the order as they appear in the function call
!    no left eigenvectors, no right eigenvectors, order of input matrix A,
!    input matrix A, leading dimension of A, array for eigenvalues,
!    array for left eigenvalue, leading dimension of DUMMY,
!    array for right eigenvalues, leading dimension of DUMMY,
!    workspace array dim>=2*order of A, dimension of WORK
!    workspace array dim=2*order of A, return value
!
! output of eigenvalues

      if (ok .eq. 0) then
        do i=1, 3
          write (*,*) "b"
          write(*,*) b(i)
        enddo
      else
      write (*,*) "An error occured"
      endif
      write (*,*) ""
      write (*,*) "Z"
      print *, Z
      end
