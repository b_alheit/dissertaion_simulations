Program Hello
    !   !     !Print *, "Hello World!"
    Implicit None
    real :: one = 1.00
    integer, PARAMETER :: NTENS = 6
    real, DIMENSION(1 : NTENS) :: DDSDDE(NTENS, NTENS), STRESS(NTENS)
    real, DIMENSION(1 : NTENS) :: DSTRAN(NTENS)
    !   !     !PARAMETER (ONE=1.0D0, TWO=2.0D0)
    !   !     !do i=1,NTENS
    !   !     !  do j=1,NTENS
    !   !     !    DDSDDE(i,j)=real(i) + real(j)
    !   !     !  ENDDO
    !   !     !ENDDO
    !   !
    integer :: i, j
    real :: ALAMBDA, BLAMBDA, CLAMBDA
    real, PARAMETER :: TWO = 2.000
    !   !     !integer :: I, J

    real :: E = 210e9
    real :: ANU = 0.33

    DO I = 1, NTENS
        DSTRAN(I) = 1.0
        STRESS(I) = 0.0
    ENDDO

    ALAMBDA = E / (1.0 + ANU) / (1.0 - 2.0 * ANU)
    BLAMBDA = (1.0 - ANU)
    CLAMBDA = (1.0 - 2.0 * ANU)
    DO I = 1, NTENS
        DO J = 1, NTENS
            DDSDDE(I, J) = 0.00
        ENDDO
    ENDDO
    DDSDDE(1, 1) = (ALAMBDA * BLAMBDA) + &
            1.0
    DDSDDE(2, 2) = (ALAMBDA * BLAMBDA)
    DDSDDE(3, 3) = (ALAMBDA * BLAMBDA)
    DDSDDE(4, 4) = (ALAMBDA * CLAMBDA)
    DDSDDE(5, 5) = (ALAMBDA * CLAMBDA)
    DDSDDE(6, 6) = (ALAMBDA * CLAMBDA)
    DDSDDE(1, 2) = (ALAMBDA * ANU)
    DDSDDE(1, 3) = (ALAMBDA * ANU)
    DDSDDE(2, 3) = (ALAMBDA * ANU)
    DDSDDE(2, 1) = (ALAMBDA * ANU)
    DDSDDE(3, 1) = (ALAMBDA * ANU)
    DDSDDE(3, 2) = (ALAMBDA * ANU)

    DO I = 1, NTENS
        DO J = 1, NTENS
            STRESS(I) = STRESS(I) + DDSDDE(I, J) * DSTRAN(J)
        ENDDO
    ENDDO

    Print *, one
    Print *, "_____________________"
    Print *, DDSDDE
    Print *, "_____________________"
    Print *, DSTRAN
    Print *, "_____________________"
    Print *, STRESS
End Program Hello