import numpy as np

def hi(*arg):
    print(arg)

# hi(1, 2)
a = [1, 2, 3]
print(*a)
# F = np.arange(0, 9).reshape([3,3])
F = np.arange(0, 9)
print(2**F)
a = a + F.tolist()
print(a)
a = np.arange(1, 2, 0.2)
# print(a)
print(F)
print(F.flatten())
print(1/2)
# print(F[[1, 4, 5]])
# print(np.shape(F)[-1])
# print(np.array([F, F]))
# print(np)