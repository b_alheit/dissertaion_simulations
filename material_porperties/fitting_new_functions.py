from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
import new_sigma_functions as nf


data = np.genfromtxt('all_soft.dat', delimiter=',')
# print(data)
# data = data[data[:, 0] > 80]
# data = data[data[:, 0] < 10]
data = data[(data[:, 0] < 10) | (data[:, 0] > 80)]
# data = data[(data[:, 0] < 80) & (data[:, 0] > 70)]
# data = data[(data[:, 0] < 20) & (data[:, 0] > 10)]

x_data = (data[:, 1], data[:, 0])
y_data = data[:, 2]
print("x data")
print(x_data)
print('y_data')
print(y_data)
mu1 = 2.91138167e+04
mu2 = 2.69054344e+04
mu3 = 0
alpha1 = 1.64892495e+02
alpha2 = 1.47182383e+00
alpha3 = 0
# # mu1 = 0
# mu2 = 0
# mu3 = 0
# # alpha1 = 0
# alpha2 = 0
# alpha3 = 0
fit_func = nf.find_stress_from_array_all2
k1 = 2.07948937e+07
k2 = 3.96933969e+02
# p0 = (k1, k2, mu1, mu2, mu3, alpha1, alpha2, alpha3)
p0 = (k1, k2, mu1, mu2, alpha1, alpha2)
# p0 = (mu1, alpha1)
# p0 = (k1, k2, mu1, alpha1)
# p0 = (mu2, alpha2)
# p0 = (mu1, mu2, alpha1, alpha2)
# p0 = [1.99999720e+05, 3.99993293e+03, 9.99990116e+00, 9.99998031e+01,
#  2.00000859e+02, 8.00002345e+04, 8.00001065e+04, 6.00010457e+03]
popt = p0

# popt, pconv = curve_fit(fit_func, xdata=x_data, ydata=y_data, p0=p0, bounds=([0, -np.inf], [np.inf, np.inf]))
# popt, pconv = curve_fit(fit_func, xdata=x_data, ydata=y_data, p0=p0, bounds=([0, 0], [np.inf, np.inf]))
popt, pconv = curve_fit(fit_func, xdata=x_data, ydata=y_data, p0=p0, bounds=([0] * len(p0), [np.inf] * len(p0)))
print('popt')
print(popt)
print('pconv')
print(pconv)
save_array = np.array(popt)
np.savetxt('new_properties.txt', save_array, delimiter=',')

N = fit_func(x_data, *popt)


plt.plot(x_data[0], y_data, linewidth=0, marker='s', mfc='none', label='Data')
plt.plot(x_data[0], N, label='model')
plt.grid()
plt.legend()
plt.show()
