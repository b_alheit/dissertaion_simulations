import numpy as np
import functions as fn
import scipy.optimize as op


def sigma_bar_aniso_new(F, a0, k1, k2, k3, k4):
    J = np.linalg.det(F)
    a_bar = np.matmul(F, a0) * J ** (-1/3)
    b_bar = np.matmul(F, F.T) * J ** (-2/3)
    C_bar = np.matmul(F.T, F) * J ** (-2/3)
    I4_bar = np.matmul(a0.T, np.matmul(C_bar, a0))
    I5_bar = np.matmul(a0.T, np.matmul(C_bar, np.matmul(C_bar, a0)))

    tens1 = np.outer(a_bar, a_bar) - 1/3 * I4_bar * np.eye(3, 3)
    tens2 = np.outer(a_bar, np.matmul(b_bar, a_bar)) + np.outer(np.matmul(b_bar, a_bar), a_bar) - 2/3 * I5_bar * np.eye(3, 3)

    sigma_bar = (2.0/J) * ((k1*(I4_bar - 1)*np.e**(k2*(I4_bar - 1)**2) - 2*k3*I4_bar*np.e**(k4*(I5_bar - I4_bar**2))) * tens1
                           + k3*np.e**(k4*(I5_bar - I4_bar**2)) * tens2)

    return sigma_bar

def sigma_bar_aniso_old(F, a0, k1, k2):
    J = np.linalg.det(F)
    a_bar = np.matmul(F, a0) * J ** (-1 / 3)
    # b_bar = np.matmul(F, F.T) * J ** (-2 / 3)
    C_bar = np.matmul(F.T, F) * J ** (-2 / 3)
    I4_bar = np.matmul(a0.T, np.matmul(C_bar, a0))
    # I5_bar = np.matmul(a0.T, np.matmul(C_bar, np.matmul(C_bar, a0)))
    #
    # tens1 = np.outer(a_bar, a_bar) - 1 / 3 * I4_bar * np.eye(3, 3)
    sca = (2.0 / J) * (k1 * (I4_bar - 1) * np.e ** (k2 * (I4_bar - 1) ** 2))
    tens = np.outer(a_bar, a_bar) - 1 / 3 * I4_bar * np.eye(3, 3)
    sigma_bar = (2.0 / J) * (k1 * (I4_bar - 1) * np.e ** (k2 * (I4_bar - 1) ** 2)) * (np.outer(a_bar, a_bar) - 1 / 3 * I4_bar * np.eye(3, 3))

    return sigma_bar


def sigma_bar_iso(F, mus, alphas):
    J = np.linalg.det(F)
    b_bar = np.matmul(F, F.T) * J ** (-2/3)
    lambdas_bars, ns = np.linalg.eig(b_bar)
    lambdas_bars = lambdas_bars ** 0.5

    sigma_bar = np.zeros([3, 3])
    lamba_sums = np.outer(lambdas_bars, np.ones(np.alen(mus)))
    lamba_sums = -1/3 * np.sum(lamba_sums**alphas, axis=0)

    for i in range(3):
        coeff = np.sum(mus * (lambdas_bars[i] ** alphas + lamba_sums))
        sigma_bar += coeff * np.outer(ns[i], ns[i])

    # sig_test = np.zeros([3, 3])
    # N = np.alen(alphas)
    # for i in range(3):
    #     for


    sigma_bar /= J
    return sigma_bar


def sigma_vol(F, K):
    J = np.linalg.det(F)
    sigma = K * (J - 1) * np.eye(3, 3)
    return sigma


def sigma(F, a0, k1, k2, k3, k4, K, mus, alphas):
    iso = sigma_bar_iso(F, mus, alphas)
    aniso = sigma_bar_aniso_new(F, a0, k1, k2, k3, k4)
    vol = sigma_vol(F, K)
    # print(aniso)

    result = iso + aniso + vol
    return result


def sigma_hgo(F, a0, k1, k2, K, mus, alphas):
    iso = sigma_bar_iso(F, mus, alphas)
    aniso = sigma_bar_aniso_old(F, a0, k1, k2)
    vol = sigma_vol(F, K)

    result = iso + aniso + vol
    return result


def sigma_hgo_parallel(lambda_1, a0, k1, k2, mus, alphas):

    # F = np.zeros([3, 3])
    # F[0, 0] = lambda_1
    # F[1, 1] = lambda_1**-0.5
    # F[2, 2] = lambda_1**-0.5

    # ##################
    # # way 1
    # ##################
    # J = np.linalg.det(F)
    # iso = sigma_bar_iso(F, mus, alphas)
    # iso -= iso[2, 2] * np.eye(3, 3)
    # aniso = sigma_bar_aniso_old(F, a0, k1, k2)
    # aniso -= aniso[2, 2] * np.eye(3, 3)
    # sigma_mat = iso + aniso
    # sigma_mat -= sigma_mat[2, 2] * np.eye(3, 3)
    # sigma_11 = sigma_mat[0, 0]

    ###################
    # Way 2
    ###################
    # sigma_11_iso = np.sum(mus * (lambda_1 ** alphas - lambda_1 ** (-alphas/2.)))
    # sigma_11_aniso = 2 * k1 * (lambda_1**2 - 1) * np.e ** (k2 * (lambda_1**2 - 1) ** 2)
    sigma_11 = np.sum(mus * (lambda_1 ** alphas - lambda_1 ** (-alphas/2.))) + 2 * k1 * (lambda_1**2 - 1) * np.e ** (k2 * (lambda_1**2 -1) ** 2) * lambda_1**2
    # lam = lambda_1 ** 2
    return sigma_11

def sigma_hgo_perpendicular(lambda_1, a0, k1, k2, mus, alphas):
    def R(lam_2):
        return np.sum(mus * (lam_2**alphas - (lambda_1 * lam_2) ** (-alphas))) + 2 * k1*(lam_2**2-1) * np.e**(k2 * (lam_2**2 -1)**2)*lam_2**2
    guess = lambda_1**-0.5
    lambda_2 = op.newton(R, guess)

    # F = np.zeros([3, 3])
    # F[0, 0] = lambda_1
    # F[1, 1] = lambda_2
    # F[2, 2] = 1/(lambda_1 * lambda_2)

    ##################
    ## way 1
    ##################
    # J = np.linalg.det(F)
    # iso = sigma_bar_iso(F, mus, alphas)
    # # iso -= iso[2, 2] * np.eye(3, 3)
    # aniso = sigma_bar_aniso_old(F, a0, k1, k2)
    # # aniso -= aniso[2, 2] * np.eye(3, 3)
    # sigma_mat = iso + aniso
    # sigma_mat -= sigma_mat[2, 2] * np.eye(3, 3)
    # sigma_11 = sigma_mat[0,0]

    ###################
    # Way 2
    ###################
    sigma_11 = np.sum(mus * (lambda_1 ** alphas - lambda_2 ** alphas)) - 2 * k1 * (lambda_2**2 - 1) * np.e ** (k2 * (lambda_2**2 -1) ** 2) * lambda_2**2
    return sigma_11


def find_nominal_stress_given_lambda1(lambda_1, theta, k1, k2, mus, alphas):
    a0 = np.array([np.cos(np.deg2rad(theta)),
                   np.sin(np.deg2rad(theta)),
                   0])

    if np.linalg.norm(theta - 0) < 1e-10:
        sigma_11 = sigma_hgo_parallel(lambda_1, a0, k1, k2, mus, alphas)
    elif np.linalg.norm(theta - 90) < 1e-10:
        sigma_11 = sigma_hgo_perpendicular(lambda_1, a0, k1, k2, mus, alphas)
    else:
        raise ValueError("theta must either be 0 deg or 90 deg")

    nominal_stress = sigma_11 / lambda_1
    return nominal_stress


def find_stress_from_array(stretch_and_fiber_direction, k1, k2, mu1, mu2, mu3, alpha1, alpha2, alpha3):
    mus = np.array([mu1, mu2, mu3])
    alphas = np.array([alpha1, alpha2, alpha3])

    lambda_1, theta = stretch_and_fiber_direction[0], stretch_and_fiber_direction[1]

    nominal_stresses = np.array([find_nominal_stress_given_lambda1(lambda_1[i], theta[i], k1, k2, mus, alphas) for i in
                                 range(np.alen(lambda_1))])

    return nominal_stresses

def find_stress_from_array_begin(stretch_and_fiber_direction, mu1, alpha1):
    mus = np.array([mu1, 0, 0])
    alphas = np.array([alpha1, 0, 0])
    k1 = 0
    k2 = 0
    lambda_1, theta = stretch_and_fiber_direction[0], stretch_and_fiber_direction[1]

    nominal_stresses = np.array([find_nominal_stress_given_lambda1(lambda_1[i], theta[i], k1, k2, mus, alphas) for i in
                                 range(np.alen(lambda_1))])

    return nominal_stresses

def find_stress_from_array_begin_with_k(stretch_and_fiber_direction, k1, k2, mu1, alpha1):
    mus = np.array([mu1, 0, 0])
    alphas = np.array([alpha1, 0, 0])

    lambda_1, theta = stretch_and_fiber_direction[0], stretch_and_fiber_direction[1]

    nominal_stresses = np.array([find_nominal_stress_given_lambda1(lambda_1[i], theta[i], k1, k2, mus, alphas) for i in
                                 range(np.alen(lambda_1))])

    return nominal_stresses

def find_stress_from_array_begin2(stretch_and_fiber_direction, mu1, mu2, alpha1, alpha2):
    mus = np.array([mu1, mu2, 0])
    alphas = np.array([alpha1, alpha2, 0])
    k1 = 0
    k2 = 0
    lambda_1, theta = stretch_and_fiber_direction[0], stretch_and_fiber_direction[1]

    nominal_stresses = np.array([find_nominal_stress_given_lambda1(lambda_1[i], theta[i], k1, k2, mus, alphas) for i in
                                 range(np.alen(lambda_1))])

    return nominal_stresses

def find_stress_from_array_begin2_only(stretch_and_fiber_direction, mu2, alpha2):
    mus = np.array([2.91138167e+04,
                    mu2,
                    0])
    alphas = np.array([1.64892495e+02,
                       alpha2,
                       0])
    k1 = 2.07948937e+07
    k2 = 3.96933969e+02
    lambda_1, theta = stretch_and_fiber_direction[0], stretch_and_fiber_direction[1]

    nominal_stresses = np.array([find_nominal_stress_given_lambda1(lambda_1[i], theta[i], k1, k2, mus, alphas) for i in
                                 range(np.alen(lambda_1))])

    return nominal_stresses

def find_stress_from_array_all2(stretch_and_fiber_direction, k1, k2, mu1, mu2, alpha1, alpha2):
    mus = np.array([mu1,
                    mu2,
                    0])
    alphas = np.array([alpha1,
                       alpha2,
                       0])
    k1 = k1
    k2 = k2
    lambda_1, theta = stretch_and_fiber_direction[0], stretch_and_fiber_direction[1]

    nominal_stresses = np.array([find_nominal_stress_given_lambda1(lambda_1[i], theta[i], k1, k2, mus, alphas) for i in
                                 range(np.alen(lambda_1))])

    return nominal_stresses


#
# a0 = np.array([0, 1, 0])
# alpha = np.array([1, 2, 3])
# mu = np.array([1, 1, 2])*100
#
# k1 = 1000
# k2 = 2
#
# # sigma_hgo_parallel(1.1, a0, k1, k2, mu, alpha)
# sigma_hgo_perpendicular(1.1, a0, k1, k2, mu, alpha)
#
# F = np.zeros([3, 3])
# F[0, 0] = 1.1
# F[1, 1] = F[0, 0] ** -0.5
# F[2, 2] = F[0, 0] ** -0.5
#
# print('new')
# print(sigma_bar_aniso_new(F, a0, k1, k2, 0, 0))
# print('old')
# print(sigma_bar_aniso_old(F, a0, k1, k2))

# F = np.eye(3, 3)
#
# theta = 0
# # theta = 90
# # F[0, 0] = 1.1
# # F[1, 1] = F[0, 0] ** (-1/2)
# # F[2, 2] = F[0, 0] ** (-1/2)
#
# F[0, 1] = 0.2
#
# alpha = np.array([1, 2, 3])
# mu = np.array([1, 1, 2])
#
# k1 = 1000
# k2 = 2
# k3 = 1000
# k4 = 600
#
# K = 3000
#
#
# a0 = np.array([1, 0, 0])
#
# print(sigma(F, a0, k1, k2, k3, k4, K, mu, alpha))

# K = 3000
#
# F[0, 1] = 0.1
# # F[0, 2] = 0.01
# # F[1, 0] = 0.01
# print('F')
# print(F)
# theta = np.deg2rad(theta)
# a0 = np.array([np.cos(theta), np.sin(theta), 0])
# sig = sigma(F, a0, k1, k2, k3, k4, K, mu, alpha)
# print('sigma new')
# print(sig - sig[2, 2] * np.eye(3, 3))
# # k3 = 0
# # k4 = 0
# print('simga old')
# sig = sigma(F, a0, k1, k2, 0, 0, K, mu, alpha)
# print(sig - sig[2, 2] * np.eye(3, 3))
# # print('in function')
# # print(fn.sigma((np.array([F[0, 0]]), np.array([0])), k1, k2, *(alpha.tolist()), *(mu.tolist())))
#
#
# def sigma_and_F(F11_theta, k1, k2, k3, k4, alpha1, alpha2, alpha3, mu1, mu2, mu3):
#     F11_arr, theta_arr = F11_theta[0], np.deg2rad(F11_theta[1])
#     alphas = np.array([alpha1, alpha2, alpha3])
#     mus = np.array([mu1, mu2, mu3])
#     K = (np.sum(mus) + k1) * 5.0e3
#     # K = (np.sum(mus)) * 3.5e2
#
#     print('**********************')
#     print('alphas')
#     print(alphas)
#     print('mus')
#     print(mus)
#     print('k1, k2')
#     print(k1, k2)
#     print('**********************')
#
#     res = np.empty(np.alen(F11_arr))
#
#     for i in range(np.alen(F11_arr)):
#         F11 = F11_arr[i]
#         theta = theta_arr[i]
#
#         def sigma_func(F):
#             # F = np.array([1, 2, 3])
#             # print("in Theta = ", np.rad2deg(theta), " deg")
#             a0 = np.array([np.cos(theta), np.sin(theta), 0.])
#             F = np.insert(F, 0, F11)
#             # N = np.alen(alphas)
#             F = F.reshape([3, 3])
#
#             sigma_out = sigma(F, a0, k1, k2, k3, k4, K, mus, alphas)
#
#             return sigma_out.flatten()
#
#         F_guess = np.array([[F11, 0, 0],
#                             [0, F11**-0.5, 0],
#                             [0, 0, F11**-0.5]])
#         # print("**************************")
#         # print("F guess")
#         # print(F_guess)
#
#         F_guess = F_guess.flatten()
#         F_guess = F_guess[1:]
#         sigma_gamma = sigma_func(F_guess)
#         # print("Sigma Guess")
#         # print(sigma_gamma.reshape([3,3]))
#         sigma_gamma[1:] = 0
#         simga_solve = np.array([True]*9)
#         simga_solve[0] = False
#
#         F_final = fn.solve_F_sigma_sp(F_guess, sigma_gamma, simga_solve, sigma_func)
#         sigma_final = sigma_func(F_final)
#         # sigma_final = sigma_func(F_guess)
#         F_final_tens = np.insert(F_final, 0, F11).reshape(3, 3)
#         sigma_final_tens = sigma_final.reshape(3, 3)
#         nominal = np.matmul(np.linalg.inv(F_final_tens), sigma_final_tens)
#         print("Theta = ", np.rad2deg(theta), " deg")
#         print("F_11 = ", F11)
#         print("J = ", np.linalg.det(F_final_tens))
#         print("F Final")
#         print(F_final_tens)
#         print("Sigma Final")
#         print(sigma_final_tens)
#         print("nominal")
#         print(nominal)
#         print("**************************")
#         res[i] = nominal[0, 0]
#
#     return res
#
#
# # print(sigma_and_F((np.array([F[0, 0]]), np.array([theta])), k1, k2, k3, k4, *(alpha.tolist()), *(mu.tolist())))
