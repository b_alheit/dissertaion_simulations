from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
import functions as fn


data = np.genfromtxt('all_soft.dat', delimiter=',')
sorted_data = [None] * 7
names = ['0 deg', '15 deg', '30 deg', '45 deg', '60 deg', '75 deg', '90 deg']
colors = ['blue', 'green', 'red', 'yellow', 'magenta', 'black', 'cyan']
a = 0
for i in range(7):
    if i ==0:
        sorted_data[i] = data[data[:, 0] < 10]
    else:
        deg = sorted_data[i-1][0, 0]
        sorted_data[i] = data[(data[:, 0] < deg+16) & (data[:, 0] > deg+1)]
print(sorted_data)

mu1 = 80e3
mu2 = 80e3
mu3 = 6e3
alpha1 = 10.0
alpha2 = 100
alpha3 = 200
# # mu1 = 0
# mu2 = 0
# mu3 = 0
# # alpha1 = 0
# alpha2 = 0
# alpha3 = 0

k1 = 5.0e6
k2 = 1e3

# mu1 = 80e3
# mu2 = 80e3
# mu3 = 2e3
# alpha1 = 10.0
# alpha2 = 80
# alpha3 = 200
# # # mu1 = 0
# # mu2 = 0
# # mu3 = 0
# # # alpha1 = 0
# # alpha2 = 0
# # alpha3 = 0
#
# k1 = 8e6
# k2 = 4000
# #
# mu1 = 80e3
# mu2 = 80e3
# mu3 = 2e3
# alpha1 = 10.0
# alpha2 = 80
# alpha3 = 200
# # # mu1 = 0
# # mu2 = 0
# # mu3 = 0
# # # alpha1 = 0
# # alpha2 = 0
# # alpha3 = 0
#
# k1 = 2.5e5
# k2 = 5e2

p0 = (k1, k2, alpha1, alpha2, alpha3, mu1, mu2, mu3)

for i in range(7):
    x_data = (sorted_data[i][:, 1], sorted_data[i][:, 0])
    y_data = sorted_data[i][:, 2]

    N = fn.sigma(x_data, *p0)
    plt.plot(x_data[0], y_data, linewidth=0, marker='s', mfc='none', label='Data ' + names[i], color=colors[i])
    # plt.plot(x_data[0], N, label='model ' + names[i], color=colors[i], marker='o', mfc='none')
    plt.plot(x_data[0], N, label='model ' + names[i], color=colors[i])

# # popt, pconv = curve_fit(fn.sigma, xdata=x_data, ydata=y_data, p0=p0)
# # N = fn.sigma(x_data, *popt)
# # print('popt')
# # print(popt)
# # print('pconv')
# # print(pconv)
# # save_array = np.array(popt)
# # np.savetxt('properties.txt', save_array, delimiter=',')
#
#
#
#
plt.xlabel("$F_{11}$")
plt.ylabel("$N_{11}$")
plt.grid()
plt.legend()
plt.show()
