from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
import new_sigma_functions as nf


data = np.genfromtxt('all_soft.dat', delimiter=',')
sorted_data = [None] * 2
names = ['0 deg', '90 deg']
colors = ['blue', 'cyan']
a = 0
for i in range(2):
    if i ==0:
        sorted_data[i] = data[data[:, 0] < 10]
    else:
        sorted_data[i] = data[data[:, 0] > 80]
print(sorted_data)


mu1 = 2e5
mu2 = 1e5
mu3 = 1e3
alpha1 = 1.0
alpha2 = 100
alpha3 = 250
# # mu1 = 0
# mu2 = 0
# mu3 = 0
# # alpha1 = 0
# alpha2 = 0
# alpha3 = 0

k1 = 5.0e6
k2 = 1e3
p0 = (k1, k2, mu1, mu2, mu3, alpha1, alpha2, alpha3)
p0 = [2.07948486e+07, 3.96934910e+02, 2.91139106e+04, 3.06688290e+02, 1.67818488e+01, 1.64892382e+02, 3.05350646e-06, 2.01204057e-15]
# p0 = [2.07948486e+07, 3.96934910e+02, 2.91139106e+04, 3.06688290e+02, 1.67818488e+01, 1.64892382e+02]
func = nf.find_stress_from_array
for i in range(2):
    x_data = (sorted_data[i][:, 1], sorted_data[i][:, 0])
    y_data = sorted_data[i][:, 2]

    N = func(x_data, *p0)
    plt.plot(x_data[0], y_data, linewidth=0, marker='s', mfc='none', label='Data ' + names[i], color=colors[i])
    # plt.plot(x_data[0], N, label='model ' + names[i], color=colors[i], marker='o', mfc='none')
    plt.plot(x_data[0], N, label='model ' + names[i], color=colors[i])

plt.xlabel("$F_{11}$")
plt.ylabel("$N_{11}$")
plt.grid()
plt.legend()
plt.show()
