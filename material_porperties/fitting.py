from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
import functions as fn


data = np.genfromtxt('all_soft.dat', delimiter=',')
# print(data)
# data = data[data[:, 0] > 80]
data = data[data[:, 0] < 10]
# data = data[(data[:, 0] < 80) & (data[:, 0] > 70)]
# data = data[(data[:, 0] < 20) & (data[:, 0] > 10)]

x_data = (data[:, 1], data[:, 0])
y_data = data[:, 2]
print("x data")
print(x_data)
print('y_data')
print(y_data)
mu1 = 80e3
mu2 = 80e3
mu3 = 6e3
alpha1 = 10.0
alpha2 = 100
alpha3 = 200
# # mu1 = 0
# mu2 = 0
# mu3 = 0
# # alpha1 = 0
# alpha2 = 0
# alpha3 = 0

k1 = 5.0e6
k2 = 1e3
p0 = (k1, k2, alpha1, alpha2, alpha3, mu1, mu2, mu3)
# p0 = [1.99999720e+05, 3.99993293e+03, 9.99990116e+00, 9.99998031e+01,
#  2.00000859e+02, 8.00002345e+04, 8.00001065e+04, 6.00010457e+03]
# popt, pconv = curve_fit(fn.sigma, xdata=x_data, ydata=y_data, p0=p0)
# N = fn.sigma(x_data, *popt)
# print('popt')
# print(popt)
# print('pconv')
# print(pconv)
# save_array = np.array(popt)
# np.savetxt('properties.txt', save_array, delimiter=',')

N = fn.sigma(x_data, *p0)



plt.plot(x_data[0], y_data, linewidth=0, marker='s', mfc='none', label='Data')
plt.plot(x_data[0], N, label='model')
plt.grid()
plt.legend()
plt.show()
