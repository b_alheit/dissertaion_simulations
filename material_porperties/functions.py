import numpy as np
import scipy.optimize as op
import warnings
warnings.filterwarnings('error')


def grad_matrix(sigma, F):
    """
    Determine d_sigma/d_F matrix given sigma(F) and current F
    :param sigma: A vector function of F
    :param F: Some vector
    :return: A matrix of partial derivatives of simgas w.r.t. Fs
    """
    n = np.alen(F)
    grad = np.zeros([n, n])
    sigma_gamma = sigma(F)
    e = 1e-7

    for k in range(n):
        F_k = F
        F_k[k, 0] += e
        sigma_k = sigma(F_k)
        grad[:, k] = (sigma_k - sigma_gamma) / e

    return grad


def solve_F_sigma(F_guess, sigma_gamma, F_change, sigma_fuc):
    F = F_guess

    sigma = sigma_fuc(F)
    G = np.zeros([np.alen(F_guess), 1])
    G[F_change] = sigma_gamma[F_change] - sigma[F_change]
    error_init = np.linalg.norm(G)
    error = error_init

    while error/error_init > 1e-7:
        grad_mat = grad_matrix(sigma_fuc, F)

        F[F_change] += np.linalg.solve(grad_mat, G)

        sigma = sigma_fuc(F)
        G[F_change] = sigma_gamma[F_change] - sigma[F_change]
        error_init = np.linalg.norm(G)

    return F, sigma


def solve_F_sigma_sp(F_guess, sigma_gamma, sigma_solve, sigma_fuc, *arg):

    def res(F):
        sigma = sigma_fuc(F)
        # G = np.zeros(np.alen(sigma_gamma[sigma_change]))
        G = sigma_gamma[sigma_solve] - sigma[sigma_solve]
        return G

    # result = op.least_squares(res, F_guess, xtol=0)
    result = op.least_squares(res, F_guess)
    print("Error: ", np.linalg.norm(result.cost))
    print("Success: ", result.success)
    print("Exit code: ", result.status)
    return result.x


def sigma(F11_theta, k1, k2, alpha1, alpha2, alpha3, mu1, mu2, mu3):
    F11_arr, theta_arr = F11_theta[0], np.deg2rad(F11_theta[1])
    alphas = np.array([alpha1, alpha2, alpha3])
    mus = np.array([mu1, mu2, mu3])
    K = (np.sum(mus) + k1) * 5.0e3
    # K = (np.sum(mus)) * 3.5e2

    print('**********************')
    print('alphas')
    print(alphas)
    print('mus')
    print(mus)
    print('k1, k2')
    print(k1, k2)
    print('**********************')

    res = np.empty(np.alen(F11_arr))

    for i in range(np.alen(F11_arr)):
        F11 = F11_arr[i]
        theta = theta_arr[i]

        def sigma_func(F):
            # F = np.array([1, 2, 3])
            print("in Theta = ", np.rad2deg(theta), " deg")
            a0 = np.array([np.cos(theta), np.sin(theta), 0.])
            F = np.insert(F, 0, F11)
            # N = np.alen(alphas)
            F = F.reshape([3, 3])
            J = np.linalg.det(F)
            lambdas, ns = np.linalg.eig(np.matmul(F, F.T))
            lambdas = lambdas ** 0.5
            # print('lambdas')
            # print(lambdas)
            # print('ns')
            # print(ns)
            I4 = np.dot(a0, np.matmul(np.matmul(F.T, F), a0))
            # I4 = I4 ** 0.5
            a = np.matmul(F, a0)
            # sigma_aniso_bar = 2.0*k1*J**(-5.0/3.0)*(I4*J**(-2.0/3.0)-1) * np.e**(k2*(I4*J**(-2.0/3.0)-1)**2) * (np.outer(a, a) - (1.0/3.0) * I4 * np.eye(3, 3))
            # print("np.e**(k2*(I4*J**(-2/3)-1)**2) = ", np.e**(k2*(I4*J**(-2.0/3.0)-1)**2))
            # print("(I4*J**(-2/3)-1) = ", (I4*J**(-2.0/3.0)-1))

            try:
                sigma_aniso_bar = 2.0*k1*J**(-5.0/3.0)*(I4*J**(-2.0/3.0)-1) * np.e**(k2*(I4*J**(-2.0/3.0)-1)**2) * (np.outer(a, a) - (1.0/3.0) * I4 * np.eye(3, 3))
            except RuntimeWarning:
                warnings.resetwarnings()
                print("I4 = ", I4)
                print("F")
                print(F)
                print("F", np.linalg.det(F))
                print("J = ", J)
                print("J**(-2/3) = ", J**(-2/3))
                print("np.e**(k2*(I4*J**(-2/3)-1)**2) = ", np.e**(k2*(I4*J**(-2/3)-1)**2))
                print("(I4*J**(-2/3)-1) = ", (I4*J**(-2/3)-1))
                sigma_aniso_bar = 2.0*k1*J**(-5.0/3.0)*(I4*J**(-2.0/3.0)-1) * np.e**(k2*(I4*J**(-2.0/3.0)-1)**2) * (np.outer(a, a) - (1.0/3.0) * I4 * np.eye(3, 3))
            sigma_iso_bar = np.zeros([3, 3])

            lambdas *= J**(-1/3)
            for i in range(3):
                # lambda_j = -1/3 * np.sum(lambdas ** alphas)
                p_sum = np.sum(mus * (lambdas[i]**alphas - 1/3 * np.sum(lambdas ** alphas)))
                sigma_iso_bar += p_sum * np.outer(ns[:, i], ns[:, i])
            sigma_iso_bar *= 1/J
            sigma_out = sigma_iso_bar + sigma_aniso_bar
            sigma_out += K * (J-1) * np.eye(3, 3)
            # sigma_out -= sigma_out[2, 2]
            # print('F')
            # print(F)
            # print('Sigma')
            # print(sigma_out)
            return sigma_out.flatten()

        F_guess = np.array([[F11, 0, 0],
                            [0, F11**-0.5, 0],
                            [0, 0, F11**-0.5]])
        # print("**************************")
        # print("F guess")
        # print(F_guess)

        F_guess = F_guess.flatten()
        F_guess = F_guess[1:]
        sigma_gamma = sigma_func(F_guess)
        # print("Sigma Guess")
        # print(sigma_gamma.reshape([3,3]))
        sigma_gamma[1:] = 0
        simga_solve = np.array([True]*9)
        simga_solve[0] = False

        F_final = solve_F_sigma_sp(F_guess, sigma_gamma, simga_solve, sigma_func)
        sigma_final = sigma_func(F_final)
        # sigma_final = sigma_func(F_guess)
        F_final_tens = np.insert(F_final, 0, F11).reshape(3, 3)
        sigma_final_tens = sigma_final.reshape(3, 3)
        nominal = np.matmul(np.linalg.inv(F_final_tens), sigma_final_tens)
        print("Theta = ", np.rad2deg(theta), " deg")
        print("F_11 = ", F11)
        print("J = ", np.linalg.det(F_final_tens))
        print("F Final")
        print(F_final_tens)
        print("Sigma Final")
        print(sigma_final_tens)
        print("nominal")
        print(nominal)
        print("**************************")
        res[i] = nominal[0, 0]

    return res








