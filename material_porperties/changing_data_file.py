import numpy as np
import matplotlib.pyplot as plt
import pprint

data = np.genfromtxt('raw_data.csv', delimiter=',')
names = ['0$^0$', '15$^0$', '30$^0$', '45$^0$', '60$^0$', '75$^0$', '90$^0$']
markers = ['s', 'd', 'o', '^', 'v', 'h', '*']
fit_grad = 200

strain_45_deg = data[:, 6]
stress_45_deg = data[:, 7]

pprint.pprint(data)
print()
pprint.pprint(data[:, 0::2])

front = (strain_45_deg < 0.1) & (strain_45_deg > 0.02)
avg_grad_front = np.mean(stress_45_deg[front] / strain_45_deg[front])

back = strain_45_deg > 0.2
avg_grad_back = np.mean(stress_45_deg[back] / strain_45_deg[back])

print("Front grad: ", avg_grad_front, " MPa")
print("Back grad: ", avg_grad_back, " MPa")

back_sf = fit_grad/avg_grad_back
front_sf = fit_grad/avg_grad_front

stress_front = data[:, 1::2] * front_sf ** 0.5
strain_front = data[:, 0::2] * front_sf ** -0.5
stress_back = data[:, 1::2] * back_sf ** 0.5
strain_back = data[:, 0::2] * back_sf ** -0.5
angle_array = np.zeros(np.shape(stress_front))
print(np.shape(stress_front))
angles = [0, 15, 30, 45, 60, 75, 90]
for i in range(len(angles)):
    angle_array[:, i] = angles[i]

all_stress = []
all_strain = []
all_angle = []

for i in range(len(names)):
    logic = np.logical_not(np.isnan(stress_front[:, i]))

    all_stress = all_stress + (stress_front[:, i][logic] * 1e6).tolist()
    all_strain = all_strain + (strain_front[:, i][logic] + 1).tolist()
    all_angle = all_angle + (angle_array[:, i][logic]).tolist()

all_stiff = np.array([all_angle,
                     all_strain,
                     all_stress]).T

np.savetxt('all_stiff.dat', all_stiff, delimiter=',')

all_stress = []
all_strain = []
all_angle = []

for i in range(len(names)):
    logic = np.logical_not(np.isnan(stress_back[:, i]))

    all_stress = all_stress + (stress_back[:, i][logic] * 1e6).tolist()
    all_strain = all_strain + (strain_back[:, i][logic] + 1).tolist()
    all_angle = all_angle + (angle_array[:, i][logic]).tolist()

all_soft = np.array([all_angle,
                     all_strain,
                     all_stress]).T

np.savetxt('all_soft.dat', all_soft, delimiter=',')


# np.savetxt('stiff_stress_nom.dat', stress_front * 1e6, delimiter=',')
# np.savetxt('stiff_F11.dat', strain_front + 1, delimiter=',')
# np.savetxt('soft_stress_nom.dat', stress_back * 1e6, delimiter=',')
# np.savetxt('soft_F11.dat', strain_back + 1, delimiter=',')
# np.savetxt('angels.dat', angle_array, delimiter=',')

for i in range(len(names)):
    plt.plot(strain_front[:, i] + 1, stress_front[:, i] * 1e6, marker=markers[i], mfc="none", linewidth=0, label=names[i])
    # plt.plot(strain_back[:, i], stress_back[:, i], marker=markers[i], mfc="none", linewidth=0, label=names[i])

# plt.plot(strain_front, stress_front, marker='o', mfc="none", linewidth=0)
# plt.plot(strain_back, stress_back, marker='o', mfc="none", linewidth=0)

plt.plot(strain_back[:,-1], strain_back[:,-1] * 200)


# plt.plot(strain_45_deg, stress_45_deg, marker='o', linewidth=0)
# plt.plot(strain_45_deg, strain_45_deg*avg_grad_front)
# plt.plot(strain_45_deg, strain_45_deg*avg_grad_back)
plt.legend()
plt.grid()
plt.show()