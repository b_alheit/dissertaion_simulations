import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('raw_data.csv', delimiter=',')
names = ['0$^0$', '15$^0$', '30$^0$', '45$^0$', '60$^0$', '75$^0$', '90$^0$']

for i in range(len(names)):
    plt.plot(data[:, 2*i], data[:, 2*i+1], marker='o', linewidth=0, label=names[i])

plt.grid()
plt.legend()
plt.show()

# stress = data[:, 1]
# strain = data[:, 0]
#
# plt.plot(strain, stress, marker='s', linewidth=0)
# plt.show()