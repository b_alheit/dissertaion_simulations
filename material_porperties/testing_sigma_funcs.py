import numpy as np
import matplotlib.pyplot as plt
import functions as fn

n_pts = 100

mu1 = 2.0*80e3
# mu1 = 0
mu2 = 0
mu3 = 0
alpha1 = 2.0
# alpha1 = 0
alpha2 = 0
alpha3 = 0

k1 = 2.456e4
k2 = 1500



# k1 = 3e3
# k2 = 1.0

theta = np.deg2rad(90) * np.ones(n_pts)

sigma_11 = np.zeros(n_pts)
# F_11 = np.linspace(1, 1.02618, n_pts)
F_11 = np.linspace(1, 1.026, n_pts)

sigma_11 = fn.sigma((F_11, theta), k1, k2, alpha1, alpha2, alpha3, mu1, mu2, mu3)

# for i in range(n_pts):
#     sigma_11[i] = fn.sigma((F_11[i], theta), k1, k2, alpha1, alpha2, alpha3, mu1, mu2, mu3)

plt.plot(F_11, sigma_11)
plt.grid()
plt.show()