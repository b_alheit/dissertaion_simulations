

subroutine cauchy_stress(F, G, D, k1, k2, n0, sigma)
   double precision :: F(3,3), G, D, k1, k2, n0(3,1)
   double precision :: b_bar(3,3), C_bar(3,3), jacobian, I4(1, 1), psi4
   double precision :: sigma(3,3), Id(3,3)
   double precision :: det, tr, heaviside

   call identity(Id, 3)
   jacobian = det(F)
   ! b_bar = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))
   ! C_bar = matmul(transpose(F), F) * (jacobian ** (-2.0/3.0))
   b_bar = matmul(F, transpose(F))
   C_bar = matmul(transpose(F), F)
   I4 = matmul(transpose(n0), matmul(C_bar, n0))
   psi4 = heaviside(I4(1, 1)-1) * (I4(1, 1)-1) * k1 * exp(k2 * (I4(1, 1)-1)**2)



   !sigma = (2.0/jacobian) * G * (b_bar - (1.0/3.0) *tr(b_bar) * Id) + (2.0/D)*(jacobian - 1.0) *Id
   ! sigma = (2.0/D)*(jacobian - 1.0) *Id
   ! sigma = sigma + (2.0/jacobian) * psi4 * matmul(F, matmul(matmul(n0, transpose(n0)), transpose(F)))
   sigma = (2.0/jacobian) * psi4 * matmul(F, matmul(matmul(n0, transpose(n0)), transpose(F)))
   print *, "sigma"
   call print_matrix(sigma, 3, 3)
   print *,
end

subroutine neo_hookean_C(F, G, D, C)
  double precision :: F(3, 3), G, D
  integer :: i, j, k, l
  integer :: ci, cj
  integer :: is(6), js(6)
  double precision :: b_bar(3,3), jacobian
  double precision :: det, tr, neo_hookean_C_ijkl
  double precision :: C(6,6)

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]

  jacobian = det(F)
  b_bar = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))

  C = 0

  do ci = 1,6
    do cj=1,6
      i = is(ci)
      j = js(ci)
      k = is(cj)
      l = js(cj)

      C(ci, cj) = neo_hookean_C_ijkl(b_bar, jacobian, G, D, i, j, k, l)
    enddo
  enddo
end



function neo_hookean_C_ijkl(b_bar, jacobian, G, D, i, j, k, l) result(c)
   double precision :: b_bar(3,3), jacobian, G, D
   integer :: i, j, k, l
   integer :: delta
   double precision :: tr
   double precision :: c

   c = (2.0/jacobian) * G * (0.5 * (delta(i,k)*b_bar(j,l) + b_bar(i,k)*delta(j,l) + delta(i,l)*b_bar(j,k) + b_bar(i,l)*delta(j,k)) &
       - (2.0/3.0) * (delta(i,j)*b_bar(k,l) + b_bar(i, j) * delta(k,l)) + (2.0/9.0)*delta(i,j)*delta(k,l)*tr(b_bar)) &
       + (2.0/D)*(2*jacobian-1)*delta(i,j)*delta(k,l)
end function neo_hookean_C_ijkl


subroutine vol_C(F, D, C)
  double precision :: F(3, 3), D
  integer :: i, j, k, l
  integer :: ci, cj
  integer :: is(6), js(6)
  double precision :: jacobian
  double precision :: det, tr, vol_C_ijkl
  double precision :: C(6,6)

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]

  jacobian = det(F)

  C = 0

  do ci = 1,6
    do cj=1,6
      i = is(ci)
      j = js(ci)
      k = is(cj)
      l = js(cj)

      C(ci, cj) = vol_C_ijkl(jacobian, D, i, j, k, l)
    enddo
  enddo
end


function vol_C_ijkl(jacobian, D, i, j, k, l) result(c)
   double precision :: jacobian, D
   integer :: i, j, k, l
   integer :: delta
   double precision :: c

   c = (2.0/D) * (2*jacobian - 1) * delta(i,j) * delta(k,l)
end function vol_C_ijkl
