! Created by  on 2019/05/04.

program test
    integer :: N
    doubleprecision :: F(3, 3), mus(3), alphas(3), k1, k2, a0(3), K0
    doubleprecision :: sigma_vec(6), c_mat(6, 6)
    doubleprecision :: det
    call identity(F, 3)

    F(1, 1) = 1.2
    F(2, 2) = 0.8
    F(1, 2) = 0.2


    N = 3
    mus = [1, 2, 3]
    alphas = [1, 2, 3]

    k1 = 1
    k2 = 1

    K0 = 1

    a0 = [1, 2, 3]
    a0 = a0 / norm2(a0)


    F = F * det(F) ** (-1./3.)
    call hgo_model(F, mus, alphas, N, k1, k2, a0, K0, sigma_vec, c_mat)

end program test