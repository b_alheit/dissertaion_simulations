import numpy as np

LE = np.zeros([3, 3])

LE[0, 1] = 1.102e-1
LE[0, 2] = -1.2e-2
LE[1, 2] = -6.37e-4
LE += LE.T
LE[0, 0] = 3.22e-3
LE[1, 1] = -8.15e-4
LE[2, 2] = 4.013e-3

sig = np.zeros([3, 3])

sig[0, 1] = 1.675e6
sig[0, 2] = -1.87e5
sig[1, 2] = -6.77e4
sig += sig.T
sig[0, 0] = 4.45e6
sig[1, 1] = 4.34e6
sig[2, 2] = 3.91e6

P = -4.232e6

pls, ns = np.linalg.eig(LE)

ps = np.exp(pls)

v = np.zeros([3, 3])
for i in range(3):
    v += ps[i] * np.outer(ns[:, i], ns[:, i])

b = np.matmul(v, v)
j = np.linalg.det(b)

sig_p, ns_s = np.linalg.eig(sig)
sig_is = sig + np.eye(3, 3)*P
test_sig_is = np.trace(sig_is)
sig_is_p, ns_is_s = np.linalg.eig(sig_is)
a = 0