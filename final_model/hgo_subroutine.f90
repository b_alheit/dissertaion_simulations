!> @brief Calculates the contribution of the isotropic ogden component of the model to
!! the CTM that Abaqus requires
!!
!! Using the equations:
!!      % Insert formula from write up here
!!
!! @param[in]   lambda_bars  the isochoric principle stretches
!! @param[in]   ns  a matrix with columns containing the principle directions in the current configuration
!! @param[in]   mus an array of material constants \f$\mu\f$
!! @param[in]   alphas an array of material constants \f$\alpha\f$
!! @param[in]   Nps size of the material constant arrays
!! @param[in]   jacobian the determinant of \f$\mathbb{F}\f$
!! @param[in]   sigma the stress contribution of the isochoric ogden strain energy function
!! @param[in]   sigma_p the principle stresses of the isochoric ogden strain energy function
!! @param[in]   \f$i,j,k,l\f$  the indicies of the CTM for which the value must be calculated
!!
!! @return  The value of the entry at index i,j,k,l
subroutine hgo_model(F, mus, alphas, N, k1, k2, a0, K0, sigma_vec, c_mat)
    implicit none

    integer, intent(in) :: N
    doubleprecision, intent(in) :: F(3, 3), mus(N), alphas(N), k1, k2, a0(3), K0

    integer :: i, j, k, l, p, ci, cj, is(6), js(6), info
    doubleprecision :: jacobian, b_bar(3, 3), ns(3, 3), eig_vecs(3, 3), lambda_bars(3), sigma_mat(3, 3), &
    work(16), Id(3, 3), work1, work2, sigma_p(3), a_bar(3), I4_bar, psi4
    integer :: delta
    doubleprecision :: det, ogden_c_bar_ijkl, aniso_C_bar_ijkl

    doubleprecision, intent(out) :: sigma_vec(6), c_mat(6, 6)


    is = [1, 2, 3, 2, 1, 1]
    js = [1, 2, 3, 3, 3, 2]
    sigma_mat = 0
    call identity(Id, 3)

    jacobian = det(F)
    b_bar = matmul(F, transpose(F)) * jacobian ** (-2./3.)

    a_bar = matmul(F, a0) * jacobian ** (-1./3.)
    I4_bar = dot_product(a_bar, a_bar)

    ns = b_bar
    call dsyev('V', 'U', 3, ns, 3, lambda_bars, work, 16, info)
    lambda_bars = lambda_bars ** 0.5

    sigma_p = 0
    do p =1,N
        work1 = -sum(lambda_bars**alphas(p))/3.
        do i=1,3
            sigma_p(i) = sigma_p(i) + (1.0/jacobian) * mus(p) * (lambda_bars(i)**alphas(p) + work1)
        enddo
    enddo

    do i=1,3
        sigma_mat = sigma_mat + sigma_p(i) * spread(ns(:, i),dim=2,ncopies=3)*spread(ns(:, i),dim=1,ncopies=3)
    end do

    do ci = 1,6
        do cj=1,6
              i = is(ci)
              j = js(ci)
              k = is(cj)
              l = js(cj)

              C_mat(ci, cj) = aniso_C_bar_ijkl(jacobian, k1, k2, a_bar, I4_bar, i, j, k, l) + &
                      ogden_c_bar_ijkl(lambda_bars, ns, mus, alphas, N, jacobian, sigma_mat, sigma_p, i, j, k, l)
        enddo
    enddo

    C_mat(1:3, 1:3) = C_mat(1:3, 1:3) + K0 * (2.*jacobian - 1)

    sigma_mat = sigma_mat + Id * K0 * (jacobian - 1.)

    psi4 = (I4_bar-1.) * k1 * exp(k2 * (I4_bar-1.)**2.)
    sigma_mat = sigma_mat + (2.0/jacobian) * psi4 &
            * (spread(a_bar,dim=2,ncopies=3)*spread(a_bar,dim=1,ncopies=3) - (1.0/3.0) * I4_bar * Id)
    print*, 'sigma_mat'
    call print_matrix(sigma_mat, 3, 3)
    print*, 'C_mat'
    call print_matrix(c_mat, 6, 6)
    call matrix_to_voignt(sigma_mat, sigma_vec)

end subroutine

!> @brief Calculates the contribution of the isotropic ogden component of the model to
!! the CTM that Abaqus requires
!!
!! Using the equations:
!!      % Insert formula from write up here
!!
!! @param[in]   lambda_bars  the isochoric principle stretches
!! @param[in]   ns  a matrix with columns containing the principle directions in the current configuration
!! @param[in]   mus an array of material constants \f$\mu\f$
!! @param[in]   alphas an array of material constants \f$\alpha\f$
!! @param[in]   N size of the material constant arrays
!! @param[in]   jacobian the determinant of \f$\mathbb{F}\f$
!! @param[in]   sigma the stress contribution of the isochoric ogden strain energy function
!! @param[in]   sigma_p the principle stresses of the isochoric ogden strain energy function
!! @param[in]   \f$i,j,k,l\f$  the indicies of the CTM for which the value must be calculated
!!
!! @return  The value of the entry at index i,j,k,l
function ogden_c_bar_ijkl(lambda_bars, ns, mus, alphas, N, jacobian, sigma, sigma_p, i, j, k, l) result(c_val)
    integer, intent(in) :: i, j, k, l, N
    doubleprecision, intent(in) :: lambda_bars(3), ns(3, 3), mus(N), alphas(N), sigma(3, 3), sigma_p(3), jacobian

    doubleprecision :: mult1, mult2, add1
    integer :: a, b, p

    doubleprecision :: det
    integer :: delta

    doubleprecision :: c_val

    c_val = 0

    do a=1,3
        do b=1,3
            mult1 = 0
            do p=1,N
                add1 = sum(lambda_bars**alphas(p))/9.
                mult1 = mult1 + alphas(p) * mus(p) * (lambda_bars(a)**alphas(p) * delta(a, b)  &
                        - lambda_bars(b)**alphas(p)/3. - lambda_bars(a)**alphas(p)/3. + add1)
            end do
            c_val = c_val + mult1 * ns(i, a) * ns(j, a) * ns(k, b) * ns(l, b)
        end do
    end do

    c_val = c_val / jacobian


!    ***************************************************
!    ************* Second bit (Third in notes) *********
!    ***************************************************

    do a=1,3
        do b=1,3
            if (a /= b)then
                if(abs(lambda_bars(a)-lambda_bars(b)) < 1e-8)then
                    mult1 = 0
                    mult2 = 0
                    do p=1,N
                        add1 = sum(lambda_bars**alphas(p))/9.
                        mult1 = mult1 + alphas(p) * mus(p) * (lambda_bars(b)**alphas(p)/3. + add1)
                    end do
                    do p=1,N

                        add1 = sum(lambda_bars**alphas(p))/9.

                        mult2 = mult2 + alphas(p) * mus(p) * (lambda_bars(a)**alphas(p) * delta(a, b) &
                                - lambda_bars(b)**alphas(p)/3. - lambda_bars(a)**alphas(p)/3. + add1)
                    end do
                    mult1 = (mult1 - mult2) / (2.0 * jacobian) - sigma_p(b)
                else
                    mult1 = (sigma_p(a) * lambda_bars(b)**2 - sigma_p(b) * lambda_bars(a)**2) /&
                            (lambda_bars(a)**2 - lambda_bars(b)**2)
                end if
                c_val = c_val + mult1 * (ns(i, a)*ns(j, b)*ns(k, a)*ns(l, b) + ns(i, a)*ns(j, b)*ns(k, b)*ns(l, a))
            end if
        end do
    end do

!    *********************************************
!    ******** Third part (second in notes) *******
!    *********************************************

    do a=1,3
        c_val = c_val - 2. * sigma_p(a) * ns(i, a) * ns(j, a) * ns(k, a) * ns(l, a)
    end do


!    ******************************
!    ****** Convert to Jaumann ****
!    ******************************

        c_val = c_val + (sigma(i, k) * delta(j, l) + sigma(i, l) * delta(j, k) + sigma(j, k) * delta(i, l) &
                + delta(i, k) * sigma(j, l))/2.

end function ogden_c_bar_ijkl

!> @brief Calculates the contribution of the anisotropic component of the HGO model to
!! the CTM that Abaqus requires
!!
!! using the equation:
!!      % Insert formula from write up here
!!
!! @param[in]   \f$\mathbb{a}\f$  the isochoric fibre direction in the current configuration (\f$\mathbb{Fa}_0J^{-1/3}\f$)
!! @param[in]   \f$I4\f$  the isochoric 4th invariant (\f$\mathbb{a\cdot a}\f$)
!! @param[in]   \f$k1, k2\f$  the material constants for the HGO model
!! @param[in]   \f$i,j,k,l\f$  the indicies of the CTM for which the value must be calculated
!!
!! @return  The value of the entry at index i,j,k,l
function aniso_C_bar_ijkl(jacobian, k1, k2, a, I4, i, j, k, l) result(c)
    double precision, intent(in) :: a(3), I4, jacobian, k1, k2
    integer, intent(in) :: i, j, k, l
    integer :: delta ! Kronecker delta function
    double precision :: c

    ! Applying the equation
    c = (2.0*k1/jacobian)*exp(k2*(I4-1)**2) * (2.0*(1+2.0*k2*(I4-1)**2) &
    *(a(i)*a(j)-(1.0/3.0)*I4*delta(i,j))*(a(k)*a(l)-(1.0/3.0)*I4*delta(k,l))&
    +(I4-1)*((delta(i,k)*a(j)*a(l) + delta(j,l)*a(i)*a(k) + delta(j,k)*a(i)*a(l) + delta(i,l)*a(j)*a(k))/2 &
    - (2.0/3.0)*(delta(k,l)*a(i)*a(j)+delta(i,j)*a(l)*a(k)) &
    + 2.0/9.0*I4*delta(i,j)*delta(k,l)))
end function aniso_C_bar_ijkl