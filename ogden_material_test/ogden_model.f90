program test
  double precision :: C(6,6), C_apprx(6,6), G, D, F(3,3), k_stress(3,3), sigma(3,3), eps, det
  integer :: delta, n_order
  double precision :: k1, k2, n0(3, 1), mus(4), alphas(4)
  double precision :: c_stress_voigt(6), start, end_x

  G = 80.0e3
  D = 0.2

  n_order = 4

  alphas = [2.7971, -2.7188, 10.505, 0.33382]
  mus = [0.77817, -0.011229, 1.268e-7, 16.169]

  k1 = 1.56e3
  k2 = 0.4465

  k1 = 0
  k2 = 0

  ! k2 = 2.4
  n0 = 0
  n0(1,1) = 1.0

  eps = 1.0e-8

  call identity(F, 3)


  ! call stresses_and_stiffness_matrix(F, G, D, c_stress, k_stress, k1, k2, n0, C, C_apprx, eps)
  start = 1.0
  end_x = 5.0
  ! call  stretch_v_stress_1(F, mus, alphas, n_order, D, sigma, k_stress, k1, k2, n0, start, end_x, 100)
  call  biaxial_stretch_v_stress_1(F, mus, alphas, n_order, D, sigma, k_stress, k1, k2, n0, start, end_x, 100)
  ! call  stretch_v_stress_2(F, G, D, sigma, k_stress, k1, k2, n0, start, end_x, 100)
end


! subroutine stresses_and_stiffness_matrix(F, G, D, c_stress, k_stress, k1, k2, n0, C, C_apprx, eps)
!   double precision :: C(6,6), C_apprx(6,6), G, D, F(3,3), k_stress(3,3), c_stress(3,3), eps, det
!   double precision :: k1, k2, n0(3, 1)
!
!   call cauchy_stress(F, G, D, k1, k2, n0, c_stress)
!   call kirchhoff_stress(F, G, D, k1, k2, n0, k_stress)
!
!   print*, "cauchy_stress"
!   call print_matrix(c_stress, 3, 3)
!
!   print*, ""
!
!   print*, "kirchhoff_stress"
!   call print_matrix(k_stress, 3, 3)
!
!   call neo_hookean_C(F, G, D, C)
!   call approximate_C(F, G, D, k1, k2, n0, C_apprx, eps)
!
!   print*, "Stiffness matrix"
!   call print_matrix(C, 6, 6)
!
!   print*,
!   print*, "Approximate Stiffness matrix"
!   call print_matrix(C_apprx, 6, 6)
!
!   print*,
!   print*, "Error %"
!   call print_matrix(100*(C_apprx - C)/C, 6, 6)
! end

subroutine cauchy_stress(F, D, mus, alphas, n_order, k1, k2, n0, sigma)
   double precision :: F(3,3), D, k1, k2, n0(3,1), mus(n_order), alphas(n_order)
   integer :: n_order
   double precision :: b_bar(3,3), C_bar(3,3), jacobian, I4(1, 1), psi4, trace_comp
   double precision :: sigma(3,3), Id(3,3)
   double precision :: det, tr, heaviside
   double precision :: lambda_bars(3), n_s(3, 3), WORK(16)
   integer :: i, j, p, INFO

   call identity(Id, 3)
   jacobian = det(F)
   b_bar = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))

   n_s = b_bar

   call dsyev('V', 'U', 3, n_s, 3, lambda_bars, WORK, 16, INFO)


   do i=1,3
     lambda_bars(i) = lambda_bars(i) ** 0.5
   enddo

   print *, "*****************************************"
   print *, "lambda_bars"
   call print_vector(lambda_bars, 3)
   print *,

   print *, "n_s"
   call print_matrix(n_s, 3, 3)
   print *,

   print *, "jacobian_bar", lambda_bars(1)*lambda_bars(2)* lambda_bars(3)
   print *,

   print *, "alphas"
   call print_vector(alphas, n_order)
   print *,

   print *, "mus"
   call print_vector(mus, n_order)
   print *,



   sigma = 0

   do p =1,n_order
     trace_comp = 0
     do j = 1,3
       trace_comp = trace_comp + lambda_bars(j)**alphas(p)
     enddo
     trace_comp = - trace_comp * (1.0/3.0)
     do i=1,3
       ! sigma = sigma + mus(p) * (lambda_bars(i)**alphas(p) + trace_comp) * matmul(n_s(:,i),transpose(n_s(:,i)))
       sigma = sigma + mus(p) * (lambda_bars(i)**alphas(p) + trace_comp) &
       * spread(n_s(:, i),dim=2,ncopies=3)*spread(n_s(:, i),dim=1,ncopies=3)
     enddo
   enddo

   ! print *, "sigma"
   ! call print_matrix(sigma, 3, 3)
   ! print *,

   sigma = sigma/jacobian + (2.0/D)*(jacobian - 1.0) *Id

   ! print *, "sigma"
   ! call print_matrix(sigma, 3, 3)
   ! print *,

   print *, "tr(sigma)", tr(sigma)
   print *,

   sigma = sigma - sigma(2,2)*Id


    print *, "sigma"
    call print_matrix(sigma, 3, 3)
    print *,

   ! C_bar = matmul(transpose(F), F) * (jacobian ** (-2.0/3.0))
   ! I4 = matmul(transpose(n0), matmul(C_bar, n0))
   ! psi4 = heaviside(I4(1, 1)-1) * (I4(1, 1)-1) * k1 * exp(k2 * (I4(1, 1)-1)**2)


   ! sigma = (2.0/jacobian) * G * (b_bar - (1.0/3.0) *tr(b_bar) * Id) + (2.0/D)*(jacobian - 1.0) *Id
   ! sigma = sigma + (2.0/jacobian) * psi4 * matmul(F, matmul(matmul(n0, transpose(n0)), transpose(F)))
end

subroutine nominal_stress(F, D, mus, alphas, n_order, k1, k2, n0, sigma)
   double precision :: F(3,3), D, k1, k2, n0(3,1), mus(n_order), alphas(n_order)
   integer :: n_order
   double precision :: b_bar(3,3), C_bar(3,3), jacobian, I4(1, 1), psi4, trace_comp
   double precision :: sigma(3,3), Id(3,3)
   double precision :: det, tr, heaviside
   double precision :: lambda_bars(3), n_s(3, 3), WORK(16)
   integer :: i, j, p, INFO

   call identity(Id, 3)
   jacobian = det(F)
   b_bar = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))

   n_s = b_bar

   call dsyev('V', 'U', 3, n_s, 3, lambda_bars, WORK, 16, INFO)


   do i=1,3
     lambda_bars(i) = lambda_bars(i) ** 0.5
   enddo

   print *, "*****************************************"
   print *, "lambda_bars"
   call print_vector(lambda_bars, 3)
   print *,

   print *, "n_s"
   call print_matrix(n_s, 3, 3)
   print *,

   print *, "jacobian_bar", lambda_bars(1)*lambda_bars(2)* lambda_bars(3)
   print *,

   print *, "alphas"
   call print_vector(alphas, n_order)
   print *,

   print *, "mus"
   call print_vector(mus, n_order)
   print *,



   sigma = 0

   do p =1,n_order
     trace_comp = 0
     do j = 1,3
       trace_comp = trace_comp + lambda_bars(j)**alphas(p)
     enddo
     trace_comp = - trace_comp * (1.0/3.0)
     do i=1,3
       ! sigma = sigma + mus(p) * (lambda_bars(i)**alphas(p) + trace_comp) * matmul(n_s(:,i),transpose(n_s(:,i)))
       sigma = sigma + mus(p) * (lambda_bars(i)**alphas(p) + trace_comp) &
       * spread(n_s(:, i),dim=2,ncopies=3)*spread(n_s(:, i),dim=1,ncopies=3)
     enddo
   enddo

   ! print *, "sigma"
   ! call print_matrix(sigma, 3, 3)
   ! print *,

   sigma = sigma/jacobian + (2.0/D)*(jacobian - 1.0) *Id

   ! print *, "sigma"
   ! call print_matrix(sigma, 3, 3)
   ! print *,

   print *, "tr(sigma)", tr(sigma)
   print *,

   sigma = sigma - sigma(3,3)*Id
   sigma = sigma / F(1, 1)


    print *, "sigma"
    call print_matrix(sigma, 3, 3)
    print *,

   ! C_bar = matmul(transpose(F), F) * (jacobian ** (-2.0/3.0))
   ! I4 = matmul(transpose(n0), matmul(C_bar, n0))
   ! psi4 = heaviside(I4(1, 1)-1) * (I4(1, 1)-1) * k1 * exp(k2 * (I4(1, 1)-1)**2)


   ! sigma = (2.0/jacobian) * G * (b_bar - (1.0/3.0) *tr(b_bar) * Id) + (2.0/D)*(jacobian - 1.0) *Id
   ! sigma = sigma + (2.0/jacobian) * psi4 * matmul(F, matmul(matmul(n0, transpose(n0)), transpose(F)))
end

! subroutine kirchhoff_stress(F, D, mus, alphas, n_order, k1, k2, n0, tau)
!    double precision :: F(3,3), D, k1, k2, n0(3,1), mus(n_order), alphas(n_order)
!    integer :: n_order
!    double precision :: jacobian
!    double precision :: det
!    double precision :: tau(3,3)
!
!    jacobian = det(F)
!    call cauchy_stress(F, G, D, k1, k2, n0, tau)
!    tau = jacobian * tau
! end

! subroutine approximate_C(F, G, D, k1, k2, n0, C, eps)
!   double precision :: F(3,3), C(6,6), G, D, eps, k1, k2, n0(3,1)
!   double precision :: F_hat(3,3), k_stress(6), k_stress_hat(6), k_stress_mat(3,3), jacobian
!   double precision :: Id(3,3), det, es(3, 3, 1)
!   integer :: is(6), js(6), i, i_mat, j_mat
!
!   is = [1, 2, 3, 2, 1, 1]
!   js = [1, 2, 3, 3, 3, 2]
!   call identity(Id, 3)
!   jacobian = det(F)
!
!   es = 0
!   es(1, 1, 1) = 1
!   es(2, 2, 1) = 1
!   es(3, 3, 1) = 1
!
!
!   call kirchhoff_stress(F, G, D, k1, k2, n0, k_stress_mat)
!   call matrix_to_voignt(k_stress_mat, k_stress)
!
!   do i=1,6
!     i_mat = is(i)
!     j_mat = js(i)
!
!     ! print*, 'i, j: ', i_mat, ', ', j_mat
!     ! print*, "ei tens ej"
!     ! call print_matrix(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), 3, 3)
!     ! print*, "ej tens ei"
!     ! call print_matrix(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), 3, 3)
!
!     F_hat = F + (eps/2.0)*(matmul(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), F) &
!             + matmul(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), F))
!
!     call kirchhoff_stress(F_hat, G, D, k1, k2, n0, k_stress_mat)
!     call matrix_to_voignt(k_stress_mat, k_stress_hat)
!
!     C(:, i) = (1.0/(jacobian * eps)) * (k_stress_hat - k_stress)
!   enddo
! end

! subroutine stretch_v_stress_2(F, mus, alphas, n_order, D, sigma, k_stress, k1, k2, n0, start, end_x, n_pts)
!   double precision :: C(6,6), C_apprx(6,6), G, D, F(3,3), k_stress(3,3), sigma(3,3), eps, det
!   double precision :: mus(n_order), alphas(n_order)
!   integer :: n_order
!   double precision :: k1, k2, n0(3, 1), start, end_x, dx
!   integer :: n_pts, i
!
!   OPEN(UNIT=1,FILE="og stress22.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
!   OPEN(UNIT=2,FILE="og stretch2.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
!
!   dx = (end_x - start)/(n_pts - 1)
!
!   call identity(F, 3)
!   do i=1,n_pts
!     F(2, 2) = start + dx *(i-1)
!     F(1, 1) = F(2, 2) ** (-0.5)
!     F(3, 3) = F(2, 2) ** (-0.5)
!
!     print*, "jacobian = ", det(F)
!
!     call cauchy_stress(F, D, mus, alphas, n_order, k1, k2, n0, sigma)
!     write(unit=1, fmt=*) sigma(2, 2)
!     write(unit=2, fmt=*) F(2, 2)
!   enddo
!   close(unit=1)
!   close(unit=2)
! end

subroutine stretch_v_stress_1(F, mus, alphas, n_order, D, sigma, k_stress, k1, k2, n0, start, end_x, n_pts)
  double precision :: C(6,6), C_apprx(6,6), G, D, F(3,3), k_stress(3,3), sigma(3,3), eps, det
  double precision :: mus(n_order), alphas(n_order)
  integer :: n_order
  double precision :: k1, k2, n0(3, 1), start, end_x, dx
  integer :: n_pts, i

  OPEN(UNIT=1,FILE="og stress11-4.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
  OPEN(UNIT=2,FILE="og stretch1-4.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")

  dx = (end_x - start)/(n_pts - 1)

  call identity(F, 3)
  do i=1,n_pts
    F(1, 1) = start + dx *(i-1)
    F(2, 2) = F(1, 1) ** (-0.5)
    F(3, 3) = F(1, 1) ** (-0.5)

    ! print*, "jacobian = ", det(F)

    ! call cauchy_stress(F, D, mus, alphas, n_order, k1, k2, n0, sigma)
    call nominal_stress(F, D, mus, alphas, n_order, k1, k2, n0, sigma)
    write(unit=1, fmt=*) sigma(1, 1)
    write(unit=2, fmt=*) F(1, 1)
  enddo
  close(unit=1)
  close(unit=2)
end


subroutine biaxial_stretch_v_stress_1(F, mus, alphas, n_order, D, sigma, k_stress, k1, k2, n0, start, end_x, n_pts)
  double precision :: C(6,6), C_apprx(6,6), G, D, F(3,3), k_stress(3,3), sigma(3,3), eps, det
  double precision :: mus(n_order), alphas(n_order)
  integer :: n_order
  double precision :: k1, k2, n0(3, 1), start, end_x, dx
  integer :: n_pts, i

  OPEN(UNIT=1,FILE="og stress11-4.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
  OPEN(UNIT=2,FILE="og stretch1-4.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")

  dx = (end_x - start)/(n_pts - 1)

  call identity(F, 3)
  do i=1,n_pts
    F(1, 1) = start + dx *(i-1)
    F(2, 2) = F(1, 1)
    F(3, 3) = F(1, 1) ** (-2.0)

    print*, "jacobian = ", det(F)

    ! call cauchy_stress(F, D, mus, alphas, n_order, k1, k2, n0, sigma)
    call nominal_stress(F, D, mus, alphas, n_order, k1, k2, n0, sigma)
    write(unit=1, fmt=*) sigma(1, 1)
    write(unit=2, fmt=*) F(1, 1)
  enddo
  close(unit=1)
  close(unit=2)
end

! function one_d_ogden_an(n_order, mus, alphas) result(sig)
!   double precision, intent(in) :: mus(n_order), alphas(n_order)
!   INTEGER, intent(in) :: n_order
!   double precision :: sig
!
!   d = mat(1,1) + mat(2,2) + mat(3,3)
! end function tr
