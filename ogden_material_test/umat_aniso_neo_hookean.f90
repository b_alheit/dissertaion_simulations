SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD, &
  RPL,DDSDDT,DRPLDE,DRPLDT, &
  STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,CMNAME, &
  NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT, &
  CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,JSTEP,KINC)

INCLUDE 'ABA_PARAM.INC'

  CHARACTER*80 CMNAME

  DIMENSION STRESS(NTENS),STATEV(NSTATV), &
  DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS), &
  STRAN(NTENS),DSTRAN(NTENS),TIME(2),PREDEF(1),DPRED(1), &
  PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3), &
  JSTEP(4)

  double precision :: c_stress(3,3), n0(3, 1), eps, k1, k2, det, G, D
!		     ELASTIC USER SUBROUTINE
  PARAMETER (ONE=1.0D0, TWO=2.0D0)
  G=PROPS(1)
  D=PROPS(2)
  k1=PROPS(3)
  k2=PROPS(4)
  n0 = 0
  n0(1,1) = 1.0
  eps = 1.0e-8


  call cauchy_stress(DFGRD1, G, D, k1, k2, n0, c_stress)
  call matrix_to_voignt(c_stress, STRESS)

  call approximate_C(DFGRD1, G, D, k1, k2, n0, DDSDDE, eps)
  print*, '********************'
  print*, 'F mat'
  call print_matrix(DFGRD1, 3, 3)
  print*,
  print*, 'jacobian = ',det(DFGRD1)
  print*, 'G = ',G
  print*, 'D = ',D
  print*, 'k1 = ',k1
  print*, 'k2 = ',k2
  print*,
  print*, 'Stress'
  call print_matrix(c_stress, 3, 3)
  print*, '********************'
  print*,

  RETURN
  END



subroutine cauchy_stress(F, G, D, k1, k2, n0, sigma)
   double precision :: F(3,3), G, D, k1, k2, n0(3,1)
   double precision :: b_bar(3,3), C_bar(3,3), jacobian, I4(1, 1), psi4
   double precision :: sigma(3,3), Id(3,3)
   double precision :: det, tr, heaviside

   call identity(Id, 3)
   jacobian = det(F)
   b_bar = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))
   !C_bar = matmul(transpose(F), F) * (jacobian ** (-2.0/3.0))
   C_bar = matmul(transpose(F), F)
   I4 = matmul(transpose(n0), matmul(C_bar, n0))
   psi4 = heaviside(I4(1, 1)-1) * (I4(1, 1)-1) * k1 * exp(k2 * (I4(1, 1)-1)**2)
   !psi4 = (I4(1, 1)-1) * k1 * exp(k2 * (I4(1, 1)-1)**2)
   print*, 'n0 ', n0
   print*, 'C_bar '
   call print_matrix(c_bar, 3, 3 )
   print*, 'psi4 ', psi4
   print*, 'I4 ', I4
   print*, 'k1 ', k1
   print*, 'k2 ', k2

   sigma = (2.0/jacobian) * G * (b_bar - (1.0/3.0) *tr(b_bar) * Id) + (2.0/D)*(jacobian - 1.0) *Id
   sigma = sigma + (2.0/jacobian) * psi4 * matmul(F, matmul(matmul(n0, transpose(n0)), transpose(F)))
end

subroutine kirchhoff_stress(F, G, D, k1, k2, n0, tau)
   double precision :: F(3,3), G, D, k1, k2, n0(3,1)
   double precision :: jacobian
   double precision :: det
   double precision :: tau(3,3)

   jacobian = det(F)
   call cauchy_stress(F, G, D, k1, k2, n0, tau)
   tau = jacobian * tau
end

subroutine approximate_C(F, G, D, k1, k2, n0, C, eps)
  double precision :: F(3,3), C(6,6), G, D, eps, k1, k2, n0(3,1)
  double precision :: F_hat(3,3), k_stress(6), k_stress_hat(6), k_stress_mat(3,3), jacobian
  double precision :: Id(3,3), det, es(3, 3, 1)
  integer :: is(6), js(6), i, i_mat, j_mat

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]
  call identity(Id, 3)
  jacobian = det(F)

  es = 0
  es(1, 1, 1) = 1
  es(2, 2, 1) = 1
  es(3, 3, 1) = 1


  call kirchhoff_stress(F, G, D, k1, k2, n0, k_stress_mat)
  call matrix_to_voignt(k_stress_mat, k_stress)

  do i=1,6
    i_mat = is(i)
    j_mat = js(i)

    ! print*, 'i, j: ', i_mat, ', ', j_mat
    ! print*, "ei tens ej"
    ! call print_matrix(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), 3, 3)
    ! print*, "ej tens ei"
    ! call print_matrix(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), 3, 3)

    F_hat = F + (eps/2.0)*(matmul(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), F) &
            + matmul(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), F))

    call kirchhoff_stress(F_hat, G, D, k1, k2, n0, k_stress_mat)
    call matrix_to_voignt(k_stress_mat, k_stress_hat)

    C(:, i) = (1.0/(jacobian * eps)) * (k_stress_hat - k_stress)
  enddo
end

function delta(i, j) result(d)
   integer, intent(in) :: i! input
   integer :: d
   ! integer             :: j ! output
   if (i==j)then
     d = 1
   else
     d = 0
   endif
end function delta

function det(mat) result(d)
  double precision, intent(in) :: mat(3,3)
  double precision :: d

  d = mat(1,1) * (mat(2,2)*mat(3,3) - mat(2, 3) * mat(3,2))
  d = d - mat(1,2) * (mat(2,1)*mat(3,3) - mat(2, 3) * mat(3,1))
  d = d + mat(1,3) * (mat(2,1)*mat(3,2) - mat(2, 2) * mat(3,1))
end function det

function tr(mat) result(d)
  double precision, intent(in) :: mat(3,3)
  double precision :: d

  d = mat(1,1) + mat(2,2) + mat(3,3)
end function tr

subroutine identity(in_mat, n)
  integer :: n, i
  double precision :: in_mat(n,n)
  in_mat = 0
  do i = 1,n
      in_mat(i,i) = 1.0
  enddo
end

subroutine matrix_to_voignt(in, out)
  double precision :: in(3,3), out(6)
  out(1) = in(1,1)
  out(2) = in(2,2)
  out(3) = in(3,3)

  out(4) = in(2,3)
  out(5) = in(1,3)
  out(6) = in(1,2)
end

subroutine voignt_to_matrix(in, out)
  double precision :: in(6), out(3, 3)

  out(1,1) = in(1)
  out(2,2) = in(2)
  out(3,3) = in(3)

  out(2,3) = in(4)
  out(1,3) = in(5)
  out(1,2) = in(6)

  out(3,2) = in(4)
  out(3,1) = in(5)
  out(2,1) = in(6)
end

function heaviside(x) result(d)
  double precision, intent(in) :: x
  double precision :: d, a

  a= 1.0
  d = 0.5*(sign(a,x)+1.0)
end function heaviside


subroutine print_matrix(mat, rows, collumns)
  integer :: rows, collumns
  double precision :: mat(rows, collumns)
  integer :: i, j

  do i =1,rows
      print*,mat(i,:)
  enddo
end

subroutine print_vector(mat, rows)
  integer :: rows
  double precision :: mat(rows)
  integer :: i

  do i =1,rows
      print*,mat(i)
  enddo
end
