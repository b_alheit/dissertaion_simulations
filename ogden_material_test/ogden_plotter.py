import numpy as np
import matplotlib.pyplot as plt
import os

title = 'Stress vs strech in the same direction'
x_axis = '$\lambda_i$'
y_axis = '$\sigma_{ii}$'

plots = [('aniso_test_stretch.txt', 'aniso_test_stress.txt', "isotropic Ogden Model", 1, 'black')]
         # ('nh stretch1c.txt', 'nh stress11c.txt', "Direction 1 - anisotropic - compressive fibres", 1.5, 'orange'),
         # ('nh stretch1i.txt', 'nh stress11i.txt', "Direction 1 - isotropic", 5, 'green'),
         # ('nh stretch2.txt', 'nh stress22.txt', "Direction 2 - anisotropic - compressive fibres", 3, 'red'),
         # ('og stretch1i.txt', 'og stress11i.txt', "Ogden - isotropic", 1, 'black')]


def load_data(file_name):
    data = np.loadtxt(fname=file_name,
                        dtype=str,
                        delimiter='\n ')
    return data[data != ''].astype(np.float)

for i in range(len(plots)):
    x = load_data(plots[i][0])
    y = load_data(plots[i][1])
    name = plots[i][2]
    plt.plot(x, y, label=name, linewidth=plots[i][3], color=plots[i][4])

plt.xlabel(x_axis)
plt.ylabel(y_axis)
plt.title(title)
plt.legend()
plt.grid()
# os.spawnl(os.P_NOWAIT, plt.show())
plt.show()
