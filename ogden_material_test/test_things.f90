program test
  double precision :: n(3), C(3, 3), hold(3, 3, 1),a(3), b(3), res(3, 3)

  a = 0
  b = 0
  a(1) = 1
  b(1) = 1
  b(2) = 1
  b = 1

  C = 0
  C(1, 1) = 1
  C(2, 2) = 1
  C(3, 3) = 1
  C(1, 3) = 1

  a = matmul(c,b)

  ! hold = 0
  ! hold(1, :, :) = C(1)
  ! hold(2, :, :) = C(1:3, 2)
  ! hold(3, :, :) = C(1:3, 3)

  ! n = 0
  ! n(1) = 1.0
  ! res = spread(a(1:3),dim=2,ncopies=3)*spread(b(1:3),dim=1,ncopies= 3)
  print*, 'b'
  call print_vector(b, 3)
  print*, 'c'
  call print_matrix(c, 3, 3)
  print*, 'a'
  call print_vector(a, 3)
  print*, 'b*Cb'
  print*, dot_product(b,matmul(c,b))

end
