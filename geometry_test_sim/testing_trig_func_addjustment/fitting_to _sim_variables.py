import numpy as np
import matplotlib.pyplot as plt
import math

t = 4

dist = t/2

pro_height = 20
pro_thick = 4
depth_of_section = 40
n_points_path = 160

path_height = pro_height / 2
path_func = lambda x: path_height / 2.0 * math.sin(x * math.pi / pro_thick - depth_of_section * math.pi / (2 * pro_thick) + 3 * math.pi / 2) + path_height / 2.0

func = lambda x: np.sin(x)

def top_adjust(x, y, theta):
    angle_deg = 90 + 45 * math.sin(theta-math.pi/2)
    angle_rad = math.pi/2 + math.pi/4 * math.sin(theta-math.pi/2)
    X = x - dist * math.cos(angle_rad)
    Y = y + dist * math.sin(angle_rad)
    return X, Y


def bottom_adjust(x, y, theta):
    angle_deg = 90 + 45 * math.sin(theta-math.pi/2)
    angle_rad = math.pi/2 + math.pi/4 * math.sin(theta-math.pi/2)
    X = x + dist * math.cos(angle_rad)
    Y = y - dist * math.sin(angle_rad)
    return X, Y

path_points = []
top_adjusted_points = []

for i in range(n_points_path):
    x = depth_of_section*(i/(n_points_path-1.0)-1.0/2.0)
    y = path_func(x)
    theta = x * math.pi / pro_thick - depth_of_section * math.pi / (2 * pro_thick) + 3 * math.pi / 2
    path_points.append([x, y])
    X1, Y1 = top_adjust(x, y, theta)
    top_adjusted_points.append([X1, Y1])
    check_dist = math.sqrt((x - X1)**2 + (y-Y1)**2)
    if math.fabs(check_dist-dist) > 1.0e-10:
    # if True:
        print("Distance discrepancy: ", math.fabs(check_dist-dist))


# x = np.linspace(0, 10, 500)
# y = func(x)
#
# X1, Y1 = top_adjust(x, y, x)
# X2, Y2 = bottom_adjust(x, y, x)

path_points = np.array(path_points)
top_adjusted_points = np.array(top_adjusted_points)


plt.plot(path_points[:, 0], path_points[:, 1], label="original")
plt.plot(top_adjusted_points[:, 0], top_adjusted_points[:, 1], label="top adjusted")
# plt.plot(X2, Y2, label="bottom adjusted")
plt.grid()
plt.legend()
plt.show()
