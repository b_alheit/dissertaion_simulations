import numpy as np
import matplotlib.pyplot as plt

# t = 0
# t = 2.5
t = 2.0
exp=2.0
rate = 1
d = t/2
# d = 0
left_surface = -10
right_surface = 10
d_vary = 0.5
T = (np.pi*rate)
n_s_pts = int(np.abs(left_surface/T))
x_peak = T * n_s_pts
r = right_surface - x_peak
print(T)
print(n_s_pts)
print(x_peak)
print(r)
# d = d_vary
# d_vary = d*0
# dist = lambda x: d * np.abs(np.sin(rate*x)) + d_vary * np.abs(np.sin(rate*x + np.pi/2))
dist = lambda x: d
# dist = 0
# col_trans = 2
col_trans = 0
amp = 3
func = lambda x: amp*np.sin(rate*x + np.pi/2)
# func = lambda x: (1/(1+np.abs(x)))*3*np.sin(5*x)
# func = lambda x: (1/(1+np.abs(x)))**(-1)*3*np.sin(5*x)
# func = lambda x: np.e**(-x/6)*3*np.sin(5*x)
# func = lambda x: np.abs(x)*3*np.sin(5*x)

def switch(x,switch_position, width_scale):
    return 1/(1+np.e**(-width_scale*(x-switch_position)))


def top_adjust(x, y, mult, place, width, theta, exp):
    angle_deg = 90 + 45 * np.sin(theta-np.pi/2)
    # angle_rad = np.pi/2 + np.pi/4 * np.sin(theta-np.pi/2)
    angle_rad = np.pi/2 - np.pi/4 * np.cos(theta)
    # X = x - dist(x) * np.cos(angle_rad)
    X = x - dist(x) * np.sign(np.cos(angle_rad)) * np.abs(np.cos(angle_rad))**exp
    sig_func = switch(-X, place, width)+switch(X, place, width)
    # Y = (y + dist * np.sin(angle_rad) + col_trans/2)*(1-sig_func) + mult*(np.abs(X))*sig_func
    Y = (y + dist(x) * np.sin(angle_rad) + col_trans/2)
    return X, Y


def bottom_adjust(x, y, mult, place, width, theta, exp):
    angle_deg = 90 + 45 * np.sin(theta-np.pi/2)
    angle_rad = np.pi/2 + np.pi/4 * np.sin(theta-np.pi/2)
    # X = x + dist(x) * np.cos(angle_rad)
    X = x + dist(x) * np.sign(np.cos(angle_rad)) * np.abs(np.cos(angle_rad))**exp
    # sig_func = switch(-X, place, width)+switch(X, place, width)
    # plt.plot(X, sig_func)
    # plt.plot(X, 1-sig_func)
    # plt.show()
    # Y = (y - dist * np.sin(angle_rad) -col_trans/2)*(1-sig_func) - mult*(np.abs(X))*sig_func
    Y = (y - dist(x) * np.sin(angle_rad) -col_trans/2)
    other = - mult*500*(switch(-x, place, width)+switch(x, place, width))
    org = (y - dist(x) * np.sin(angle_rad) -col_trans/2)*(1-(switch(-x, place, width)))
    return X, Y, other, org


x = np.linspace(-11, 11, 500)
y = func(x)

mult = 0.5
place = 8
sharpness = 0.6

X1, Y1 = top_adjust(x, y, mult, place, sharpness, rate*x+ np.pi/2, exp)
X2, Y2, ot, org = bottom_adjust(x, y, mult, place, sharpness, rate*x+ np.pi/2, exp)

plt.plot(x, y, label="Imaginary center")
plt.plot(X1, Y1, label="Top surface")
plt.plot(X2, Y2, label="Bottom surface")
plt.vlines([left_surface, right_surface], -(amp+t+col_trans)*1.05, (amp+t+col_trans)*1.05)
# plt.plot(X2, ot)
# plt.plot(X2, org)
# plt.plot(x, 1-(switch(-x, 5, 3)+switch(x, 5, 3)))
plt.grid()
plt.legend()
# plt.savefig('../../../../Meetings/dissertation_meetings/2. _model_geometry_and_implimenting_ogden/slides/figures/demo_col_func.png')
# plt.savefig('../../../../Meetings/dissertation_meetings/2. _model_geometry_and_implimenting_ogden/slides/figures/demo_col_func_problem.png')
# plt.savefig('../../../../Meetings/dissertation_meetings/2. _model_geometry_and_implimenting_ogden/slides/figures/col_pure_translation.png')
# plt.savefig('../../../../Meetings/dissertation_meetings/2. _model_geometry_and_implimenting_ogden/slides/figures/col_compromise.png')
# plt.savefig('../../../../Meetings/dissertation_meetings/2. _model_geometry_and_implimenting_ogden/slides/figures/possibility_5.png')
plt.show()