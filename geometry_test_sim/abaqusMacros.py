# -*- coding: mbcs -*-
# Do not delete the following import lines
from abaqus import *
from abaqusConstants import *
from helpful_functions import *
import __main__
import math


def new_bone_args(activate_breakpoints,
    part_name,
    depth_of_section,
    horizontal_section_length,
    radius_of_curvature,
    right_thickness,
    left_thickness,
    protrusion_length):

    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    # activate_breakpoints = False
    # part_name = "bone2"
    # depth_of_section = 50.0
    # horizontal_section_length = 50.0
    # radius_of_curvature = 100.0
    # right_thickness = 15.0
    # left_thickness = 10.0
    # protrusion_length = 15.0

    # activate_breakpoints = False
    # part_name = "bone1"
    # depth_of_section = 60.0
    # horizontal_section_length = 75.0
    # radius_of_curvature = 180.0
    # right_thickness = 10.0
    # left_thickness = 5.0
    # protrusion_length = 25.0

    print(part_name)
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=200.0)
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=STANDALONE)

    # Creating y-axis
    y_axis = s1.ConstructionLine(point1=(0.0, 0.0), point2=(0.0, 10.0))  # g2
    y_axis_fix = s1.FixedConstraint(entity=y_axis)

    # Creating x-axis
    x_axis = s1.ConstructionLine(point1=(0.0, 0.0), point2=(10.0, 0.0))  # g3
    x_axis_fix = s1.FixedConstraint(entity=x_axis)

    # Creating line parallel to x-axis
    cline_x_axis_parallel = s1.ConstructionLine(point1=(0.0, -10.0), point2=(5.0, -10.0))  # g4
    s1.ParallelConstraint(entity1=cline_x_axis_parallel, entity2=x_axis)
    s1.DistanceDimension(entity1=cline_x_axis_parallel, entity2=x_axis, textPoint=(33.7514762878418, -3.71352958679199), value=right_thickness)

    arc_top, v_arc_top_center, v_arc_top_point1, v_arc_top_point2  = make_ArcByCenterEnds(s1, center=(-5.0, -28.75), point1=(-5.0, -5.0), point2=(-28.75, -23.75), direction=COUNTERCLOCKWISE)
    s1.CoincidentConstraint(entity1=v_arc_top_center, entity2=y_axis)
    s1.CoincidentConstraint(entity1=v_arc_top_point1, entity2=x_axis)
    s1.CoincidentConstraint(entity1=v_arc_top_point1, entity2=y_axis)
    s1.HorizontalDimension(vertex1=v[0], vertex2=v[1], textPoint=(-10.8330364227295, 18.8702774047852), value=horizontal_section_length)
    s1.VerticalDimension(vertex1=v[2], vertex2=v[0], textPoint=(64.4481735229492, -15.0), value=radius_of_curvature)

    arc_bottom, v_arc_bottom_center, v_arc_bottom_point1, v_arc_bottom_point2  = make_ArcByCenterEnds(s1, center=(-5.0, -35.0), point1=(-5.0, -20.0), point2=(-21.25, -32.5), direction=COUNTERCLOCKWISE)
    s1.CoincidentConstraint(entity1=v_arc_bottom_center, entity2=y_axis)
    s1.CoincidentConstraint(entity1=v_arc_bottom_point1, entity2=y_axis)
    s1.CoincidentConstraint(entity1=v_arc_bottom_point1, entity2=cline_x_axis_parallel)

    line_left, line_left_point1, line_left_point2 = make_Line(s1, point1=(-35.0, -25.0), point2=(-25.0, -32.5), message="line left")
    s1.CoincidentConstraint(entity1=line_left_point1, entity2=v_arc_top_point2)
    s1.CoincidentConstraint(entity1=line_left_point2, entity2=v_arc_bottom_point2)
    s1.PerpendicularConstraint(entity1=line_left, entity2=arc_bottom)
    s1.ObliqueDimension(vertex1=line_left_point1, vertex2=line_left_point2, textPoint=(-46.3366470336914, -46.5077590942383), value=left_thickness)

    line_bottom, dummy, v_line_bottom_point2 = make_Line(s1, point1=(0.0, -right_thickness), point2=(10.0, -right_thickness), message="line bottom")
    break_point(activate_breakpoints, "big shaq1")
    s1.CoincidentConstraint(entity1=line_bottom, entity2=cline_x_axis_parallel)
    break_point(activate_breakpoints, "big shaq1.1")
    line_right, dummy, dummy = make_Line(s1, point1=(10.0, -right_thickness), point2=(10.0, 0.0), message="line right")
    break_point(activate_breakpoints, "big shaq2")
    s1.VerticalConstraint(entity=line_right, addUndoState=False)
    break_point(activate_breakpoints, "big shaq2.1")
    line_top, dummy, dummy = make_Line(s1, point1=(10.0, 0.0), point2=(0.0, 0.0), message="line top")
    break_point(activate_breakpoints, "big shaq3")
    s1.CoincidentConstraint(entity1=line_top, entity2=x_axis)
    break_point(activate_breakpoints, "big shaq3.1")

    s1.HorizontalDimension(vertex1=v_arc_bottom_point1, vertex2=v_line_bottom_point2, textPoint=(6.18564033508301, -22.5884265899658), value=protrusion_length)
    break_point(activate_breakpoints, "big shaq9")

    session.viewports['Viewport: 1'].view.setValues(nearPlane=163.685, farPlane=213.438, width=253.528, height=138.692, cameraPosition=(-18.3291, 29.1575, 188.562), cameraTarget=(-18.3291, 29.1575, 0))
    p = mdb.models['Model-1'].Part(name=part_name, dimensionality=THREE_D, type=DEFORMABLE_BODY)
    p = mdb.models['Model-1'].parts[part_name]
    p.BaseSolidExtrude(sketch=s1, depth=depth_of_section)
    s1.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    del mdb.models['Model-1'].sketches['__profile__']


def all_together():

    activate_breakpoints = False
    left_bone_name = 'left-bone'
    right_bone_name = 'right-bone'
    col_part_name = 'collagen-block'
    depth_of_section = 20.0
    horizontal_section_length = 50.0
    radius_of_curvature = 120.0
    right_thickness = 15.0
    left_thickness = 10.0
    protrusion_length = 20.0
    protrusion_width = 3
    collagen_thickness = 1.0
    n_points = 125
    n_path_points = n_points
    collagen_top_thickness = 3.0
    collagen_translate = 2.0
    pre_cut_col_thickness = 3 * (protrusion_length + collagen_thickness + collagen_top_thickness)
    flat_length = protrusion_length/2 + collagen_thickness + collagen_translate

    new_bone_args(activate_breakpoints,
        left_bone_name,
        depth_of_section,
        horizontal_section_length,
        radius_of_curvature,
        right_thickness,
        left_thickness,
        protrusion_length)
    create_left_bone_protrusions_args(left_bone_name, protrusion_length,
        protrusion_width, n_points, depth_of_section, right_thickness,
        protrusion_length, collagen_thickness, False)

    new_bone_args(activate_breakpoints,
        right_bone_name,
        depth_of_section,
        horizontal_section_length,
        radius_of_curvature,
        right_thickness,
        left_thickness,
        protrusion_length)
    create_right_bone_protrusions_args(right_bone_name, protrusion_length,
        protrusion_width, n_points, depth_of_section, right_thickness,
        protrusion_length, collagen_thickness, False)

    create_col_block_args(col_part_name, right_thickness, pre_cut_col_thickness,
        depth_of_section)
    col_block_right_cut_args(col_part_name, protrusion_length, protrusion_width,
        n_path_points, depth_of_section, right_thickness, protrusion_length, collagen_thickness, pre_cut_col_thickness, collagen_translate, False)
    col_block_left_cut_args(col_part_name, protrusion_length, protrusion_width,
        n_path_points, depth_of_section, right_thickness, protrusion_length, collagen_thickness, pre_cut_col_thickness, collagen_translate, False)
    collagen_top_args(col_part_name, depth_of_section, collagen_top_thickness,
        radius_of_curvature, horizontal_section_length, right_thickness, flat_length, protrusion_length, False)




def create_bones():

    activate_breakpoints = False
    left_bone_name = 'left-bone'
    right_bone_name = 'right-bone'
    col_part_name = 'collagen'
    depth_of_section = 40.0
    # depth_of_section = 50.0
    horizontal_section_length = 50.0
    radius_of_curvature = 60.0
    right_thickness = 15.0
    left_thickness = 10.0
    protrusion_length = 40.0
    protrusion_width = 4.0
    collagen_thickness = 2.0
    pre_cut_col_thickness = 100
    n_points = 250

    new_bone_args(activate_breakpoints,
        left_bone_name,
        depth_of_section,
        horizontal_section_length,
        radius_of_curvature,
        right_thickness,
        left_thickness,
        protrusion_length)
    create_left_bone_protrusions_args(left_bone_name, protrusion_length, protrusion_width, n_points, depth_of_section, right_thickness, protrusion_length, collagen_thickness, False)

    new_bone_args(activate_breakpoints,
        right_bone_name,
        depth_of_section,
        horizontal_section_length,
        radius_of_curvature,
        right_thickness,
        left_thickness,
        protrusion_length)
    create_right_bone_protrusions_args(right_bone_name, protrusion_length, protrusion_width, n_points, depth_of_section, right_thickness, protrusion_length, collagen_thickness, False)

    # create_col_block_args(col_part_name, right_thickness, pre_cut_col_thickness, depth_of_section)
    # col_block_right_cut_args(col_part_name, 20, protrusion_width, 160, depth_of_section, right_thickness, protrusion_length, collagen_thickness, False)
    # col_block_left_cut_args(col_part_name, 20, protrusion_width, 160, depth_of_section, right_thickness, protrusion_length, collagen_thickness, False)


def create_right_bone_protrusions_args(part_name, pro_height, pro_thick, n_points_path, depth_of_section, right_thickness, protrusion_length, col_thick, activate_breakpoints=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    print(part_name)

    break_point(activate_breakpoints, "1")
    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    # t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[16], sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0, depth_of_section/2)) # Need to parameterise sketch height
    # t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[17],
    #     sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(20.0, 0.0,
    #     20.0))
    t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[17],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0,
        depth_of_section/2))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__', sheetSize=104.4, gridSpacing=2.61, transform=t)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)

    break_point(activate_breakpoints, "2")
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)

    # sketching path
    path_height = pro_height/2.0
    dist = col_thick/2.0
    # path_theta_func = lambda x: x*math.pi/pro_thick + math.pi
    # # path_theta_func = lambda x: x*math.pi/pro_thick - depth_of_section * math.pi/(2*pro_thick) + 3*math.pi/2 + math.pi
    # path_func = lambda x: path_height/2.0*math.sin(path_theta_func(x)) + path_height/2.0 + dist
    #
    # def bottom_adjust(x, y, theta):
    #     angle_rad = math.pi/2 + math.pi/4 * math.sin(theta-math.pi/2)
    #     X = x + dist * math.cos(angle_rad)
    #     Y = y - dist * math.sin(angle_rad)
    #     return X, Y
    #
    # path_points = []
    # for i in range(n_points_path):
    #     x = depth_of_section*(i/(n_points_path-1.0)-1.0/2.0)
    #     y = path_func(x)
    #     theta = path_theta_func(x)
    #     X, Y = bottom_adjust(x, y, theta)
    #     path_points.append((X, Y))
    #
    # path_points = tuple(path_points)
    #
    # s.Spline(points=path_points)

    # path_theta_func = lambda x: x*math.pi/pro_thick - math.pi
    path_theta_func = lambda x: x*math.pi/pro_thick - math.pi
    path_func = lambda x: path_height/2.0*math.cos(path_theta_func(x)) - path_height/2.0 - dist

    def bottom_adjust(x, y, theta):
        angle_rad = math.pi/2 + math.pi/4 * math.cos(theta-math.pi/2)
        X = x - dist * math.cos(angle_rad)
        Y = y + dist * math.sin(angle_rad)
        return X, Y

    path_points = []

    dx = depth_of_section / (n_points_path-1.0)
    x = 100*dx-depth_of_section/2
    X = 1
    while X >= -depth_of_section/2:
        x -= dx
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)

    path_points.append((X, Y))

    while X <= depth_of_section/2:
        x += dx
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)
        path_points.append((X, Y))

    # for i in range(n_points_path):
    #     x = depth_of_section*(i/(n_points_path-1.0)-1.0/2.0)
    #     y = path_func(x)
    #     theta = path_theta_func(x)
    #     X, Y = bottom_adjust(x, y, theta)
    #     path_points.append((X, Y))

    path_points = tuple(path_points)

    break_point(activate_breakpoints, "in")
    print("path_points")
    print(path_points)
    s.Spline(points=path_points)

    break_point(activate_breakpoints, "3")
    s.unsetPrimaryObject()
    s.unsetPrimaryObject()

    p = mdb.models['Model-1'].parts[part_name]

    f1, e1 = p.faces, p.edges
    # t = p.MakeSketchTransform(sketchPlane=f1[7], sketchUpEdge=e1[17],
    #     sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0,
    #     -right_thickness/2.0, 0.0))
    # t = p.MakeSketchTransform(sketchPlane=f1[6], sketchUpEdge=e1[16],
    #     sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(-11.770648,
    #     -14.711809, 40.0))
    t = p.MakeSketchTransform(sketchPlane=f1[6], sketchUpEdge=e1[16],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0,
        -right_thickness/2.0, depth_of_section))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=137.79, gridSpacing=3.44, transform=t)
    g1, v1, d1, c1 = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)

    break_point(activate_breakpoints, "4")
    p = mdb.models['Model-1'].parts[part_name]

    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)

    # cut_theta_func = lambda x: x*math.pi/pro_thick + 3*math.pi/2 + math.pi
    # cut_theta_func = path_theta_func
    # cut_func = lambda x: path_height/2.0*math.sin(cut_theta_func(x)) + path_height/2.0 + dist
    #
    # cut_points = []
    # n_points_cut = n_points_path
    # dx = right_thickness / (n_points_cut-1.0)
    # x = -right_thickness/2
    # X = 1
    # while X >= -right_thickness/2:
    #     x -= dx
    #     y = cut_func(x)
    #     theta = cut_theta_func(x)
    #     X, Y = bottom_adjust(x, y, theta)
    #
    # cut_points.append((X, Y))
    #
    # while X <= right_thickness/2:
    #     x += dx
    #     y = cut_func(x)
    #     theta = cut_theta_func(x)
    #     X, Y = bottom_adjust(x, y, theta)
    #     cut_points.append((X, Y))

    cut_theta_func = path_theta_func
    cut_func = path_func

    cut_points = []
    n_points_cut = n_points_path
    dx = right_thickness / (n_points_cut-1.0)
    x = -right_thickness/2
    X = 1
    while X >= -1.25*right_thickness/2:
        x -= dx
        y = cut_func(x)
        theta = cut_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)

    cut_points.append((X, Y))

    while X <= 1.25*right_thickness/2:
        x += dx
        y = cut_func(x)
        theta = cut_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)
        cut_points.append((X, Y))



    start_point, end_point  = cut_points[0], cut_points[len(cut_points)-1]
    cut_points = tuple(cut_points)

    print("cut points, soft code")
    print(cut_points)

    break_point(activate_breakpoints, "4")

    s1.Spline(points=cut_points)

    break_point(activate_breakpoints, "4")

    point1 = start_point
    point2 = (start_point[0], start_point[1] - protrusion_length - pro_height)
    point3 = (end_point[0], point2[1])
    point4 = end_point

    break_point(activate_breakpoints, "4")
    s1.Line(point1=point1, point2=point2)
    s1.VerticalConstraint(entity=g1[9], addUndoState=False)
    break_point(activate_breakpoints, "4")
    s1.Line(point1=point2, point2=point3)
    s1.HorizontalConstraint(entity=g1[10], addUndoState=False)
    s1.PerpendicularConstraint(entity1=g1[9], entity2=g1[10], addUndoState=False)
    break_point(activate_breakpoints, "4")
    s1.Line(point1=point3, point2=point4)
    s1.VerticalConstraint(entity=g1[11], addUndoState=False)
    s1.PerpendicularConstraint(entity1=g1[10], entity2=g1[11], addUndoState=False)
    break_point(activate_breakpoints, "4")

    s1.unsetPrimaryObject()

    break_point(activate_breakpoints, "5")
    p = mdb.models['Model-1'].parts[part_name]
    break_point(activate_breakpoints, "5.1")

    f, e = p.faces, p.edges
    break_point(activate_breakpoints, "5.2")
    # p.CutSweep(pathPlane=f[5], pathUpEdge=e[16], sketchPlane=f[7],
    #     sketchUpEdge=e[17], pathOrientation=RIGHT, path=s,
    #     sketchOrientation=RIGHT, profile=s1, profileNormal=ON)
    p.CutSweep(pathPlane=f[5], pathUpEdge=e[17], sketchPlane=f[6],
        sketchUpEdge=e[16], pathOrientation=RIGHT, path=s,
        sketchOrientation=RIGHT, profile=s1, profileNormal=ON)
    break_point(activate_breakpoints, "5.3")
    del mdb.models['Model-1'].sketches['__sweep__']
    del mdb.models['Model-1'].sketches['__profile__']


def create_left_bone_protrusions_args(part_name, pro_height, pro_thick, n_points_path, depth_of_section, right_thickness, protrusion_length, col_thick, activate_breakpoints=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    print(part_name)

    def top_adjust(x, y, theta):
        angle_rad = math.pi/2 + math.pi/4 * math.sin(theta-math.pi/2)
        X = x - dist * math.cos(angle_rad)
        Y = y + dist * math.sin(angle_rad)
        return X, Y

    break_point(activate_breakpoints, "1")
    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[16], sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0, depth_of_section/2)) # Need to parameterise sketch height
    s = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__', sheetSize=104.4, gridSpacing=2.61, transform=t)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)

    break_point(activate_breakpoints, "2")
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)

    # sketching path
    path_height = pro_height/2.0
    dist = col_thick/2.0
    # path_theta_func = lambda x: x*math.pi/pro_thick - depth_of_section * math.pi/(2*pro_thick) + 3*math.pi/2
    # path_theta_func = lambda x: x*math.pi/pro_thick
    # path_func = lambda x: path_height/2.0*math.sin(path_theta_func(x)) + path_height/2.0 + dist
    #
    # def bottom_adjust(x, y, theta):
    #     angle_rad = math.pi/2 + math.pi/4 * math.sin(theta-math.pi/2)
    #     X = x + dist * math.cos(angle_rad)
    #     Y = y - dist * math.sin(angle_rad)
    #     return x, y
    #     # return X, Y
    #
    # path_points = []
    # for i in range(n_points_path):
    #     x = depth_of_section*(i/(n_points_path-1.0)-1.0/2.0)
    #     y = path_func(x)
    #     theta = path_theta_func(x)
    #     X, Y = bottom_adjust(x, y, theta)
    #     path_points.append((X, Y))
    #
    # path_points = tuple(path_points)
    #
    # s.Spline(points=path_points)

    path_theta_func = lambda x: x*math.pi/pro_thick - math.pi
    path_func = lambda x: path_height/2.0*math.cos(path_theta_func(x)) + path_height/2.0 + dist

    def bottom_adjust(x, y, theta):
        # angle_rad = math.pi/2 + math.pi/4 * math.sin(theta-math.pi/2)
        angle_rad = math.pi/2 + math.pi/4 * math.cos(theta-math.pi/2)
        X = x + dist * math.cos(angle_rad)
        Y = y - dist * math.sin(angle_rad)
        return X, Y

    path_points = []

    dx = depth_of_section / (n_points_path-1.0)
    x = 100*dx-depth_of_section/2
    X = 1
    while X >= -depth_of_section/2:
        x -= dx
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)

    path_points.append((X, Y))

    while X <= depth_of_section/2:
        x += dx
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)
        path_points.append((X, Y))

    # for i in range(n_points_path):
    #     x = depth_of_section*(i/(n_points_path-1.0)-1.0/2.0)
    #     y = path_func(x)
    #     theta = path_theta_func(x)
    #     X, Y = bottom_adjust(x, y, theta)
    #     path_points.append((X, Y))

    path_points = tuple(path_points)

    break_point(activate_breakpoints, "in")
    print("path_points")
    print(path_points)
    s.Spline(points=path_points)

    break_point(activate_breakpoints, "3")
    s.unsetPrimaryObject()
    s.unsetPrimaryObject()

    p = mdb.models['Model-1'].parts[part_name]

    f1, e1 = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f1[7], sketchUpEdge=e1[17],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0,
        -right_thickness/2.0, 0.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=137.79, gridSpacing=3.44, transform=t)
    g1, v1, d1, c1 = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)

    break_point(False, "4")
    p = mdb.models['Model-1'].parts[part_name]

    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)

    # # cut_theta_func = lambda x: x*math.pi/pro_thick + 3*math.pi/2
    # cut_theta_func = path_theta_func
    # # cut_func = lambda x: path_height/2.0*math.sin(cut_theta_func(x)) + path_height/2.0
    # cut_func = lambda x: path_height/2.0*math.sin(cut_theta_func(x)) + path_height/2.0 + dist
    # # cut_func = lambda x: path_height/2.0*math.sin(cut_theta_func(x)) + dist
    # # cut_func = path_func - path_height/2.0
    #
    #
    # cut_points = []
    # n_points_cut = n_points_path
    # dx = right_thickness / (n_points_cut-1.0)
    # x = -right_thickness/2
    # X = 1
    # while X >= -right_thickness/2:
    #     x -= dx
    #     y = cut_func(x)
    #     theta = cut_theta_func(x)
    #     X, Y = bottom_adjust(x, y, theta)
    #
    # cut_points.append((X, Y))
    #
    # while X <= right_thickness/2:
    #     x += dx
    #     y = cut_func(x)
    #     theta = cut_theta_func(x)
    #     X, Y = bottom_adjust(x, y, theta)
    #     cut_points.append((X, Y))
    #
    # start_point, end_point  = cut_points[0], cut_points[len(cut_points)-1]
    # cut_points = tuple(cut_points)

    cut_theta_func = path_theta_func
    cut_func = path_func

    cut_points = []
    n_points_cut = n_points_path
    dx = right_thickness / (n_points_cut-1.0)
    x = -right_thickness/2
    X = 1
    while X >= -1.25*right_thickness/2:
        x -= dx
        y = cut_func(x)
        theta = cut_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)

    cut_points.append((X, Y))

    while X <= 1.25*right_thickness/2:
        x += dx
        y = cut_func(x)
        theta = cut_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)
        cut_points.append((X, Y))

    start_point, end_point  = cut_points[0], cut_points[len(cut_points)-1]
    cut_points = tuple(cut_points)

    print("cut points, soft code")
    print(cut_points)

    break_point(activate_breakpoints, "4")

    s1.Spline(points=cut_points)

    break_point(activate_breakpoints, "4")

    point1 = start_point
    point2 = (start_point[0], start_point[1] + protrusion_length + pro_height)
    point3 = (end_point[0], point2[1])
    point4 = end_point

    break_point(activate_breakpoints, "4")
    s1.Line(point1=point1, point2=point2)
    s1.VerticalConstraint(entity=g1[9], addUndoState=False)
    break_point(activate_breakpoints, "4")
    s1.Line(point1=point2, point2=point3)
    s1.HorizontalConstraint(entity=g1[10], addUndoState=False)
    s1.PerpendicularConstraint(entity1=g1[9], entity2=g1[10], addUndoState=False)
    break_point(activate_breakpoints, "4")
    s1.Line(point1=point3, point2=point4)
    s1.VerticalConstraint(entity=g1[11], addUndoState=False)
    s1.PerpendicularConstraint(entity1=g1[10], entity2=g1[11], addUndoState=False)
    break_point(activate_breakpoints, "4")

    s1.unsetPrimaryObject()

    break_point(activate_breakpoints, "5")
    p = mdb.models['Model-1'].parts[part_name]

    f, e = p.faces, p.edges
    p.CutSweep(pathPlane=f[5], pathUpEdge=e[16], sketchPlane=f[7],
        sketchUpEdge=e[17], pathOrientation=RIGHT, path=s,
        sketchOrientation=RIGHT, profile=s1, profileNormal=ON)
    del mdb.models['Model-1'].sketches['__sweep__']
    del mdb.models['Model-1'].sketches['__profile__']


def create_protrusions_args(part_name, activate_breakpoints=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    print(part_name)
    break_point(activate_breakpoints, "1")
    p = mdb.models['Model-1'].parts[part_name]
    # p = part

    f, e = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[16],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(7.5, 0.0,
        25.0))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__', sheetSize=104.4,
        gridSpacing=2.61, transform=t)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)

    break_point(activate_breakpoints, "2")
    p = mdb.models['Model-1'].parts[part_name]
    # p = part

    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=95.3035,
        farPlane=136.351, width=68.3254, height=37.0445, cameraPosition=(
        9.17418, 104.403, 27.4335), cameraTarget=(9.17418, 0, 27.4335))
    s.Spline(points=((-26.1, -10.44), (-25.0, -7.5), (-20.88, -2.61), (-15.66,
        -7.5), (-11.745, -2.61), (-7.83, -7.5), (-1.9575, -2.61), (3.915,
        -7.5), (8.4825, -2.61), (14.355, -7.5), (19.575, -2.61), (25.0, -7.5),
        (26.7525, -10.44)))
    s.CoincidentConstraint(entity1=v[6], entity2=g[5], addUndoState=False)
    s.CoincidentConstraint(entity1=v[8], entity2=g[5], addUndoState=False)
    s.CoincidentConstraint(entity1=v[10], entity2=g[5], addUndoState=False)
    s.CoincidentConstraint(entity1=v[12], entity2=g[5], addUndoState=False)
    s.unsetPrimaryObject()
    s.unsetPrimaryObject()
    session.viewports['Viewport: 1'].view.setValues(nearPlane=154.201,
        farPlane=252.77, width=111.453, height=57.4797, cameraPosition=(
        25.7597, 70.8706, -157.96), cameraUpVector=(-0.957348, 0.135993,
        0.254932), cameraTarget=(-8.75845, -13.6674, 25.7201))

    break_point(activate_breakpoints, "3")
    p = mdb.models['Model-1'].parts[part_name]
    # p = part

    f1, e1 = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f1[7], sketchUpEdge=e1[17],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(-15.317594,
        -9.817616, 0.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=137.79, gridSpacing=3.44, transform=t)
    g1, v1, d1, c1 = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)

    break_point(activate_breakpoints, "4")
    p = mdb.models['Model-1'].parts[part_name]
    # p = part

    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=128.466,
        farPlane=197.13, width=67.2932, height=36.4848, cameraPosition=(
        -4.25671, -9.94841, -137.798), cameraTarget=(-4.25671, -9.94841, 0))
    s1.Spline(points=((-6.88, 11.18), (-5.182384, 15.317594), (-3.44, 20.64), (
        -0.86, 15.48), (0.86, 20.64), (3.44, 15.48), (5.16, 20.64), (6.88,
        15.48), (8.6, 20.64), (9.817616, 15.317594), (11.18, 12.04)))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=126.829,
        farPlane=198.766, width=82.3588, height=44.6531, cameraPosition=(
        -3.01219, -10.2038, -137.798), cameraTarget=(-3.01219, -10.2038, 0))
    s1.Line(point1=(-6.88, 11.18), point2=(-6.88, 33.54))
    s1.VerticalConstraint(entity=g1[9], addUndoState=False)
    s1.Line(point1=(-6.88, 33.54), point2=(11.18, 33.54))
    s1.HorizontalConstraint(entity=g1[10], addUndoState=False)
    s1.PerpendicularConstraint(entity1=g1[9], entity2=g1[10], addUndoState=False)
    s1.Line(point1=(11.18, 33.54), point2=(11.18, 12.04))
    s1.VerticalConstraint(entity=g1[11], addUndoState=False)
    s1.PerpendicularConstraint(entity1=g1[10], entity2=g1[11], addUndoState=False)
    s1.unsetPrimaryObject()

    break_point(activate_breakpoints, "5")
    p = mdb.models['Model-1'].parts[part_name]
    # p = part

    f, e = p.faces, p.edges
    p.CutSweep(pathPlane=f[5], pathUpEdge=e[16], sketchPlane=f[7],
        sketchUpEdge=e[17], pathOrientation=RIGHT, path=s,
        sketchOrientation=RIGHT, profile=s1, profileNormal=ON)
    del mdb.models['Model-1'].sketches['__sweep__']
    del mdb.models['Model-1'].sketches['__profile__']

#
# def create_collagen():
#     activate_breakpoints = False
#     left_bone_name = 'bone7'
#     right_bone_name = 'bone8'
#     depth_of_section = 40.0
#     # depth_of_section = 50.0
#     horizontal_section_length = 50.0
#     radius_of_curvature = 100.0
#     right_thickness = 15.0
#     left_thickness = 10.0
#     protrusion_length = 40.0
#     protrusion_width = 4
#     collagen_thickness = 1.0
#
#     create_collagen_args(protrusion_length, collagen_thickness, depth_of_section, right_thickness, radius_of_curvature, horizontal_section_length)
#
# def create_collagen_args(protrusion_length, col_thickness, depth_of_section,
#     right_thickness, right_radius, right_hor_dist):
#     import section
#     import regionToolset
#     import displayGroupMdbToolset as dgm
#     import part
#     import material
#     import assembly
#     import step
#     import interaction
#     import load
#     import mesh
#     import optimization
#     import job
#     import sketch
#     import visualization
#     import xyPlot
#     import displayGroupOdbToolset as dgo
#     import connectorBehavior
#     s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
#         sheetSize=200.0)
#     g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
#     s.setPrimaryObject(option=STANDALONE)
#
#     left_v_line = s.Line(point1=(-30.0, 20.0), point2=(-30.0, 0.0))
#     s.VerticalConstraint(entity=left_v_line, addUndoState=False)
#     left_line_point = v[len(v) - 2]
#
#     left_h_line = s.Line(point1=(-30.0, 0.0), point2=(0.0, 0.0))
#     s.HorizontalConstraint(entity=left_h_line, addUndoState=False)
#     s.PerpendicularConstraint(entity1=left_h_line, entity2=left_v_line, addUndoState=False)
#
#     right_h_line = s.Line(point1=(0.0, 0.0), point2=(30.0, 0.0))
#     s.HorizontalConstraint(entity=right_h_line, addUndoState=False)
#     s.ParallelConstraint(entity1=left_h_line, entity2=right_h_line, addUndoState=False)
#
#     right_v_line = s.Line(point1=(30.0, 0.0), point2=(30.0, 20.0))
#     s.VerticalConstraint(entity=right_v_line, addUndoState=False)
#     s.PerpendicularConstraint(entity1=right_h_line, entity2=right_v_line, addUndoState=False)
#     right_line_point = v[len(v) - 1]
#
#     s.FixedConstraint(entity=v[2])
#
#     s.EqualLengthConstraint(entity1=left_h_line, entity2=right_h_line)
#
#     s.HorizontalDimension(vertex1=v[1], vertex2=v[3], textPoint=(-0.365, -7.7), value=protrusion_length+col_thickness)
#
#     s.EqualLengthConstraint(entity1=left_v_line, entity2=right_v_line)
#     s.VerticalDimension(vertex1=v[0], vertex2=v[1], textPoint=(-40., 8.78), value=right_thickness)
#
#     rb_arc = s.ArcByCenterEnds(center=(protrusion_length+col_thickness/2, -20.0), point1=(protrusion_length+col_thickness/2, right_thickness), point2=(55.0, 2.5), direction=CLOCKWISE)
#     rb_arc_center, rb_arc_point1, rb_arc_point2 = v[len(v) - 1], v[len(v) - 3], v[len(v) - 2]
#     s.CoincidentConstraint(entity1=rb_arc_center, entity2=right_v_line)
#     s.CoincidentConstraint(entity1=right_line_point, entity2=rb_arc_point1)
#     s.VerticalDimension(vertex1=rb_arc_point1, vertex2=rb_arc_center, textPoint=(36.695671081543, -11.1204299926758), value=right_radius)
#     s.HorizontalDimension(vertex1=rb_arc_point1, vertex2=rb_arc_point2, textPoint=(55.0838088989258, 50.1389083862305), value=right_hor_dist)
#
#     break_point(True, "How we doing?")
#
#     rt_arc = s.ArcByCenterEnds(center=(25.0, -40.0), point1=(25.0, 30.0), point2=(68.75, 15.0), direction=CLOCKWISE)
#     rt_arc_center, rt_arc_point1, rt_arc_point2 = v[len(v) - 1], v[len(v) - 3], v[len(v) - 2]
#     s.CoincidentConstraint(entity1=rt_arc_point1, entity2=right_v_line)
#     s.CoincidentConstraint(entity1=rt_arc_center, entity2=right_v_line)
#
#     co_line_right = s.Line(point1=(40, 40), point2=(20, 20))
#     co_line_right_pt1, co_line_right_pt2 = v[len(v)-2], v[len(v)-1]
#     s.CoincidentConstraint(entity1=co_line_right_pt1, entity2=rb_arc_point2)
#     s.CoincidentConstraint(entity1=co_line_right_pt2, entity2=rt_arc_point2)
#     s.PerpendicularConstraint(entity1=co_line_right, entity2=rt_arc)
#     s.ObliqueDimension(vertex1=co_line_right_pt1, vertex2=co_line_right_pt2, textPoint=(77.6573638916016,
#         2.89506912231445), value=5.0)
#     s.VerticalDimension(vertex1=right_line_point, vertex2=rt_arc_point1, textPoint=(80.5519638061523,
#         24.2526359558105), value=5.0)
#
#     break_point(True, "Yano, well. And you?")
#
#     lb_arc = s.ArcByCenterEnds(center=(-20.0, -60.0), point1=(-25.0, 20.0), point2=(-60.0, 10.0), direction=COUNTERCLOCKWISE)
#     break_point(True, "What's happening here?")
#     lb_arc_center, lb_arc_point1, lb_arc_point2 = v[len(v) - 1], v[len(v) - 3], v[len(v) - 2]
#     s.CoincidentConstraint(entity1=lb_arc_center, entity2=left_v_line)
#     s.CoincidentConstraint(entity1=left_line_point, entity2=lb_arc_point1)
#     break_point(True, "And now?")
#
#     s.VerticalDimension(vertex1=lb_arc_center, vertex2=lb_arc_point1, textPoint=(-10.8682689666748, -28.1039009094238), value=right_radius)
#     s.HorizontalDimension(vertex1=lb_arc_point1, vertex2=lb_arc_point2, textPoint=(-42.4156646728516, 50.5444641113281), value=right_hor_dist)
#     break_point(True, "constrained?")
#
#
#     lt_arc = s.ArcByCenterEnds(center=(-25.0, -32.5), point1=(-25.0, 36.25), point2=(-67.5, 18.75), direction=COUNTERCLOCKWISE)
#     lt_arc_center, lt_arc_point1, lt_arc_point2 = v[len(v) - 1], v[len(v) - 3], v[len(v) - 2]
#     s.CoincidentConstraint(entity1=lt_arc_center, entity2=left_v_line)
#     s.CoincidentConstraint(entity1=lt_arc_point1, entity2=left_v_line)
#
#     co_line_left = s.Line(point1=(-65.0, 9.28203230275509), point2=(-68.8855910346235, 20.420859777046))
#     co_line_left_pt1, co_line_left_pt2 = v[len(v)-2], v[len(v)-1]
#     s.CoincidentConstraint(entity1=co_line_left_pt1, entity2=lb_arc_point2)
#     s.CoincidentConstraint(entity1=co_line_left_pt2, entity2=lt_arc_point2)
#     s.PerpendicularConstraint(entity1=co_line_left, entity2=lt_arc)
#     s.ObliqueDimension(vertex1=co_line_left_pt1, vertex2=co_line_left_pt2, textPoint=(77.6573638916016, 2.89506912231445), value=5.0)
#     s.VerticalDimension(vertex1=left_line_point, vertex2=lt_arc_point1, textPoint=(80.5519638061523, 24.2526359558105), value=5.0)
#
#     break_point(True, "working?")
#
#     top_line = s.Line(point1=(-15.0, 37.5), point2=(11.25, 32.5))
#     top_line_pt1, top_line_pt2 = v[len(v)-2], v[len(v)-1]
#
#     s.CoincidentConstraint(entity1=lt_arc_point1, entity2=top_line_pt1)
#     s.CoincidentConstraint(entity1=rt_arc_point1, entity2=top_line_pt2)
#
#     break_point(True, "working?")
#
#     p = mdb.models['Model-1'].Part(name='collagen', dimensionality=THREE_D,
#         type=DEFORMABLE_BODY)
#     p = mdb.models['Model-1'].parts['collagen']
#     p.BaseSolidExtrude(sketch=s, depth=50.0)
#     s.unsetPrimaryObject()
#     p = mdb.models['Model-1'].parts['collagen']
#     session.viewports['Viewport: 1'].setValues(displayedObject=p)
#     del mdb.models['Model-1'].sketches['__profile__']
#
# def make_collagen():
#     # activate_breakpoints = False
#     # left_bone_name = 'bone7'
#     # right_bone_name = 'bone8'
#     # col_part_name = 'collagen-block'
#     # depth_of_section = 40.0
#     # horizontal_section_length = 50.0
#     # radius_of_curvature = 100.0
#     # right_thickness = 15.0
#     # left_thickness = 10.0
#     # protrusion_length = 40.0
#     # protrusion_width = 4
#     # collagen_thickness = 1.5
#     # pre_cut_col_thickness = 100
#     # n_path_points = 200
#     # collagen_top_thickness = 1.0
#
#     activate_breakpoints = False
#     left_bone_name = 'bone7'
#     right_bone_name = 'bone8'
#     col_part_name = 'collagen-block'
#     depth_of_section = 40.0
#     horizontal_section_length = 50.0
#     radius_of_curvature = 50.0
#     right_thickness = 8.0
#     left_thickness = 8.0
#     protrusion_length = 40.0
#     protrusion_width = 4.0
#     collagen_thickness = 2.0
#     pre_cut_col_thickness = 200
#     n_path_points = 250
#     collagen_top_thickness = 2.0
#     collagen_translate = 2.0
#     flat_length = protrusion_length/2 + collagen_thickness + collagen_translate
#
#
#     create_col_block_args(col_part_name, right_thickness, pre_cut_col_thickness, depth_of_section)
#     col_block_right_cut_args(col_part_name, protrusion_length, protrusion_width, n_path_points, depth_of_section, right_thickness, protrusion_length, collagen_thickness, pre_cut_col_thickness, collagen_translate, False)
#     col_block_left_cut_args(col_part_name, protrusion_length, protrusion_width, n_path_points, depth_of_section, right_thickness, protrusion_length, collagen_thickness, pre_cut_col_thickness, collagen_translate, False)
#     collagen_top_args(col_part_name, depth_of_section, collagen_top_thickness, radius_of_curvature, horizontal_section_length, right_thickness, flat_length, protrusion_length, True)
#
#
# def create_col_block():
#     right_bone_thickness = 15
#     pre_cut_col_thickness = 100
#     depth_of_section = 50
#     create_col_block_args(right_bone_thickness, pre_cut_col_thickness, depth_of_section)
#

def create_col_block_args(part_name, right_bone_thickness, pre_cut_col_thickness, depth_of_section):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=200.0)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=STANDALONE)
    s.rectangle(point1=(-pre_cut_col_thickness/2.0, -right_bone_thickness/2.0), point2=(pre_cut_col_thickness/2.0, right_bone_thickness/2.0))

    s.ObliqueDimension(vertex1=v[0], vertex2=v[1], textPoint=(-24.4589042663574, 14.7560958862305), value=right_bone_thickness)
    s.ObliqueDimension(vertex1=v[3], vertex2=v[0], textPoint=(10.9517517089844, -10.2439022064209), value=pre_cut_col_thickness)

    p = mdb.models['Model-1'].Part(name=part_name, dimensionality=THREE_D,
        type=DEFORMABLE_BODY)
    p = mdb.models['Model-1'].parts[part_name]
    p.BaseSolidExtrude(sketch=s, depth=depth_of_section)
    s.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    del mdb.models['Model-1'].sketches['__profile__']

#
# def col_block_right_cut():
#     col_block_right_cut_args()

def col_block_right_cut_args(part_name, pro_height, pro_thick, n_points_path, depth_of_section, right_thickness, protrusion_length, col_thick, pre_cut_col_thickness, collagen_translate, activate_breakpoints=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f[1], sketchUpEdge=e[6],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, right_thickness/2,
        depth_of_section/2.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__',
        sheetSize=231.51, gridSpacing=5.78, transform=t)
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)

    # s1.Spline(points=((-25.0, 0.0), (-17.34, 17.34), (-11.56, 0.0), (-5.78, 17.34),
    #     (0.0, 0.0), (5.78, 17.34), (11.56, 0.0), (17.34, 17.34), (25.0, 0.0)))


    # sketching path
    path_height = pro_height/2.0
    dist = col_thick/2.0
    # path_theta_func = lambda x: x*math.pi/pro_thick - depth_of_section * math.pi/(2*pro_thick) + 3*math.pi/2 + math.pi
    path_theta_func = lambda x: x*math.pi/pro_thick - math.pi
    path_func = lambda x: path_height/2.0*math.cos(path_theta_func(x)) + path_height/2.0

    def bottom_adjust(x, y, theta):
        # angle_rad = math.pi/2 + math.pi/4 * math.sin(theta-math.pi/2)
        angle_rad = math.pi/2 + math.pi/4 * math.cos(theta-math.pi/2)
        X = x + dist * math.cos(angle_rad)
        Y = y - dist * math.sin(angle_rad) - collagen_translate/2
        return X, Y

    path_points = []

    dx = depth_of_section / (n_points_path-1.0)
    x = 100*dx-depth_of_section/2
    X = 1
    while X >= -depth_of_section/2:
        x -= dx
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)

    path_points.append((X, Y))

    while X <= depth_of_section/2:
        x += dx
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)
        path_points.append((X, Y))

    # for i in range(n_points_path):
    #     x = depth_of_section*(i/(n_points_path-1.0)-1.0/2.0)
    #     y = path_func(x)
    #     theta = path_theta_func(x)
    #     X, Y = bottom_adjust(x, y, theta)
    #     path_points.append((X, Y))

    path_points = tuple(path_points)

    break_point(activate_breakpoints, "in")
    print("path_points")
    print(path_points)
    s1.Spline(points=path_points)

    break_point(activate_breakpoints, "1")


    s1.unsetPrimaryObject()
    s1.unsetPrimaryObject()

    p = mdb.models['Model-1'].parts[part_name]
    f1, e1 = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f1[4], sketchUpEdge=e1[4],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0,
        0.0))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=231.51, gridSpacing=5.78, transform=t)
    g1, v1, d1, c1 = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
    break_point(activate_breakpoints, "4")

    # cut_theta_func = lambda x: x*math.pi/pro_thick + 3*math.pi/2 + math.pi
    # cut_func = lambda x: path_height/2.0*math.sin(cut_theta_func(x)) + path_height/2.0

    cut_theta_func = path_theta_func
    cut_func = path_func

    cut_points = []
    n_points_cut = n_points_path
    dx = right_thickness / (n_points_cut-1.0)
    x = -right_thickness/2
    X = 1
    while X >= -1.25*right_thickness/2:
        x -= dx
        y = cut_func(x)
        theta = cut_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)

    cut_points.append((X, Y))

    while X <= 1.25*right_thickness/2:
        x += dx
        y = cut_func(x)
        theta = cut_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)
        cut_points.append((X, Y))

    start_point, end_point  = cut_points[0], cut_points[len(cut_points)-1]
    cut_points = tuple(cut_points)

    print("cut points, soft code")
    print(cut_points)

    break_point(activate_breakpoints, "4")

    s.Spline(points=cut_points)

    break_point(activate_breakpoints, "4")

    point1 = start_point
    point2 = (start_point[0], -(start_point[1] + pre_cut_col_thickness))
    point3 = (end_point[0], point2[1])
    point4 = end_point

    break_point(activate_breakpoints, "4.1")
    left_v = s.Line(point1=point1, point2=point2)
    s.VerticalConstraint(entity=left_v, addUndoState=False)
    break_point(activate_breakpoints, "4")
    h_line = s.Line(point1=point2, point2=point3)
    s.HorizontalConstraint(entity=h_line, addUndoState=False)
    s.PerpendicularConstraint(entity1=h_line, entity2=left_v, addUndoState=False)
    break_point(activate_breakpoints, "4")
    right_v = s.Line(point1=point3, point2=point4)
    s.VerticalConstraint(entity=right_v, addUndoState=False)
    s.PerpendicularConstraint(entity1=right_v, entity2=h_line, addUndoState=False)
    break_point(activate_breakpoints, "4")

    # s.Spline(points=((-11.56, 0.0), (-4.335, 17.34), (-1.445, 2.89), (2.89, 17.34),
    #     (11.56, 0.0)))
    #
    # s.Line(point1=(-11.56, 0.0), point2=(-11.56, -57.8))
    # s.VerticalConstraint(entity=g1[7], addUndoState=False)
    # s.Line(point1=(-11.56, -57.8), point2=(11.56, -57.8))
    # s.HorizontalConstraint(entity=g1[8], addUndoState=False)
    # s.PerpendicularConstraint(entity1=g1[7], entity2=g1[8], addUndoState=False)
    # s.Line(point1=(11.56, -57.8), point2=(11.56, 0.0))
    # s.VerticalConstraint(entity=g1[9], addUndoState=False)
    # s.PerpendicularConstraint(entity1=g1[8], entity2=g1[9], addUndoState=False)

    break_point(activate_breakpoints, "2")

    s.unsetPrimaryObject()

    p = mdb.models['Model-1'].parts[part_name]
    break_point(activate_breakpoints, "3")
    f, e = p.faces, p.edges
    break_point(activate_breakpoints, "4")
    p.CutSweep(pathPlane=f[1], pathUpEdge=e[6], sketchPlane=f[4],
        sketchUpEdge=e[4], pathOrientation=RIGHT, path=s1,
        sketchOrientation=RIGHT, profile=s, profileNormal=ON)
    break_point(activate_breakpoints, "5")

    del mdb.models['Model-1'].sketches['__sweep__']
    break_point(activate_breakpoints, "9")
    del mdb.models['Model-1'].sketches['__profile__']


def col_block_left_cut_args(part_name, pro_height, pro_thick, n_points_path, depth_of_section, right_thickness, protrusion_length, col_thick, pre_cut_col_thickness, collagen_translate, activate_breakpoints=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    break_point(activate_breakpoints, "1")

    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f[1], sketchUpEdge=e[4],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, right_thickness/2,
        depth_of_section/2.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__',
        sheetSize=316.49, gridSpacing=7.91, transform=t)
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)

    break_point(activate_breakpoints, "Easy now")

    # sketching path
    path_height = pro_height/2.0
    dist = col_thick/2.0
    # path_theta_func = lambda x: x*math.pi/pro_thick - depth_of_section * math.pi/(2*pro_thick) + 3*math.pi/2 + math.pi
    path_theta_func = lambda x: x*math.pi/pro_thick - math.pi
    path_func = lambda x: path_height/2.0*math.cos(path_theta_func(x)) + path_height/2.0

    def bottom_adjust(x, y, theta):
        angle_rad = math.pi/2 + math.pi/4 * math.cos(theta-math.pi/2)
        X = x - dist * math.cos(angle_rad)
        Y = y + dist * math.sin(angle_rad) + collagen_translate/2
        return X, Y

    path_points = []

    dx = depth_of_section / (n_points_path-1.0)
    x = 100*dx-depth_of_section/2
    X = 1
    while X >= -depth_of_section/2:
        x -= dx
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)

    path_points.append((X, Y))

    while X <= depth_of_section/2:
        x += dx
        y = path_func(x)
        theta = path_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)
        path_points.append((X, Y))

    # for i in range(n_points_path):
    #     x = depth_of_section*(i/(n_points_path-1.0)-1.0/2.0)
    #     y = path_func(x)
    #     theta = path_theta_func(x)
    #     X, Y = bottom_adjust(x, y, theta)
    #     path_points.append((X, Y))

    path_points = tuple(path_points)

    break_point(activate_breakpoints, "in")
    print("path_points")
    print(path_points)
    s1.Spline(points=path_points)

    break_point(activate_breakpoints, "1")

    # s1.Spline(points=((-20.0, -13.8425), (-15.82, -23.73), (-11.865, -13.8425), (
    #     -7.91, -23.73), (-3.955, -13.8425), (0.0, -23.73), (3.955, -13.8425), (
    #     7.91, -23.73), (11.865, -13.8425), (15.82, -23.73), (20.0, -13.8425)))


    s1.unsetPrimaryObject()
    s1.unsetPrimaryObject()
    session.viewports['Viewport: 1'].view.setValues(nearPlane=149.51,
        farPlane=212.448, width=69.029, height=35.6003, cameraPosition=(
        -13.7972, 167.129, 39.9679), cameraTarget=(-15.4906, -13.5907,
        30.4309))
    p = mdb.models['Model-1'].parts[part_name]
    f1, e1 = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f1[5], sketchUpEdge=e1[6],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0,
        0.0, 0.0))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=304.28, gridSpacing=7.6, transform=t)
    g1, v1, d1, c1 = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)

    break_point(activate_breakpoints, "4")

    # cut_theta_func = lambda x: x*math.pi/pro_thick + 3*math.pi/2 + math.pi
    # cut_func = lambda x: path_height/2.0*math.sin(cut_theta_func(x)) + path_height/2.0

    cut_theta_func = path_theta_func
    cut_func = path_func

    cut_points = []
    n_points_cut = n_points_path
    dx = right_thickness / (n_points_cut-1.0)
    x = -right_thickness/2
    X = 1
    while X >= -1.25*right_thickness/2:
        x -= dx
        y = cut_func(x)
        theta = cut_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)

    cut_points.append((X, Y))

    while X <= 1.25*right_thickness/2:
        x += dx
        y = cut_func(x)
        theta = cut_theta_func(x)
        X, Y = bottom_adjust(x, y, theta)
        cut_points.append((X, Y))

    start_point, end_point  = cut_points[0], cut_points[len(cut_points)-1]
    cut_points = tuple(cut_points)

    print("cut points, soft code")
    print(cut_points)

    break_point(activate_breakpoints, "4")

    s.Spline(points=cut_points)

    break_point(activate_breakpoints, "4")

    point1 = start_point
    point2 = (start_point[0], (start_point[1] + pre_cut_col_thickness))
    point3 = (end_point[0], point2[1])
    point4 = end_point

    break_point(activate_breakpoints, "4.1")
    left_v = s.Line(point1=point1, point2=point2)
    s.VerticalConstraint(entity=left_v, addUndoState=False)
    break_point(activate_breakpoints, "4")
    h_line = s.Line(point1=point2, point2=point3)
    s.HorizontalConstraint(entity=h_line, addUndoState=False)
    s.PerpendicularConstraint(entity1=h_line, entity2=left_v, addUndoState=False)
    break_point(activate_breakpoints, "4")
    right_v = s.Line(point1=point3, point2=point4)
    s.VerticalConstraint(entity=right_v, addUndoState=False)
    s.PerpendicularConstraint(entity1=right_v, entity2=h_line, addUndoState=False)
    break_point(activate_breakpoints, "4")

    break_point(activate_breakpoints, "2")


    # session.viewports['Viewport: 1'].view.setValues(nearPlane=96.8279,
    #     farPlane=153.885, width=61.5006, height=33.3442, cameraPosition=(
    #     -16.9036, -5.6876, 145.356), cameraTarget=(-16.9036, -5.6876, 40))
    # s.Spline(points=((-8.46013188317871, -14.5723731495361), (-4.46940112069702,
    #     -22.7354724384766), (0.0, -13.3), (4.46559071585083,
    #     -22.9099099613647), (9.5, -9.5)))
    # s.dragEntity(entity=v1[4], points=((-8.46013188317871, -14.5723731495361), (
    #     -8.46013188317871, -15.2), (-8.2986843581543, -11.4), (-7.6, -9.5), (
    #     -9.5, -7.6), (-9.5, -8.83894610180664)))
    # s.dragEntity(entity=v1[5], points=((-4.46940112069702, -22.7354724384766), (
    #     -4.46940112069702, -22.8), (-3.8, -20.9)))
    # s.dragEntity(entity=v1[7], points=((4.51638954348303, -22.7706215061426), (
    #     4.51638954348303, -22.8), (3.8, -21.6189639545898), (2.78302693411255,
    #     -19.8181436039429), (1.9, -19.0)))
    # s.dragEntity(entity=v1[7], points=((1.9, -19.0), (1.9, -19.0), (3.8, -19.0)))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=82.3382,
    #     farPlane=168.374, width=172.835, height=93.7071, cameraPosition=(
    #     -18.7178, -8.54263, 145.356), cameraTarget=(-18.7178, -8.54263, 40))
    # s.Line(point1=(-9.5, -8.83894610180664), point2=(-9.5, 32.3))
    # s.VerticalConstraint(entity=g1[7], addUndoState=False)
    # s.Line(point1=(-9.5, 32.3), point2=(9.5, 32.3))
    # s.HorizontalConstraint(entity=g1[8], addUndoState=False)
    # s.PerpendicularConstraint(entity1=g1[7], entity2=g1[8], addUndoState=False)
    # s.Line(point1=(9.5, 32.3), point2=(9.5, -9.5))
    # s.VerticalConstraint(entity=g1[9], addUndoState=False)
    # s.PerpendicularConstraint(entity1=g1[8], entity2=g1[9], addUndoState=False)



    s.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    p.CutSweep(pathPlane=f[1], pathUpEdge=e[4], sketchPlane=f[5],
        sketchUpEdge=e[6], pathOrientation=RIGHT, path=s1,
        sketchOrientation=RIGHT, profile=s, profileNormal=ON)
    del mdb.models['Model-1'].sketches['__sweep__']
    del mdb.models['Model-1'].sketches['__profile__']

def collagen_top_args(part_name, depth_of_section, collagen_top_thickness, radius_of_curvature, horizontal_section_length, right_thickness, flat_length, protrusion_length, b_points=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    # t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[11],
    #     sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(-protrusion_length/2, 0.0,
    #     depth_of_section))
    t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[11],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(-protrusion_length/4, 0.0,
        depth_of_section))
    # t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[11],
    #     sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0,
    #     depth_of_section))
    # t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[11],
    #     sketchPlaneSide=SIDE1, sketchOrientation=LEFT, origin=(0.0, right_thickness/2,
    #     depth_of_section))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=105.35, gridSpacing=2.63, transform=t)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)


    x_axis = s.ConstructionLine(point1=(7.89, 0.0), point2=(13.15, 0.0))
    s.HorizontalConstraint(entity=x_axis, addUndoState=False)
    s.FixedConstraint(entity=x_axis)

    break_point(b_points, "1")

    y_axis_touch = s.ConstructionLine(point1=(right_thickness/2, 0.0), point2=(right_thickness/2, 5.26))
    s.VerticalConstraint(entity=y_axis_touch, addUndoState=False)
    # s.CoincidentConstraint(entity1=y_axis_touch, entity2=v[3])
    s.FixedConstraint(entity=y_axis_touch)

    break_point(b_points, "2")

    s.Line(point1=(right_thickness/2, 13.15), point2=(right_thickness/2, 0.0))
    s.VerticalConstraint(entity=g[8], addUndoState=False)
    s.ParallelConstraint(entity1=g[7], entity2=g[8], addUndoState=False)

    break_point(b_points, "3")

    s.CoincidentConstraint(entity1=v[4], entity2=g[7], addUndoState=False)
    s.CoincidentConstraint(entity1=v[5], entity2=g[6], addUndoState=False)

    break_point(b_points, "4")

    s.Line(point1=(right_thickness/2, 0.0), point2=(right_thickness/2, -10.2053871154785))
    s.VerticalConstraint(entity=g[9], addUndoState=False)
    s.ParallelConstraint(entity1=g[8], entity2=g[9], addUndoState=False)

    break_point(b_points, "5")

    s.CoincidentConstraint(entity1=v[6], entity2=g[7], addUndoState=False)
    s.EqualLengthConstraint(entity1=g[9], entity2=g[8])
    s.VerticalDimension(vertex1=v[6], vertex2=v[4], textPoint=(-15.2956047058105,
        1.6713809967041), value=flat_length)

    break_point(b_points, "6")

    s.ConstructionLine(point1=(18.41, -21.04), point2=(21.04, -21.04))
    s.HorizontalConstraint(entity=g[10], addUndoState=False)
    s.CoincidentConstraint(entity1=v[6], entity2=g[10])

    break_point(b_points, "7")

    s.ConstructionLine(point1=(13.15, 23.67), point2=(18.41, 23.67))
    s.HorizontalConstraint(entity=g[11], addUndoState=False)
    s.CoincidentConstraint(entity1=g[11], entity2=v[4])

    break_point(b_points, "8")


    s.ArcByCenterEnds(center=(right_thickness/2-radius_of_curvature, -flat_length/2), point1=(right_thickness/2, -flat_length/2),
        point2=(3.2875, -flat_length/2-horizontal_section_length), direction=CLOCKWISE)
    break_point(b_points, "8.1")
    s.CoincidentConstraint(entity1=v[8], entity2=g[10], addUndoState=False)
    break_point(b_points, "8.2")

    s.HorizontalDimension(vertex1=v[6], vertex2=v[8], textPoint=(1.63712692260742,
        -14.0401802062988), value=radius_of_curvature)
    break_point(b_points, "8.3")

    s.VerticalDimension(vertex1=v[6], vertex2=v[7], textPoint=(39.6365776062012,
        -47.5099487304688), value=horizontal_section_length)

    break_point(b_points, "9")

    s.ArcByCenterEnds(center=(right_thickness/2-radius_of_curvature-collagen_top_thickness, -flat_length/2), point1=(collagen_top_thickness + right_thickness/2,
        -flat_length/2), point2=(13.8075, -flat_length/2-horizontal_section_length- collagen_top_thickness), direction=CLOCKWISE)
    break_point(b_points, "9.1")
    s.CoincidentConstraint(entity1=v[11], entity2=g[10], addUndoState=False)
    break_point(b_points, "9.2")
    s.CoincidentConstraint(entity1=v[9], entity2=g[10], addUndoState=False)
    s.HorizontalDimension(vertex1=v[9], vertex2=v[6], textPoint=(10.9628486633301,
        -44.4892044067383), value=collagen_top_thickness)

    break_point(b_points, "10")

    s.Line(point1=(2.63, -42.08), point2=(13.15, -46.6825))
    break_point(b_points, "10.1")
    s.CoincidentConstraint(entity1=v[12], entity2=v[7])
    break_point(b_points, "10.2")
    s.CoincidentConstraint(entity1=v[13], entity2=v[10])
    break_point(b_points, "10.3")
    s.ObliqueDimension(vertex1=v[12], vertex2=v[13], textPoint=(4.13760471343994,
        -43.1775588989258), value=collagen_top_thickness)
    break_point(b_points, "10.4")
    s.PerpendicularConstraint(entity1=g[14], entity2=g[13])


    break_point(b_points, "10.5")



    break_point(b_points, "11")

    s.ArcByCenterEnds(center=(right_thickness/2-radius_of_curvature, flat_length/2), point1=(right_thickness/2, flat_length/2),
        point2=(3.2875, flat_length/2+horizontal_section_length), direction=COUNTERCLOCKWISE)
    # s.ArcByCenterEnds(center=(-49.97, 17.5), point1=(7.5, 17.5), point2=(-1.315,
    #     35.505), direction=COUNTERCLOCKWISE)
    break_point(b_points, "11.1")
    s.CoincidentConstraint(entity1=v[15], entity2=g[11], addUndoState=False)
    break_point(b_points, "11.2")
    s.HorizontalDimension(vertex1=v[15], vertex2=v[4], textPoint=(
        -18.0604610443115, 28.4272918701172), value=radius_of_curvature)
    break_point(b_points, "11.3")
    s.VerticalDimension(vertex1=v[4], vertex2=v[14], textPoint=(33.7830619812012,
        26.2830066680908), value=horizontal_section_length)

    break_point(b_points, "12")

    s.ArcByCenterEnds(center=(right_thickness/2-radius_of_curvature-collagen_top_thickness, flat_length/2), point1=(collagen_top_thickness + right_thickness/2,
        flat_length/2), point2=(13.8075, flat_length/2+horizontal_section_length+collagen_top_thickness), direction=COUNTERCLOCKWISE)
    break_point(b_points, "12.1")
    # s.ArcByCenterEnds(center=(-100.19, 17.5), point1=(17.7524999999721, 17.5),
    #     point2=(14.465, 34.8475), direction=COUNTERCLOCKWISE)
    s.CoincidentConstraint(entity1=v[18], entity2=g[11], addUndoState=False)
    break_point(b_points, "12.2")
    s.CoincidentConstraint(entity1=v[16], entity2=g[11], addUndoState=False)

    break_point(b_points, "13")

    s.Line(point1=(3.2875, 42.08), point2=(13.15, 38.135))
    s.CoincidentConstraint(entity1=v[19], entity2=v[14])
    s.CoincidentConstraint(entity1=v[20], entity2=v[17])
    s.PerpendicularConstraint(entity1=g[17], entity2=g[16])
    s.ObliqueDimension(vertex1=v[19], vertex2=v[20], textPoint=(4.04012870788574,
        47.5974197387695), value=collagen_top_thickness)
    s.HorizontalDimension(vertex1=v[16], vertex2=v[4], textPoint=(12.1466503143311,
        46.71435546875), value=collagen_top_thickness)

    break_point(b_points, "14")

    s.Line(point1=(20.3825, 13.8075), point2=(24.985, -11.835))
    s.CoincidentConstraint(entity1=v[21], entity2=v[16])
    s.CoincidentConstraint(entity1=v[22], entity2=v[9])

    break_point(b_points, "15")

    p = mdb.models['Model-1'].parts[part_name]
    f1, e1 = p.faces, p.edges
    p.SolidExtrude(sketchPlane=f1[5], sketchUpEdge=e1[11], sketchPlaneSide=SIDE1,
        sketchOrientation=RIGHT, sketch=s, depth=depth_of_section, flipExtrudeDirection=ON)
    s.unsetPrimaryObject()
    del mdb.models['Model-1'].sketches['__profile__']

#
# def new_collagen_left_cut():
#     import section
#     import regionToolset
#     import displayGroupMdbToolset as dgm
#     import part
#     import material
#     import assembly
#     import step
#     import interaction
#     import load
#     import mesh
#     import optimization
#     import job
#     import sketch
#     import visualization
#     import xyPlot
#     import displayGroupOdbToolset as dgo
#     import connectorBehavior
#     p = mdb.models['Model-1'].parts['collagen-block']
#     f, e = p.faces, p.edges
#     t = p.MakeSketchTransform(sketchPlane=f[1], sketchUpEdge=e[4],
#         sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(-26.748904,
#         7.5, 20.0))
#     s1 = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__',
#         sheetSize=316.49, gridSpacing=7.91, transform=t)
#     g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
#     s1.setPrimaryObject(option=SUPERIMPOSE)
#     p = mdb.models['Model-1'].parts['collagen-block']
#     p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=120.352,
#         farPlane=154.513, width=69.0897, height=37.4589, cameraPosition=(
#         -15.7033, 137.433, 14.1976), cameraTarget=(-15.7033, 7.5, 14.1976))
#     s1.Spline(points=((-20.0, -13.8425), (-15.82, -23.73), (-11.865, -13.8425), (
#         -7.91, -23.73), (-3.955, -13.8425), (0.0, -23.73), (3.955, -13.8425), (
#         7.91, -23.73), (11.865, -13.8425), (15.82, -23.73), (20.0, -13.8425)))
#     s1.CoincidentConstraint(entity1=v[4], entity2=g[4], addUndoState=False)
#     s1.CoincidentConstraint(entity1=v[14], entity2=g[5], addUndoState=False)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=117.718,
#         farPlane=157.147, width=95.6153, height=51.8405, cameraPosition=(
#         -19.9002, 137.433, 7.27813), cameraTarget=(-19.9002, 7.5, 7.27813))
#     s1.unsetPrimaryObject()
#     s1.unsetPrimaryObject()
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=149.51,
#         farPlane=212.448, width=69.029, height=35.6003, cameraPosition=(
#         -13.7972, 167.129, 39.9679), cameraTarget=(-15.4906, -13.5907,
#         30.4309))
#     p = mdb.models['Model-1'].parts['collagen-block']
#     f1, e1 = p.faces, p.edges
#     t = p.MakeSketchTransform(sketchPlane=f1[5], sketchUpEdge=e1[6],
#         sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(-26.091048,
#         0.000211, 40.0))
#     s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
#         sheetSize=304.28, gridSpacing=7.6, transform=t)
#     g1, v1, d1, c1 = s.geometry, s.vertices, s.dimensions, s.constraints
#     s.setPrimaryObject(option=SUPERIMPOSE)
#     p = mdb.models['Model-1'].parts['collagen-block']
#     p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=96.8279,
#         farPlane=153.885, width=61.5006, height=33.3442, cameraPosition=(
#         -16.9036, -5.6876, 145.356), cameraTarget=(-16.9036, -5.6876, 40))
#     s.Spline(points=((-8.46013188317871, -14.5723731495361), (-4.46940112069702,
#         -22.7354724384766), (0.0, -13.3), (4.46559071585083,
#         -22.9099099613647), (9.5, -9.5)))
#     s.dragEntity(entity=v1[4], points=((-8.46013188317871, -14.5723731495361), (
#         -8.46013188317871, -15.2), (-8.2986843581543, -11.4), (-7.6, -9.5), (
#         -9.5, -7.6), (-9.5, -8.83894610180664)))
#     s.dragEntity(entity=v1[5], points=((-4.46940112069702, -22.7354724384766), (
#         -4.46940112069702, -22.8), (-3.8, -20.9)))
#     s.dragEntity(entity=v1[7], points=((4.51638954348303, -22.7706215061426), (
#         4.51638954348303, -22.8), (3.8, -21.6189639545898), (2.78302693411255,
#         -19.8181436039429), (1.9, -19.0)))
#     s.dragEntity(entity=v1[7], points=((1.9, -19.0), (1.9, -19.0), (3.8, -19.0)))
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=82.3382,
#         farPlane=168.374, width=172.835, height=93.7071, cameraPosition=(
#         -18.7178, -8.54263, 145.356), cameraTarget=(-18.7178, -8.54263, 40))
#     s.Line(point1=(-9.5, -8.83894610180664), point2=(-9.5, 32.3))
#     s.VerticalConstraint(entity=g1[7], addUndoState=False)
#     s.Line(point1=(-9.5, 32.3), point2=(9.5, 32.3))
#     s.HorizontalConstraint(entity=g1[8], addUndoState=False)
#     s.PerpendicularConstraint(entity1=g1[7], entity2=g1[8], addUndoState=False)
#     s.Line(point1=(9.5, 32.3), point2=(9.5, -9.5))
#     s.VerticalConstraint(entity=g1[9], addUndoState=False)
#     s.PerpendicularConstraint(entity1=g1[8], entity2=g1[9], addUndoState=False)
#     s.unsetPrimaryObject()
#     p = mdb.models['Model-1'].parts['collagen-block']
#     f, e = p.faces, p.edges
#     p.CutSweep(pathPlane=f[1], pathUpEdge=e[4], sketchPlane=f[5],
#         sketchUpEdge=e[6], pathOrientation=RIGHT, path=s1,
#         sketchOrientation=RIGHT, profile=s, profileNormal=ON)
#     del mdb.models['Model-1'].sketches['__sweep__']
#     del mdb.models['Model-1'].sketches['__profile__']
# def collagen_top():
#     import section
#     import regionToolset
#     import displayGroupMdbToolset as dgm
#     import part
#     import material
#     import assembly
#     import step
#     import interaction
#     import load
#     import mesh
#     import optimization
#     import job
#     import sketch
#     import visualization
#     import xyPlot
#     import displayGroupOdbToolset as dgo
#     import connectorBehavior
#     p = mdb.models['Model-1'].parts['collagen']
#     f, e = p.faces, p.edges
#     t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[11],
#         sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0,
#         40.0))
#     s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
#         sheetSize=105.35, gridSpacing=2.63, transform=t)
#     g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
#     s.setPrimaryObject(option=SUPERIMPOSE)
#     p = mdb.models['Model-1'].parts['collagen']
#     p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=31.2797,
#         farPlane=83.1183, width=44.4461, height=24.0977, cameraPosition=(
#         -7.43712, 0.119707, 77.199), cameraTarget=(-7.43712, 0.119707, 40))
#     s.Line(point1=(7.89, 15.78), point2=(7.89, 0.0))
#     s.VerticalConstraint(entity=g[6], addUndoState=False)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=23.0706,
#         farPlane=91.3275, width=101.884, height=55.2394, cameraPosition=(
#         -16.9662, -10.6992, 77.199), cameraTarget=(-16.9662, -10.6992, 40))
#     s.undo()
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=27.3778,
#         farPlane=87.0202, width=70.8235, height=38.3989, cameraPosition=(
#         -10.9631, -4.61163, 77.199), cameraTarget=(-10.9631, -4.61163, 40))
#     s.ConstructionLine(point1=(7.89, 0.0), point2=(13.15, 0.0))
#     s.HorizontalConstraint(entity=g[6], addUndoState=False)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=26.1076,
#         farPlane=88.2905, width=83.2818, height=45.1535, cameraPosition=(
#         -13.7284, -7.4206, 77.199), cameraTarget=(-13.7284, -7.4206, 40))
#     s.FixedConstraint(entity=g[6])
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=27.3737,
#         farPlane=87.0243, width=70.8531, height=38.4149, cameraPosition=(
#         -11.455, -4.58254, 77.199), cameraTarget=(-11.455, -4.58254, 40))
#     s.ConstructionLine(point1=(10.52, 0.0), point2=(10.52, 5.26))
#     s.VerticalConstraint(entity=g[7], addUndoState=False)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=30.6396,
#         farPlane=83.7585, width=47.3021, height=25.6461, cameraPosition=(
#         -9.09574, -1.28433, 77.199), cameraTarget=(-9.09574, -1.28433, 40))
#     s.CoincidentConstraint(entity1=g[7], entity2=v[3])
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=24.8874,
#         farPlane=89.5106, width=88.7825, height=48.1359, cameraPosition=(
#         -11.3671, -11.4689, 77.199), cameraTarget=(-11.3671, -11.4689, 40))
#     s.Line(point1=(7.5, 13.15), point2=(7.5, 0.0))
#     s.VerticalConstraint(entity=g[8], addUndoState=False)
#     s.ParallelConstraint(entity1=g[7], entity2=g[8], addUndoState=False)
#     s.CoincidentConstraint(entity1=v[4], entity2=g[7], addUndoState=False)
#     s.CoincidentConstraint(entity1=v[5], entity2=g[6], addUndoState=False)
#     s.Line(point1=(7.5, 0.0), point2=(7.5, -10.2053871154785))
#     s.VerticalConstraint(entity=g[9], addUndoState=False)
#     s.ParallelConstraint(entity1=g[8], entity2=g[9], addUndoState=False)
#     s.CoincidentConstraint(entity1=v[6], entity2=g[7], addUndoState=False)
#     s.EqualLengthConstraint(entity1=g[9], entity2=g[8])
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=30.7492,
#         farPlane=83.6489, width=46.5117, height=25.2176, cameraPosition=(
#         -5.06189, -1.77849, 77.199), cameraTarget=(-5.06189, -1.77849, 40))
#     s.dragEntity(entity=v[5], points=((7.5, 0.0), (7.2325, 0.0), (9.8625, 0.0), (
#         5.26, 0.0), (7.89, 4.6025), (7.2325, -1.315)))
#     s.dragEntity(entity=v[5], points=((7.5, 0.0), (7.2325, 0.0), (7.2325, 1.315), (
#         7.89, 5.9175), (7.89, -2.63), (7.89, 2.63), (7.89, 5.9175)))
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=27.5379,
#         farPlane=86.8601, width=72.5419, height=39.3306, cameraPosition=(
#         -6.75839, -8.18781, 77.199), cameraTarget=(-6.75839, -8.18781, 40))
#     s.dragEntity(entity=v[4], points=((7.5, 10.2053871154785), (7.2325, 10.52), (
#         7.89, 13.8075), (7.2325, 14.465), (7.89, 9.8625), (7.89, 12.4925), (
#         13.8075, 12.4925), (7.2325, 12.4925), (3.945, 11.835), (6.575,
#         13.8075)))
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=25.502,
#         farPlane=88.8961, width=87.8291, height=47.6189, cameraPosition=(
#         -6.90279, -12.6442, 77.199), cameraTarget=(-6.90279, -12.6442, 40))
#     s.VerticalDimension(vertex1=v[6], vertex2=v[4], textPoint=(-15.2956047058105,
#         1.6713809967041), value=35.0)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=21.9887,
#         farPlane=92.4093, width=109.686, height=59.4693, cameraPosition=(
#         5.25973, -8.15983, 77.199), cameraTarget=(5.25973, -8.15983, 40))
#     s.ConstructionLine(point1=(18.41, -21.04), point2=(21.04, -21.04))
#     s.HorizontalConstraint(entity=g[10], addUndoState=False)
#     s.CoincidentConstraint(entity1=v[6], entity2=g[10])
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=20.372,
#         farPlane=94.0261, width=121.345, height=65.7905, cameraPosition=(
#         -7.70975, -0.630928, 77.199), cameraTarget=(-7.70975, -0.630928, 40))
#     s.ConstructionLine(point1=(13.15, 23.67), point2=(18.41, 23.67))
#     s.HorizontalConstraint(entity=g[11], addUndoState=False)
#     s.CoincidentConstraint(entity1=g[11], entity2=v[4])
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=18.1956,
#         farPlane=96.2025, width=142.69, height=77.3634, cameraPosition=(
#         -8.27428, -4.45696, 77.199), cameraTarget=(-8.27428, -4.45696, 40))
#     s.ArcByCenterEnds(center=(-4.60249999998952, -17.5), point1=(7.5, -17.5),
#         point2=(3.2875, -28.2725), direction=CLOCKWISE)
#     s.CoincidentConstraint(entity1=v[8], entity2=g[10], addUndoState=False)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=16.5961,
#         farPlane=97.8019, width=154.7, height=83.8747, cameraPosition=(
#         -10.5317, -7.11947, 77.199), cameraTarget=(-10.5317, -7.11947, 40))
#     s.HorizontalDimension(vertex1=v[6], vertex2=v[8], textPoint=(1.63712692260742,
#         -14.0401802062988), value=80.0)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=94.3536,
#         farPlane=254.874, width=452.474, height=245.321, cameraPosition=(
#         -30.776, -44.0285, 194.614), cameraTarget=(-30.776, -44.0285, 40))
#     s.VerticalDimension(vertex1=v[6], vertex2=v[7], textPoint=(39.6365776062012,
#         -47.5099487304688), value=20.0)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=125.495,
#         farPlane=223.733, width=209.983, height=113.848, cameraPosition=(
#         -6.85816, -10.0726, 194.614), cameraTarget=(-6.85816, -10.0726, 40))
#     s.ArcByCenterEnds(center=(-19.0675000000023, -17.5), point1=(17.094999999986,
#         -17.5), point2=(13.8075, -40.1075), direction=CLOCKWISE)
#     s.CoincidentConstraint(entity1=v[11], entity2=g[10], addUndoState=False)
#     s.CoincidentConstraint(entity1=v[9], entity2=g[10], addUndoState=False)
#     s.Line(point1=(2.63, -42.08), point2=(13.15, -46.6825))
#     s.CoincidentConstraint(entity1=v[12], entity2=v[7])
#     s.CoincidentConstraint(entity1=v[13], entity2=v[10])
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=136.317,
#         farPlane=212.911, width=131.943, height=71.5363, cameraPosition=(
#         10.4943, -3.70428, 194.614), cameraTarget=(10.4943, -3.70428, 40))
#     s.ObliqueDimension(vertex1=v[12], vertex2=v[13], textPoint=(4.13760471343994,
#         -43.1775588989258), value=5.0)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=128.787,
#         farPlane=220.441, width=193.924, height=105.141, cameraPosition=(
#         -0.980656, -11.5397, 194.614), cameraTarget=(-0.980656, -11.5397, 40))
#     s.HorizontalDimension(vertex1=v[9], vertex2=v[6], textPoint=(10.9628486633301,
#         -44.4892044067383), value=5.0)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=135.539,
#         farPlane=213.689, width=137.555, height=74.5792, cameraPosition=(
#         10.4798, -5.9685, 194.614), cameraTarget=(10.4798, -5.9685, 40))
#     s.PerpendicularConstraint(entity1=g[14], entity2=g[13])
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=128.787,
#         farPlane=220.441, width=193.925, height=105.142, cameraPosition=(
#         10.8088, -13.3038, 194.614), cameraTarget=(10.8088, -13.3038, 40))
#     s.ArcByCenterEnds(center=(-49.97, 17.5), point1=(7.5, 17.5), point2=(-1.315,
#         35.505), direction=COUNTERCLOCKWISE)
#     s.CoincidentConstraint(entity1=v[15], entity2=g[11], addUndoState=False)
#     s.HorizontalDimension(vertex1=v[15], vertex2=v[4], textPoint=(
#         -18.0604610443115, 28.4272918701172), value=80.0)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=118.825,
#         farPlane=230.403, width=268.728, height=145.698, cameraPosition=(
#         14.3681, -19.202, 194.614), cameraTarget=(14.3681, -19.202, 40))
#     s.VerticalDimension(vertex1=v[4], vertex2=v[14], textPoint=(33.7830619812012,
#         26.2830066680908), value=20.0)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=128.71,
#         farPlane=220.518, width=186.803, height=101.28, cameraPosition=(
#         -0.0885696, -7.41476, 194.614), cameraTarget=(-0.0885696, -7.41476,
#         40))
#     s.ArcByCenterEnds(center=(-34.19, 17.5), point1=(17.7524999999721, 17.5),
#         point2=(14.465, 34.8475), direction=COUNTERCLOCKWISE)
#     s.CoincidentConstraint(entity1=v[18], entity2=g[11], addUndoState=False)
#     s.CoincidentConstraint(entity1=v[16], entity2=g[11], addUndoState=False)
#     s.Line(point1=(3.2875, 42.08), point2=(13.15, 38.135))
#     s.CoincidentConstraint(entity1=v[19], entity2=v[14])
#     s.CoincidentConstraint(entity1=v[20], entity2=v[17])
#     s.PerpendicularConstraint(entity1=g[17], entity2=g[16])
#     s.ObliqueDimension(vertex1=v[19], vertex2=v[20], textPoint=(4.04012870788574,
#         47.5974197387695), value=5.0)
#     s.HorizontalDimension(vertex1=v[16], vertex2=v[4], textPoint=(12.1466503143311,
#         46.71435546875), value=5.0)
#     s.Line(point1=(20.3825, 13.8075), point2=(24.985, -11.835))
#     s.CoincidentConstraint(entity1=v[21], entity2=v[16])
#     s.CoincidentConstraint(entity1=v[22], entity2=v[9])
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=58.2749,
#         farPlane=134.248, width=61.3207, height=31.6249, projection=1,
#         cameraPosition=(14.951, 22.0838, 112.649), cameraUpVector=(0.0296814,
#         0.619508, 0.784429), cameraTarget=(-0.299924, -0.855424, 15.6862),
#         viewOffsetX=1.01117, viewOffsetY=1.31644)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=47.3192,
#         farPlane=149.958, width=49.7924, height=25.6794, cameraPosition=(
#         41.8403, 87.5761, 39.8195), cameraUpVector=(0.085761, 0.991408,
#         -0.0987691), cameraTarget=(-0.035759, -2.17852, 21.0823),
#         viewOffsetX=0.821066, viewOffsetY=1.06895)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=47.8591,
#         farPlane=148.214, width=50.3605, height=25.9724, cameraPosition=(
#         35.1171, 66.5355, 83.4174), cameraUpVector=(0.0962006, 0.928693,
#         0.358155), cameraTarget=(-0.514138, -2.83309, 19.5505),
#         viewOffsetX=0.830434, viewOffsetY=1.08115)
#     p = mdb.models['Model-1'].parts['collagen']
#     f1, e1 = p.faces, p.edges
#     p.SolidExtrude(sketchPlane=f1[5], sketchUpEdge=e1[11], sketchPlaneSide=SIDE1,
#         sketchOrientation=RIGHT, sketch=s, depth=20.0, flipExtrudeDirection=ON)
#     s.unsetPrimaryObject()
#     del mdb.models['Model-1'].sketches['__profile__']
# def new_ref():
#     import section
#     import regionToolset
#     import displayGroupMdbToolset as dgm
#     import part
#     import material
#     import assembly
#     import step
#     import interaction
#     import load
#     import mesh
#     import optimization
#     import job
#     import sketch
#     import visualization
#     import xyPlot
#     import displayGroupOdbToolset as dgo
#     import connectorBehavior
#     p = mdb.models['Model-1'].parts['right-bone']
#     f, e = p.faces, p.edges
#     t = p.MakeSketchTransform(sketchPlane=f[5], sketchUpEdge=e[17],
#         sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(20.0, 0.0,
#         20.0))
#     s = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__', sheetSize=113.13,
#         gridSpacing=2.82, transform=t)
#     g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
#     s.setPrimaryObject(option=SUPERIMPOSE)
#     p = mdb.models['Model-1'].parts['right-bone']
#     p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=101.253,
#         farPlane=175.021, width=89.233, height=48.3801, cameraPosition=(19.422,
#         113.137, 20.2331), cameraTarget=(19.422, 0, 20.2331))
#     s.Line(point1=(-20.0, 20.0), point2=(0.0, 8.46))
#     s.Line(point1=(0.0, 8.46), point2=(20.0, 20.0))
#     s.unsetPrimaryObject()
#     s.unsetPrimaryObject()
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=173.142,
#         farPlane=310.18, cameraPosition=(-84.4679, 7.75165, -207.084),
#         cameraUpVector=(0.905333, -0.251751, -0.342044), cameraTarget=(-5, -15,
#         20))
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=152.62,
#         farPlane=330.701, cameraPosition=(-226.232, 67.8238, -30.9563),
#         cameraUpVector=(0.0463813, -0.430643, -0.90133), cameraTarget=(
#         -5.00013, -14.9999, 20.0002))
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=150.12,
#         farPlane=333.201, cameraPosition=(-195.738, 99.4139, 114.491),
#         cameraUpVector=(-0.532618, -0.21101, -0.819629), cameraTarget=(
#         -5.00015, -14.9999, 20.0001))
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=158.841,
#         farPlane=324.481, cameraPosition=(-148.489, 49.0275, 203.606),
#         cameraUpVector=(-0.759754, 0.126363, -0.637813), cameraTarget=(
#         -5.00023, -14.9998, 20))
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=163.507,
#         farPlane=319.814, cameraPosition=(-125.361, 35.2026, 223.452),
#         cameraUpVector=(-0.819545, 0.204479, -0.535289), cameraTarget=(
#         -5.00016, -14.9998, 20.0001))
#     p = mdb.models['Model-1'].parts['right-bone']
#     f1, e1 = p.faces, p.edges
#     t = p.MakeSketchTransform(sketchPlane=f1[6], sketchUpEdge=e1[16],
#         sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(-11.770648,
#         -14.711809, 40.0))
#     s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
#         sheetSize=220.9, gridSpacing=5.52, transform=t)
#     g1, v1, d1, c1 = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
#     s1.setPrimaryObject(option=SUPERIMPOSE)
#     p = mdb.models['Model-1'].parts['right-bone']
#     p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)
#     session.viewports['Viewport: 1'].view.setValues(nearPlane=175.715,
#         farPlane=276.11, width=226.742, height=122.934, cameraPosition=(
#         -10.5873, -28.1317, 245.913), cameraTarget=(-10.5873, -28.1317, 40))
#     s1.rectangle(point1=(0.0, -22.08), point2=(22.08, -55.2))
#     s1.unsetPrimaryObject()
#     p = mdb.models['Model-1'].parts['right-bone']
#     f, e = p.faces, p.edges
#     p.CutSweep(pathPlane=f[5], pathUpEdge=e[17], sketchPlane=f[6],
#         sketchUpEdge=e[16], pathOrientation=RIGHT, path=s,
#         sketchOrientation=RIGHT, profile=s1, profileNormal=ON)
#     del mdb.models['Model-1'].sketches['__sweep__']
#     del mdb.models['Model-1'].sketches['__profile__']
