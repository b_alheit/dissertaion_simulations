!********************************************
!************** All together ****************
!********************************************

subroutine hgo_sigma(F, mus, alphas, n_order, k1, k2, a0, K0, sigma)
  ! All working hundreds
  double precision :: F(3, 3), mus(n_order), alphas(n_order), k1, k2, a0(3), K0
  integer :: n_order
  double precision :: sigma(3,3)

  sigma = 0
  call iso_ogden_sigma(F, mus, alphas, n_order, sigma)
  call aniso_bar_sigma(F, k1, k2, a0, sigma)
  call vol_sigma(F, K0, sigma)
end

subroutine hgo_apprx_C(F, mus, alphas, n,  k1, k2, a0, K0, C)
    double precision :: F(3,3), C(6,6), mus(n), alphas(n), eps,  k1, k2, a0(3), K0
    double precision :: F_hat(3,3), k_stress(6), k_stress_hat(6), k_stress_mat(3,3), jacobian
    double precision :: Id(3,3), det, es(3, 3, 1)
    integer :: is(6), js(6), i, i_mat, j_mat, n

    is = [1, 2, 3, 2, 1, 1]
    js = [1, 2, 3, 3, 3, 2]
    call identity(Id, 3)
    jacobian = det(F)
    C = 0
    eps = 1.0e-9

    es = 0
    es(1, 1, 1) = 1
    es(2, 2, 1) = 1
    es(3, 3, 1) = 1

    k_stress_mat = 0
    ! call iso_ogden_sigma(F, mus, alphas, n, k_stress_mat)
    call hgo_sigma(F, mus, alphas, n, k1, k2, a0, K0, k_stress_mat)
    k_stress_mat = k_stress_mat * jacobian
    call matrix_to_voignt(k_stress_mat, k_stress)
    print*, 'k_stress'
    call print_vector(k_stress, 6)
    print*, 'Jacobian', jacobian
    print*, 'epsilon', eps
    do i=1,6
      i_mat = is(i)
      j_mat = js(i)


      F_hat = F + (eps/2.0)*(matmul(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), F) &
              + matmul(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), F))

      ! call kirchhoff_stress(F_hat, G, D, k1, k2, n0, k_stress_mat)
      ! call iso_ogden_sigma(F_hat, mus, alphas, n, k_stress_mat)
      k_stress_mat = 0
      call hgo_sigma(F_hat, mus, alphas, n, k1, k2, a0, K0, k_stress_mat)
      k_stress_mat = k_stress_mat * det(F_hat)
      call matrix_to_voignt(k_stress_mat, k_stress_hat)
      print*, 'i = ', i
      print*, 'k_stress_hat'
      call print_vector(k_stress_hat, 6)
      print*, 'C column', (1.0/(jacobian * eps)) * (k_stress_hat - k_stress)
      C(:, i) = (1.0/(jacobian * eps)) * (k_stress_hat - k_stress)
    enddo
!    print *, 'C'
!    call print_matrix(C, 6, 6)
  end

subroutine hgo_C(F, mus, alphas, n_order, k1, k2, a0, K0, C)
  double precision :: F(3, 3), mus(n_order), alphas(n_order), k1, k2, a0(3), K0
  double precision :: C_og(6,6), C_an(6,6), C_vol(6,6), eps
  integer :: n_order
  double precision :: C(6,6)

  C = 0
  C_og = 0
  C_an = 0
  C_vol = 0

  eps = 1.0e-9

  call apprx_ogden_C(F, mus, alphas, n_order, C_og, eps)
  call aniso_C_bar(F, C_an, k1, k2, a0)
  call vol_C(F, K0, C_vol)

  C = C_og + C_an + C_vol
end

!********************************************
!************** Ogden ***********************
!********************************************

subroutine iso_ogden_sigma(F, mus, alphas, n_order, sigma)
  !*****************************************************
  !******************* Routine initiations **************
  !*****************************************************
  ! inputs
   double precision :: F(3,3), mus(n_order), alphas(n_order)
   integer :: n_order
   ! used inside
   double precision :: jacobian, trace_comp
   double precision :: lambda_bars(3), n_s(3, 3), WORK(16)
   integer :: i, j, p, INFO
   ! output
   double precision :: sigma(3,3)
   !function
   double precision :: det
   !*****************************************************
   !************** Actual code **************************
   !*****************************************************
    print*, a
   jacobian = det(F)
   n_s = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))
   call dsyev('V', 'U', 3, n_s, 3, lambda_bars, WORK, 16, INFO)

   do i=1,3
     lambda_bars(i) = lambda_bars(i) ** 0.5
   enddo

   do p =1,n_order
     trace_comp = 0
     do j = 1,3
       trace_comp = trace_comp + lambda_bars(j)**alphas(p)
     enddo
     trace_comp = - trace_comp * (1.0/3.0)
     do i=1,3
       sigma = sigma + (1.0/jacobian) * mus(p) * (lambda_bars(i)**alphas(p) + trace_comp) &
       * spread(n_s(:, i),dim=2,ncopies=3)*spread(n_s(:, i),dim=1,ncopies=3)
     enddo
   enddo
end

subroutine ogden_c_bar(F, mus, alphas, Nps, c)
    integer :: Nps
    doubleprecision :: F(3, 3), C(6, 6), mus(Nps), alphas(Nps)
    integer :: i, j, k, l, ci, cj, is(6), js(6), INFO, delta
    doubleprecision :: jacobain, ns(3, 3), lambda_bars(3), sigma(3, 3), WORK(16)
    doubleprecision :: det, ogden_c_bar_ijkl

   jacobian = det(F)
    ns = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))
    call dsyev('V', 'U', 3, ns, 3, lambda_bars, WORK, 16, INFO)

    do i=1,3
     lambda_bars(i) = lambda_bars(i) ** 0.5
    enddo
    sigma = 0

    call iso_ogden_sigma(F, mus, alphas, Nps, sigma)

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]


  C = 0

  do ci = 1,6
    do cj=1,6
      i = is(ci)
      j = js(ci)
      k = is(cj)
      l = js(cj)


      C(ci, cj) = ogden_c_bar_ijkl(lambda_bars, ns, mus, alphas, Nps, F, sigma, i, j, k, l)
      ! print*, aniso_C_ijkl(F, jacobian, k1, k2, a0, i, j, k, l)
    enddo
  enddo
    print*, 'sigma'
    call print_matrix(sigma, 3, 3)

  do ci = 1,6
    do cj=1,6
      i = is(ci)
      j = js(ci)
      k = is(cj)
      l = js(cj)


!      C(ci, cj) = C(ci, cj) - C(ci, cj)*3.0/2.0 * (2 - delta(i, j) - delta(i, k))
      ! print*, aniso_C_ijkl(F, jacobian, k1, k2, a0, i, j, k, l)
    enddo
  enddo
end

function ogden_c_bar_ijkl(lambdas, ns, mus, alphas, Nps, F, sigma, i, j, k, l) result(c_val)
    integer :: i, j, k, l, Nps
    doubleprecision :: lambdas(3), ns(3, 3), mus(Nps), alphas(Nps), F(3, 3), sigma(3, 3)
    doubleprecision :: mult1, mult2, mult3, add1, jacobian, det
    integer :: a, b, c, p, delta
    doubleprecision :: c_val
    mult1 = 0
    mult2 = 0
    mult3 = 0
    jacobian = det(F)
    c_val = 0
    do a=1,3
        do b=1,3
            mult1 = 0
            add1 = 0
            do p=1,Nps
                do c=1,3
!                    add1 = add1 + lambdas(c)**alphas(p)
                    add1 = add1 + lambdas(c)**alphas(p)
                end do
                mult1 = mult1 + mus(p) * alphas(p) * (lambdas(a)**alphas(p) - lambdas(b)**alphas(p)/3.0 - &
                        lambdas(a)**alphas(p)/3.0 + 1.0/9.0 * add1)
            end do
            c_val = c_val + mult1 * ns(i, a) * ns(j, a) * ns(k, b) * ns(l, b) / jacobian
        end do
    end do

    do a=1,3
        c_val = c_val - 2.0*sigma(a, a) * ns(i, a) * ns(j, a) * ns(k, a) * ns(l, a)
    end do

    do a = 1,3
        do b = 1,3
            if (abs(lambdas(a)**2 - lambdas(b)**2) > 1e-12) then
                print*, 'yes'
                print*, 2 * ((sigma(a, a)*lambdas(b)**2 - sigma(b, b)*lambdas(a)**2)/(lambdas(a)**2 - lambdas(b)**2)) &
                * ns(i, a) * ns(j, b) * ns(k, a) * ns(l, b)
!                c_val = c_val + 2 * ((sigma(a, a)*lambdas(b)**2 - sigma(b, b)*lambdas(a)**2)/(lambdas(a)**2 - lambdas(b)**2)) &
                c_val = c_val - ((sigma(a, a)*lambdas(b)**2 - sigma(b, b)*lambdas(a)**2)/(lambdas(a)**2 - lambdas(b)**2)) &
                * (ns(i, a) * ns(j, b) * ns(k, a) * ns(l, b) + ns(i, a) * ns(j, b) * ns(k, b) * ns(l, a))
            end if
        end do
    end do
    c_val = c_val + delta(k, l) * sigma(i, j)
    c_val = delta(j,k)*delta(i,l)  - 2./3. *delta(i,j) * delta(k,l) +  delta(i,k) * delta(j,l)
end function ogden_c_bar_ijkl


subroutine apprx_ogden_C(F, mus, alphas, n, C, eps)
  double precision :: F(3,3), C(6,6), mus(n), alphas(n), eps
  double precision :: F_hat(3,3), k_stress(6), k_stress_hat(6), k_stress_mat(3,3), jacobian
  double precision :: Id(3,3), det, es(3, 3, 1)
  integer :: is(6), js(6), i, i_mat, j_mat, n

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]
  call identity(Id, 3)
  jacobian = det(F)

  es = 0
  es(1, 1, 1) = 1
  es(2, 2, 1) = 1
  es(3, 3, 1) = 1


  k_stress_mat = 0
  call iso_ogden_sigma(F, mus, alphas, n, k_stress_mat)
  k_stress_mat = k_stress_mat * jacobian
  ! print*, 'F'
  ! call print_matrix(F, 3, 3)
  ! print*,
  ! print*, 'J ', jacobian
  ! print*,
  ! print*, 'mus '
  ! call print_vector(mus, n)
  ! print*,
  ! print*, 'alphas '
  ! call print_vector(alphas, n)
  ! print*,
  !
  ! print*, 'k_stress'
  ! call print_matrix(k_stress_mat, 3, 3)
  ! print*,
  call matrix_to_voignt(k_stress_mat, k_stress)

  do i=1,6
    i_mat = is(i)
    j_mat = js(i)

    F_hat = F + (eps/2.0)*(matmul(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), F) &
            + matmul(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), F))

    k_stress_mat = 0
    call iso_ogden_sigma(F_hat, mus, alphas, n, k_stress_mat)
    k_stress_mat = k_stress_mat * det(F_hat)

    call matrix_to_voignt(k_stress_mat, k_stress_hat)

    C(:, i) = (1.0/(jacobian * eps)) * (k_stress_hat - k_stress)
  enddo
end

!********************************************
!************** Aniso ***********************
!********************************************

subroutine aniso_bar_sigma(F, k1, k2, a0, sigma)
   double precision :: F(3,3), k1, k2, a0(3)
   double precision :: C_bar(3,3), jacobian, I4, psi4, Id(3,3)
   double precision :: sigma(3,3)
   double precision :: det

   call identity(Id, 3)

   jacobian = det(F)
   C_bar = matmul(transpose(F), F) * (jacobian ** (-2.0/3.0))
   I4 = dot_product(a0, matmul(C_bar, a0))
   psi4 =  (I4-1) * k1 * exp(k2 * (I4-1)**2)
   sigma = sigma + (2.0/jacobian) * psi4 &
   * (matmul(F, matmul(spread(a0,dim=2,ncopies=3)*spread(a0,dim=1,ncopies=3), transpose(F))) &
   * jacobian ** (-2.0/3.0) - (1.0/3.0) * I4 * Id)
end

subroutine aniso_C_bar(F, C, k1, k2, a0)
  double precision :: F(3, 3), a0(3), k1, k2
  integer :: i, j, k, l
  integer :: ci, cj
  integer :: is(6), js(6)
  double precision :: jacobian
  double precision :: det, tr, aniso_C_bar_ijkl
  double precision :: C(6,6)

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]

  jacobian = det(F)

  C = 0

  do ci = 1,6
    do cj=1,6
      i = is(ci)
      j = js(ci)
      k = is(cj)
      l = js(cj)


      C(ci, cj) = aniso_C_bar_ijkl(F, jacobian, k1, k2, a0, i, j, k, l)
      ! print*, aniso_C_ijkl(F, jacobian, k1, k2, a0, i, j, k, l)
    enddo
  enddo
end

function aniso_C_bar_ijkl(F, jacobian, k1, k2, a0, i, j, k, l) result(c)
   double precision :: F(3,3), jacobian, k1, k2, a0(3)
   integer :: i, j, k, l
   integer :: delta
   double precision :: tr
   double precision :: c, a(3), C_bar(3,3), I4

   a = matmul(F,a0) * jacobian **(-1.0/3.0)
   C_bar = matmul(transpose(F),F) * jacobian **(-1.0/3.0)
   I4 = dot_product(a0,matmul(C_bar,a0))

   c = (2.0*k1/jacobian)*exp(k2*(I4-1)**2) * (2.0*(1+2.0*k2*(I4-1)**2) &
   *(a(i)*a(j)-(1.0/3.0)*I4*delta(i,j))*(a(k)*a(l)-(1.0/3.0)*I4*delta(k,l))&
   +(I4-1)*(delta(i,k)*a(j)*a(l)+delta(j,k)*a(i)*a(l) - (2.0/3.0)*(delta(k,l)*a(i)*a(j)+delta(i,j)*a(l)*a(k)) &
   ! +(I4-1)*(0.5*(delta(i,k)*a(j)*a(l)+delta(j,l)*a(i)*a(k)) - (2.0/3.0)*(delta(k,l)*a(i)*a(j)+delta(i,j)*a(l)*a(k)) &
   + 2.0/9.0*I4*delta(i,j)*delta(k,l)))


   c = (2.0*k1/jacobian)*exp(k2*(I4-1)**2) * (2.0*(1+2.0*k2*(I4-1)**2) &
   *(a(i)*a(j)-(1.0/3.0)*I4*delta(i,j))*(a(k)*a(l)-(1.0/3.0)*I4*delta(k,l))&
   +(I4-1)*((delta(i,k)*a(j)*a(l) + delta(j,l)*a(i)*a(k) + delta(j,k)*a(i)*a(l) + delta(i,l)*a(j)*a(k))/2 &
   - (2.0/3.0)*(delta(k,l)*a(i)*a(j)+delta(i,j)*a(l)*a(k)) &
   + 2.0/9.0*I4*delta(i,j)*delta(k,l)))

end function aniso_C_bar_ijkl

!********************************************
!************** Vol ***********************
!********************************************

subroutine vol_sigma(F, K0, sigma)
   double precision :: F(3,3), K0
   double precision :: jacobian, Id(3,3)
   double precision :: sigma(3,3)
   double precision :: det

   call identity(Id, 3)
   jacobian = det(F)

   sigma = sigma + k0*(jacobian - 1.0) *Id
end

subroutine vol_C(F, K0, C)
  double precision :: F(3, 3), K0
  integer :: i, j, k, l
  integer :: ci, cj
  integer :: is(6), js(6)
  double precision :: jacobian
  double precision :: det, tr, vol_C_ijkl
  double precision :: C(6,6)

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]

  jacobian = det(F)

  C = 0

  do ci = 1,6
    do cj=1,6
      i = is(ci)
      j = js(ci)
      k = is(cj)
      l = js(cj)

      C(ci, cj) = vol_C_ijkl(jacobian, K0, i, j, k, l)
    enddo
  enddo
end

function vol_C_ijkl(jacobian, K0, i, j, k, l) result(c)
   double precision :: jacobian, K0
   integer :: i, j, k, l
   integer :: delta
   double precision :: c

   c = (K0) * (2*jacobian - 1) * delta(i,j) * delta(k,l)
end function vol_C_ijkl
