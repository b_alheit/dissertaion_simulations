import numpy as np

n = np.array([1., 2., 3.])
n /= np.linalg.norm(n)
N = np.array([1., 2., 3.])
N /= np.linalg.norm(N)

n_o_N = np.outer(n, N)

N_o_n = np.outer(N, n)

print("n outer N", n_o_N)
print("N outer n", N_o_n)
print("mat mul", np.matmul(N_o_n, n_o_N))