import numpy as np
import matplotlib.pyplot as plt

# t = 0
t = 2
# t = 3.5

# dist = t/2
dist = 0
# col_trans = 2
col_trans = 1

func = lambda x: 3*np.sin(5*x + np.pi/2)
# func = lambda x: (1/(1+np.abs(x)))*3*np.sin(5*x)
# func = lambda x: (1/(1+np.abs(x)))**(-1)*3*np.sin(5*x)
# func = lambda x: np.e**(-x/6)*3*np.sin(5*x)
# func = lambda x: np.abs(x)*3*np.sin(5*x)

def switch(x,switch_position, width_scale):
    return 1/(1+np.e**(-width_scale*(x-switch_position)))


def top_adjust(x, y, mult, place, width, theta):
    angle_deg = 90 + 45 * np.sin(theta-np.pi/2)
    # angle_rad = np.pi/2 + np.pi/4 * np.sin(theta-np.pi/2)
    angle_rad = np.pi/2 - np.pi/4 * np.cos(theta)
    X = x - dist * np.cos(angle_rad)
    sig_func = switch(-X, place, width)+switch(X, place, width)
    Y = (y + dist * np.sin(angle_rad) + col_trans/2)*(1-sig_func) + mult*(np.abs(X))*sig_func
    return X, Y


def bottom_adjust(x, y, mult, place, width, theta):
    angle_deg = 90 + 45 * np.sin(theta-np.pi/2)
    angle_rad = np.pi/2 + np.pi/4 * np.sin(theta-np.pi/2)
    X = x + dist * np.cos(angle_rad)
    sig_func = switch(-X, place, width)+switch(X, place, width)
    plt.plot(X, sig_func)
    plt.plot(X, 1-sig_func)
    plt.show()
    Y = (y - dist * np.sin(angle_rad) -col_trans/2)*(1-sig_func) - mult*(np.abs(X))*sig_func
    other = - mult*500*(switch(-x, place, width)+switch(x, place, width))
    org = (y - dist * np.sin(angle_rad) -col_trans/2)*(1-(switch(-x, place, width)))
    return X, Y, other, org


x = np.linspace(-11, 11, 500)
y = func(x)

mult = 0.5
place = 8
sharpness = 0.6

X1, Y1 = top_adjust(x, y, mult, place, sharpness, 5*x+ np.pi/2)
X2, Y2, ot, org = bottom_adjust(x, y, mult, place, sharpness, 5*x+ np.pi/2)

plt.plot(x, y, label="Imaginary center")
plt.plot(X1, Y1, label="Top surface")
plt.plot(X2, Y2, label="Bottom surface")
# plt.plot(X2, ot)
# plt.plot(X2, org)
# plt.plot(x, 1-(switch(-x, 5, 3)+switch(x, 5, 3)))
plt.grid()
plt.legend()
# plt.savefig('../../../../Meetings/dissertation_meetings/2. _model_geometry_and_implimenting_ogden/slides/figures/demo_col_func.png')
# plt.savefig('../../../../Meetings/dissertation_meetings/2. _model_geometry_and_implimenting_ogden/slides/figures/demo_col_func_problem.png')
# plt.savefig('../../../../Meetings/dissertation_meetings/2. _model_geometry_and_implimenting_ogden/slides/figures/col_pure_translation.png')
# plt.savefig('../../../../Meetings/dissertation_meetings/2. _model_geometry_and_implimenting_ogden/slides/figures/col_compromise.png')
# plt.savefig('../../../../Meetings/dissertation_meetings/2. _model_geometry_and_implimenting_ogden/slides/figures/possibility_5.png')
plt.show()