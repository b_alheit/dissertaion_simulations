!********************************************
!************** All together ****************
!********************************************

subroutine hgo_sigma(F, mus, alphas, n_order, k1, k2, a0, D, sigma)
  ! All working hundreds
  double precision :: F(3, 3), mus(n_order), alphas(n_order), k1, k2, a0(3), D
  integer :: n_order
  double precision :: sigma(3,3)

  sigma = 0
  call iso_ogden_sigma(F, mus, alphas, n_order, sigma)
  call aniso_bar_sigma(F, k1, k2, a0, sigma)
  call vol_sigma(F, D, sigma)
end

subroutine hgo_apprx_C(F, mus, alphas, n,  k1, k2, a0, D, C)
  double precision :: F(3,3), C(6,6), mus(n), alphas(n), eps,  k1, k2, a0(3), D
  double precision :: F_hat(3,3), k_stress(6), k_stress_hat(6), k_stress_mat(3,3), jacobian
  double precision :: Id(3,3), det, es(3, 3, 1)
  integer :: is(6), js(6), i, i_mat, j_mat, n

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]
  call identity(Id, 3)
  jacobian = det(F)

  eps = 1.0e-9

  es = 0
  es(1, 1, 1) = 1
  es(2, 2, 1) = 1
  es(3, 3, 1) = 1

  k_stress_mat = 0
  ! call iso_ogden_sigma(F, mus, alphas, n, k_stress_mat)
  call hgo_sigma(F, mus, alphas, n, k1, k2, a0, D, k_stress_mat)
  k_stress_mat = k_stress_mat * jacobian
  call matrix_to_voignt(k_stress_mat, k_stress)

  do i=1,6
    i_mat = is(i)
    j_mat = js(i)


    F_hat = F + (eps/2.0)*(matmul(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), F) &
            + matmul(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), F))

    ! call kirchhoff_stress(F_hat, G, D, k1, k2, n0, k_stress_mat)
    ! call iso_ogden_sigma(F_hat, mus, alphas, n, k_stress_mat)
    k_stress_mat = 0
    call hgo_sigma(F_hat, mus, alphas, n, k1, k2, a0, D, k_stress_mat)
    k_stress_mat = k_stress_mat * det(F_hat)
    call matrix_to_voignt(k_stress_mat, k_stress_hat)

    C(:, i) = (1.0/(jacobian * eps)) * (k_stress_hat - k_stress)
  enddo
end

subroutine hgo_C(F, mus, alphas, n_order, k1, k2, a0, D, C)
  double precision :: F(3, 3), mus(n_order), alphas(n_order), k1, k2, a0(3), D
  double precision :: C_og(6,6), C_an(6,6), C_vol(6,6), eps
  integer :: n_order
  double precision :: C(6,6)

  C = 0
  C_og = 0
  C_an = 0
  C_vol = 0

  eps = 1.0e-9

  call apprx_ogden_C(F, mus, alphas, n_order, C_og, eps)
  call aniso_C_bar(F, C_an, k1, k2, a0)
  call vol_C(F, D, C_vol)

  C = C_og + C_an + C_vol
end

!********************************************
!************** Ogden ***********************
!********************************************

subroutine iso_ogden_sigma(F, mus, alphas, n_order, sigma)
  !*****************************************************
  !******************* Routine initiations **************
  !*****************************************************
  ! inputs
   double precision :: F(3,3), mus(n_order), alphas(n_order)
   integer :: n_order
   ! used inside
   double precision :: jacobian, trace_comp
   double precision :: lambda_bars(3), n_s(3, 3), WORK(16)
   integer :: i, j, p, INFO
   ! output
   double precision :: sigma(3,3)
   !function
   double precision :: det
   !*****************************************************
   !************** Actual code **************************
   !*****************************************************

   jacobian = det(F)
   n_s = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))
   call dsyev('V', 'U', 3, n_s, 3, lambda_bars, WORK, 16, INFO)

   do i=1,3
     lambda_bars(i) = lambda_bars(i) ** 0.5
   enddo

   do p =1,n_order
     trace_comp = 0
     do j = 1,3
       trace_comp = trace_comp + lambda_bars(j)**alphas(p)
     enddo
     trace_comp = - trace_comp * (1.0/3.0)
     do i=1,3
       sigma = sigma + (1.0/jacobian) * mus(p) * (lambda_bars(i)**alphas(p) + trace_comp) &
       * spread(n_s(:, i),dim=2,ncopies=3)*spread(n_s(:, i),dim=1,ncopies=3)
     enddo
   enddo
end

subroutine apprx_ogden_C(F, mus, alphas, n, C, eps)
  double precision :: F(3,3), C(6,6), mus(n), alphas(n), eps
  double precision :: F_hat(3,3), k_stress(6), k_stress_hat(6), k_stress_mat(3,3), jacobian
  double precision :: Id(3,3), det, es(3, 3, 1)
  integer :: is(6), js(6), i, i_mat, j_mat, n

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]
  call identity(Id, 3)
  jacobian = det(F)

  es = 0
  es(1, 1, 1) = 1
  es(2, 2, 1) = 1
  es(3, 3, 1) = 1


  k_stress_mat = 0
  call iso_ogden_sigma(F, mus, alphas, n, k_stress_mat)
  k_stress_mat = k_stress_mat * jacobian
  ! print*, 'F'
  ! call print_matrix(F, 3, 3)
  ! print*,
  ! print*, 'J ', jacobian
  ! print*,
  ! print*, 'mus '
  ! call print_vector(mus, n)
  ! print*,
  ! print*, 'alphas '
  ! call print_vector(alphas, n)
  ! print*,
  !
  ! print*, 'k_stress'
  ! call print_matrix(k_stress_mat, 3, 3)
  ! print*,
  call matrix_to_voignt(k_stress_mat, k_stress)

  do i=1,6
    i_mat = is(i)
    j_mat = js(i)

    F_hat = F + (eps/2.0)*(matmul(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), F) &
            + matmul(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), F))

    k_stress_mat = 0
    call iso_ogden_sigma(F_hat, mus, alphas, n, k_stress_mat)
    k_stress_mat = k_stress_mat * det(F_hat)

    call matrix_to_voignt(k_stress_mat, k_stress_hat)

    C(:, i) = (1.0/(jacobian * eps)) * (k_stress_hat - k_stress)
  enddo
end

!********************************************
!************** Aniso ***********************
!********************************************

subroutine aniso_bar_sigma(F, k1, k2, a0, sigma)
   double precision :: F(3,3), k1, k2, a0(3)
   double precision :: C_bar(3,3), jacobian, I4, psi4, Id(3,3)
   double precision :: sigma(3,3)
   double precision :: det

   call identity(Id, 3)

   jacobian = det(F)
   C_bar = matmul(transpose(F), F) * (jacobian ** (-2.0/3.0))
   I4 = dot_product(a0, matmul(C_bar, a0))
   psi4 =  (I4-1) * k1 * exp(k2 * (I4-1)**2)
   sigma = sigma + (2.0/jacobian) * psi4 &
   * (matmul(F, matmul(spread(a0,dim=2,ncopies=3)*spread(a0,dim=1,ncopies=3), transpose(F))) &
   * jacobian ** (-2.0/3.0) - (1.0/3.0) * I4 * Id)
end

subroutine aniso_C_bar(F, C, k1, k2, a0)
  double precision :: F(3, 3), a0(3), k1, k2
  integer :: i, j, k, l
  integer :: ci, cj
  integer :: is(6), js(6)
  double precision :: jacobian
  double precision :: det, tr, aniso_C_bar_ijkl
  double precision :: C(6,6)

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]

  jacobian = det(F)

  C = 0

  do ci = 1,6
    do cj=1,6
      i = is(ci)
      j = js(ci)
      k = is(cj)
      l = js(cj)


      C(ci, cj) = aniso_C_bar_ijkl(F, jacobian, k1, k2, a0, i, j, k, l)
      ! print*, aniso_C_ijkl(F, jacobian, k1, k2, a0, i, j, k, l)
    enddo
  enddo
end

function aniso_C_bar_ijkl(F, jacobian, k1, k2, a0, i, j, k, l) result(c)
   double precision :: F(3,3), jacobian, G, D, k1, k2, a0(3)
   integer :: i, j, k, l
   integer :: delta
   double precision :: tr
   double precision :: c, a(3), C_bar(3,3), I4

   a = matmul(F,a0) * jacobian **(-1.0/3.0)
   C_bar = matmul(transpose(F),F) * jacobian **(-1.0/3.0)
   I4 = dot_product(a0,matmul(C_bar,a0))

   c = (2.0*k1/jacobian)*exp(k2*(I4-1)**2) * (2.0*(1+2.0*k2*(I4-1)**2) &
   *(a(i)*a(j)-(1.0/3.0)*I4*delta(i,j))*(a(k)*a(l)-(1.0/3.0)*I4*delta(k,l))&
   +(I4-1)*(delta(i,k)*a(j)*a(l)+delta(j,k)*a(i)*a(l) - (2.0/3.0)*(delta(k,l)*a(i)*a(j)+delta(i,j)*a(l)*a(k)) &
   ! +(I4-1)*(0.5*(delta(i,k)*a(j)*a(l)+delta(j,l)*a(i)*a(k)) - (2.0/3.0)*(delta(k,l)*a(i)*a(j)+delta(i,j)*a(l)*a(k)) &
   + (2.0/9.0)*(I4*delta(i,j)*delta(k,l))))
end function aniso_C_bar_ijkl

!********************************************
!************** Vol ***********************
!********************************************

subroutine vol_sigma(F, D, sigma)
   double precision :: F(3,3), D
   double precision :: jacobian, Id(3,3)
   double precision :: sigma(3,3)
   double precision :: det

   call identity(Id, 3)
   jacobian = det(F)

   sigma = sigma + (2.0/D)*(jacobian - 1.0) *Id
end

subroutine vol_C(F, D, C)
  double precision :: F(3, 3), D
  integer :: i, j, k, l
  integer :: ci, cj
  integer :: is(6), js(6)
  double precision :: jacobian
  double precision :: det, tr, vol_C_ijkl
  double precision :: C(6,6)

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]

  jacobian = det(F)

  C = 0

  do ci = 1,6
    do cj=1,6
      i = is(ci)
      j = js(ci)
      k = is(cj)
      l = js(cj)

      C(ci, cj) = vol_C_ijkl(jacobian, D, i, j, k, l)
    enddo
  enddo
end

function vol_C_ijkl(jacobian, D, i, j, k, l) result(c)
   double precision :: jacobian, D
   integer :: i, j, k, l
   integer :: delta
   double precision :: c

   c = (2.0/D) * (2*jacobian - 1) * delta(i,j) * delta(k,l)
end function vol_C_ijkl
