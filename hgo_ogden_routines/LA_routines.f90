
subroutine voignt_to_matrix(in, out)
  double precision :: in(6), out(3, 3)

  out(1,1) = in(1)
  out(2,2) = in(2)
  out(3,3) = in(3)

  out(2,3) = in(4)
  out(1,3) = in(5)
  out(1,2) = in(6)

  out(3,2) = in(4)
  out(3,1) = in(5)
  out(2,1) = in(6)

end


subroutine  tensor_product_2(mat_A, mat_B, len_a, len_b, out)
  integer :: len_a, len_b
  real:: mat_A(len_a, 1), mat_B(len_b, 1)
  real, dimension(len_a,len_b) :: out
  integer :: i,j

  do i = 1,len_a
    do j = 1,len_b
        out(i,j) = mat_A(i,1) * mat_B(j,1)
    enddo
  enddo
end

subroutine  tensor_product_1(mat_A, mat_B, len_a, len_b, out)
  integer :: len_a, len_b
  double precision:: mat_A(len_a), mat_B(len_b)
  double precision, dimension(len_a,len_b) :: out
  integer :: i,j

  do i = 1,len_a
    do j = 1,len_b
        out(i,j) = mat_A(i) * mat_B(j)
    enddo
  enddo
end


function delta(i, j) result(d)
   integer, intent(in) :: i! input
   integer :: d
   ! integer             :: j ! output
   if (i==j)then
     d = 1
   else
     d = 0
   endif
end function delta

function det(mat) result(d)
  double precision, intent(in) :: mat(3,3)
  double precision :: d

  d = mat(1,1) * (mat(2,2)*mat(3,3) - mat(2, 3) * mat(3,2))
  d = d - mat(1,2) * (mat(2,1)*mat(3,3) - mat(2, 3) * mat(3,1))
  d = d + mat(1,3) * (mat(2,1)*mat(3,2) - mat(2, 2) * mat(3,1))
end function det

function tr(mat) result(d)
  double precision, intent(in) :: mat(3,3)
  double precision :: d

  d = mat(1,1) + mat(2,2) + mat(3,3)
end function tr

function heaviside(x) result(d)
  double precision, intent(in) :: x
  double precision :: d, a

  a= 1.0
  d = 0.5*(sign(a,x)+1.0)
end function heaviside

subroutine identity(in_mat, n)
  integer :: n, i
  double precision :: in_mat(n,n)
  in_mat = 0
  do i = 1,n
      in_mat(i,i) = 1.0
  enddo
end

subroutine matrix_to_voignt(in, out)
  double precision :: in(3,3), out(6)
  out(1) = in(1,1)
  out(2) = in(2,2)
  out(3) = in(3,3)

  out(4) = in(2,3)
  out(5) = in(1,3)
  out(6) = in(1,2)
end

subroutine print_matrix(mat, rows, collumns)
  integer :: rows, collumns
  double precision :: mat(rows, collumns)
  integer :: i, j

  do i =1,rows
      print*,mat(i,:)
  enddo
end

subroutine print_vector(mat, rows)
  integer :: rows
  double precision :: mat(rows)
  integer :: i

  do i =1,rows
      print*,mat(i)
  enddo
end
