import numpy as np
import matplotlib.pyplot as plt

title = 'Stress vs strech in the same direction'
x_axis = '$\lambda_i$'
y_axis = '$\sigma_{ii}$'

plots = [#('vol_uni_test_stretch.dat', 'vol_uni_test_stress.dat', "Direction 1 - anisotropic - non-compressive fibres", 3, 'blue')]
         # ('no_fibre_stretch.dat', 'no_fibre_stress.dat', "Direction 1 - anisotropic - compressive fibres", 1.5, 'orange')]
         # ('no_og_stretch.dat', 'no_og_stress.dat', "Direction 1 - anisotropic - compressive fibres", 1.5, 'orange')]
         ('just_vol_stretch.dat', 'just_vol_stress.dat', "Direction 1 - anisotropic - compressive fibres", 1.5, 'orange')]
         # ('nh stretch1c.txt', 'nh stress11c.txt', "Direction 1 - anisotropic - compressive fibres", 1.5, 'orange'),
         # ('nh stretch1i.txt', 'nh stress11i.txt', "Direction 1 - isotropic", 5, 'green'),
         # ('nh stretch2.txt', 'nh stress22.txt', "Direction 2 - anisotropic - compressive fibres", 3, 'red'),
         # ('og stretch1i.txt', 'og stress11i.txt', "Ogden - isotropic", 1, 'black')]


def load_data(file_name):
    data = np.loadtxt(fname=file_name,
                        dtype=str,
                        delimiter='\n ')
    return data[data != ''].astype(np.float)

for i in range(len(plots)):
    x = load_data(plots[i][0])
    y = load_data(plots[i][1])
    name = plots[i][2]
    plt.plot(x, y, label=name, linewidth=plots[i][3], color=plots[i][4])

plt.xlabel(x_axis)
plt.ylabel(y_axis)
plt.title(title)
plt.legend()
plt.grid()
plt.show()
