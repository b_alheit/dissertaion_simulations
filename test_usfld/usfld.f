      SUBROUTINE USDFLD(FIELD,STATEV,PNEWDT,DIRECT,T,CELENT, &
      TIME,DTIME,CMNAME,ORNAME,NFIELD,NSTATV,NOEL,NPT,LAYER, &
      KSPT,KSTEP,KINC,NDI,NSHR,COORD,JMAC,JMATYP,MATLAYO, &
      LACCFLA)

      INCLUDE 'ABA_PARAM.INC'

      CHARACTER*80 CMNAME,ORNAME
      CHARACTER*3  FLGRAY(15)
      DIMENSION FIELD(NFIELD),STATEV(NSTATV),DIRECT(3,3), &
      T(3,3),TIME(2)
      DIMENSION ARRAY(15),JARRAY(15),JMAC(*),JMATYP(*), &
      COORD(*)

! Absolute value of current strain:
      CALL GETVRM('E',ARRAY,JARRAY,FLGRAY,JRCD,JMAC,JMATYP, &
      MATLAYO,LACCFLA)
      EPS = ABS( ARRAY(1) )
! Maximum value of strain up to this point in time:
      CALL GETVRM('SDV',ARRAY,JARRAY,FLGRAY,JRCD,JMAC,JMATYP, &
      MATLAYO,LACCFLA)
      EPSMAX = ARRAY(1)
! Use the maximum strain as a field variable
      FIELD(1) = MAX( EPS , EPSMAX )
! Store the maximum strain as a solution dependent state 
! variable
      STATEV(1) = FIELD(1)
! If error, write comment to .DAT file:
      IF(JRCD.NE.0)THEN
       WRITE(6,*) 'REQUEST ERROR IN USDFLD FOR ELEMENT NUMBER ', &
          NOEL,'INTEGRATION POINT NUMBER ',NPT
      ENDIF
!
      RETURN
      END
