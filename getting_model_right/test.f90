! Created by  on 2019/05/04.

program test
    integer :: n
    double precision :: F(3,3), sigma(3,3), x_start, x_end, mus(3), alphas(3)
    double precision :: k1, k2, a0(3), C_apprx(6, 6), C_exact(6, 6)
    double precision :: K0, G, det, eps
    character(len=40) :: name
    integer :: n_pts

    call identity(F, 3)

    F(1, 1) = 1.2
    F(2, 2) = 0.8
    F(1, 2) = 0.2
    n = 3

    mus = [1, 2, 3]
    alphas = [1, 2, 3]

    k1 = 1
    k2 = 1

    K0 = 1

    a0 = [1, 2, 3]
    a0 = a0 / norm2(a0)


    F = F * det(F) ** (-1./3.)

    call hgo_sigma(F, mus, alphas, n, k1, k2, a0, K0, sigma)
    C_apprx = 0
    eps = 1e-12
    call hgo_apprx_C(F, mus, alphas, n,  k1, k2, a0, K0, C_apprx)
    print*, 'sigma'
    call print_matrix(sigma, 3, 3)
    print*, 'C approx'
    call print_matrix(C_apprx, 6, 6)
end program test