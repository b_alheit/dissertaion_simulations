program test
  integer :: n
  double precision :: F(3,3), sigma(3,3), x_start, x_end, mus(1), alphas(1)
  double precision :: k1, k2, a0(3), C_apprx(6, 6), C_exact(6, 6)
  double precision :: K0, G, det, eps
  character(len=40) :: name
  integer :: n_pts

  name = "no_fibre"

  sigma = 0
  n_pts = 100
  G = 80.0e3

  ! n = 4
  ! alphas = [2.7971, -2.7188, 10.505, 0.33382]
  ! mus = [0.77817, -0.011229, 1.268e-7, 16.169]


  n = 1
  alphas = [1.0]
  mus = [1]


!  mus = 0
!  alphas = 0

!  k1 = 1.56e3
!  k1 = 2.3e3
!  k2 = 0.4465
  ! k1 = 0
  ! k2 = 0.0
  ! a0 = [5, 2, 2]
  a0 = [1.0, 2.0, 3.0]
  a0 = a0 /(norm2(a0))
  k1 = 0
  k2 = 0
  print*, 'nrom a0', norm2(a0)


!  K0 = 1000
!  K0 = 1000

  call identity(F, 3)
  F(1,1) = 2.5
   F(1,1) = 1.9
  ! F(1,1) = 1.0
  F(2,2) = 1.0
!  F(2,2) = F(1, 1) ** (-0.5)
  F(3,3) = 1/(F(1, 1) * F(2, 2))
  F(1, 2) = 0.5
  F(2, 1) = 2
!  F(3,3) = F(1, 1) ** (-0.5)
!  F(3,3) = 0.2
  F = F / det(F) ** (-1./3.)
  x_start = 1.0
  x_end = 1.7
  ! call  test_iso_og(F, sigma, mus, alphas, n, x_start, x_end, n_pts)
  ! call  test_iso_fibre(F, sigma, k1, k2, a0, x_start, x_end, n_pts)
  ! call  test_vol(F, D, sigma, x_start, x_end, n_pts)
!  call all_test_stress(F, mus, alphas, n, k1, k2, a0, D, sigma, x_start, x_end, n_pts, name)
  C_apprx = 0
  C_exact = 0
  eps = 1e-12
  call hgo_apprx_C(F, mus, alphas, n,  k1, k2, a0, K0, C_apprx)
!  call apprx_C(F, mus, alphas, n,  k1, k2, a0, K0, C_apprx, eps)
!  call aniso_C_bar(F, C_exact, k1, k2, a0)
!  call vol_C(F, K0, C_exact)
  call ogden_c_bar(F, mus, alphas, n, C_exact)

!  print*, 'F'
!  call print_matrix(F, 3, 3)
!  print*, 'det(F)', det(F)
  print*, 'C exact'
  call print_matrix(C_exact, 6, 6)
  print*, 'C approx'
  call print_matrix(C_apprx, 6, 6)
  print*, 'C diff %'
  call print_matrix(100 * abs((C_apprx-C_exact)/C_exact), 6, 6)
  print*, 'abs error sum', sum(abs((C_apprx-C_exact)))
  ! call test_C(F, mus, alphas, n,  k1, k2, a0, D)
!  print*, 'c_2323 = ', (F(1, 1) + F(2, 2)/2.)/3.
!  print*, 'mult1 = ', (F(1, 1) + 5*F(2, 2))/9, 'mult2 = ', (F(1, 1) - 4*F(2, 2))/9, 'sigma_b = ', (-F(1, 1) + F(2, 2))/3.
!  print*, 'c_2323 = ', ((F(1, 1) + 5*F(2, 2))/9 - (F(1, 1) - 4*F(2, 2))/9)/2 - (-F(1, 1) + F(2, 2))/3.
!  print*, ' det(F)', det(F)
!  print*, 'c_2323 = ', (-F(1, 1) + 5*F(2, 2)/2.)/3.
!  print*, 'F'
!  call print_matrix(F, 3, 3)
end

subroutine test_iso_og(F, sigma, mus, alphas, n, x_start, x_end, n_pts)
  double precision :: F(3,3), sigma(3,3), mus(n), alphas(n), x_start, x_end
  double precision :: Id(3,3), dx
  integer :: n_pts, n, i

  OPEN(UNIT=1,FILE="./results/og_uni_test_stress.dat",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
  OPEN(UNIT=2,FILE="./results/og_uni_test_stretch.dat",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")

  dx = (x_end - x_start)/(n_pts - 1)

  call identity(F, 3)
  call identity(Id, 3)
  print*, 'dx ', dx
  print*, 'n_pts ', n_pts
  do i=1,n_pts
    F(1, 1) = x_start + dx *(i-1)
    F(2, 2) = F(1, 1) ** (-0.5)
    F(3, 3) = F(1, 1) ** (-0.5)
    sigma = 0

    call iso_ogden_sigma(F, mus, alphas, n, sigma)
    sigma = sigma - Id * sigma(3,3)
    sigma = sigma * (1.0/F(1,1))
    write(unit=1, fmt=*) sigma(1, 1)
    write(unit=2, fmt=*) F(1, 1)
  enddo
  close(unit=1)
  close(unit=2)
end

subroutine test_iso_fibre(F, sigma, k1, k2, a0, x_start, x_end, n_pts)
  double precision :: F(3,3), sigma(3,3), x_start, x_end, k1, k2, a0(3)
  double precision :: Id(3,3), dx
  integer :: n_pts, i

  OPEN(UNIT=1,FILE="./results/fibre_uni_test_stress.dat",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
  OPEN(UNIT=2,FILE="./results/fibre_uni_test_stretch.dat",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")

  dx = (x_end - x_start)/(n_pts - 1)

  call identity(F, 3)
  call identity(Id, 3)
  print*, 'dx ', dx
  print*, 'n_pts ', n_pts
  do i=1,n_pts
    F(1, 1) = x_start + dx *(i-1)
    F(2, 2) = F(1, 1) ** (-0.5)
    F(3, 3) = F(1, 1) ** (-0.5)
    sigma = 0

    call aniso_bar_sigma(F, k1, k2, a0, sigma)
    sigma = sigma - Id * sigma(3,3)
    sigma = sigma * (1.0/F(1,1))
    write(unit=1, fmt=*) sigma(1, 1)
    write(unit=2, fmt=*) F(1, 1)
  enddo
  close(unit=1)
  close(unit=2)
end

subroutine test_vol(F, D, sigma, x_start, x_end, n_pts)
  double precision :: F(3,3), sigma(3,3), x_start, x_end, D
  double precision :: Id(3,3), dx
  integer :: n_pts, i

  OPEN(UNIT=1,FILE="./results/vol_uni_test_stress.dat",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
  OPEN(UNIT=2,FILE="./results/vol_uni_test_stretch.dat",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")

  dx = (x_end - x_start)/(n_pts - 1)

  call identity(F, 3)
  call identity(Id, 3)
  print*, 'dx ', dx
  print*, 'n_pts ', n_pts
  do i=1,n_pts
    F(1, 1) = x_start + dx *(i-1)
    F(2, 2) = F(1, 1) ** (-0.5)
    F(3, 3) = F(1, 1) ** (-0.5)
    sigma = 0

    ! print*, 'F'
    ! call print_matrix(F, 3, 3)

    call vol_sigma(F, D, sigma)
    ! sigma = sigma - Id * sigma(3,3)
    ! sigma = sigma * (1.0/F(1,1))
    write(unit=1, fmt=*) sigma(1, 1)
    write(unit=2, fmt=*) F(1, 1)
  enddo
  close(unit=1)
  close(unit=2)
end

subroutine all_test_stress(F, mus, alphas, n_order, k1, k2, a0, D, sigma, x_start, x_end, n_pts, name)
  double precision :: F(3,3), sigma(3,3), x_start, x_end, D
  double precision :: mus(n_order), alphas(n_order), k1, k2, a0(3)
  double precision :: Id(3,3), dx
  character(len=40) :: name
  integer :: n_pts, i, n_order

  OPEN(UNIT=1,FILE="all_stress1.dat",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
  OPEN(UNIT=2,FILE="all_stretch1.dat",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")

  dx = (x_end - x_start)/(n_pts - 1)

  call identity(F, 3)
  call identity(Id, 3)
  print*, 'dx ', dx
  print*, 'x_end ', x_end
  print*, 'x_start ', x_start
  print*, 'n_pts ', n_pts
  do i=1,n_pts
    F(1, 1) = x_start + dx *(i-1)
    F(2, 2) = F(1, 1) ** (-0.5)
    F(3, 3) = F(1, 1) ** (-0.5)

    ! print*, 'F'
    ! call print_matrix(F, 3, 3)

    call hgo_sigma(F, mus, alphas, n_order, k1, k2, a0, D, sigma)
    print*, 'sigma 1'
    call print_matrix(sigma, 3, 3)
    sigma = sigma - Id * sigma(3,3)
    ! sigma = sigma * (1.0/F(1,1))
    print*, 'sigma 2'
    call print_matrix(sigma, 3, 3)
    print*,
    write(unit=1, fmt=*) sigma(1, 1)
    write(unit=2, fmt=*) F(1, 1)
  enddo
  close(unit=1)
  close(unit=2)
end

subroutine test_C(F, mus, alphas, n,  k1, k2, a0, D)
  double precision :: F(3,3), mus(n), alphas(n), eps,  k1, k2, a0(3), D
  integer :: n
  double precision :: C_apprx(6,6), C(6,6)

  C = 0
  C_apprx = 0

  call hgo_C(F, mus, alphas, n, k1, k2, a0, D, C)
  eps = 1e-9
  call apprx_C(F, mus, alphas, n,  k1, k2, a0, D, C_apprx, eps)

  print*, "Stiffness matrix"
  call print_matrix(C, 6, 6)

  print*,
  print*, "Approximate Stiffness matrix"
  call print_matrix(C_apprx, 6, 6)

  print*,
  print*, "Error %"
  call print_matrix(100*(C_apprx - C)/C, 6, 6)
end

subroutine apprx_C(F, mus, alphas, n,  k1, k2, a0, D, C, eps)
  double precision :: F(3,3), C(6,6), mus(n), alphas(n), eps,  k1, k2, a0(3), D
  double precision :: F_hat(3,3), k_stress(6), k_stress_hat(6), k_stress_mat(3,3), jacobian
  double precision :: Id(3,3), det, es(3, 3, 1)
  integer :: is(6), js(6), i, i_mat, j_mat, n

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]
  call identity(Id, 3)
  jacobian = det(F)

  es = 0
  es(1, 1, 1) = 1
  es(2, 2, 1) = 1
  es(3, 3, 1) = 1

  k_stress_mat = 0
  ! call iso_ogden_sigma(F, mus, alphas, n, k_stress_mat)
  call hgo_sigma(F, mus, alphas, n, k1, k2, a0, D, k_stress_mat)
  k_stress_mat = k_stress_mat * jacobian
  call matrix_to_voignt(k_stress_mat, k_stress)

  do i=1,6
    i_mat = is(i)
    j_mat = js(i)


    F_hat = F + (eps/2.0)*(matmul(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), F) &
            + matmul(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), F))

    ! call kirchhoff_stress(F_hat, G, D, k1, k2, n0, k_stress_mat)
    ! call iso_ogden_sigma(F_hat, mus, alphas, n, k_stress_mat)
    k_stress_mat = 0
    call hgo_sigma(F_hat, mus, alphas, n, k1, k2, a0, D, k_stress_mat)
    k_stress_mat = k_stress_mat * det(F_hat)
    call matrix_to_voignt(k_stress_mat, k_stress_hat)

    C(:, i) = (1.0/(jacobian * eps)) * (k_stress_hat - k_stress)
  enddo
end
