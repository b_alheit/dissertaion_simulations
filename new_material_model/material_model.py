import numpy as np


def sigma_bar_aniso(F, a0, k1, k2, k3, k4):
    J = np.linalg.det(F)
    a_bar = np.matmul(F, a0) * J ** (-1/3)
    b_bar = np.matmul(F, F.T) * J ** (-2/3)
    C_bar = np.matmul(F.T, F) * J ** (-2/3)
    I4_bar = np.matmul(a0.T, np.matmul(C_bar, a0))
    I5_bar = np.matmul(a0.T, np.matmul(C_bar, np.matmul(C_bar, a0)))

    tens1 = np.outer(a_bar, a_bar) - 1/3 * I4_bar * np.eye(3, 3)
    tens2 = np.outer(a_bar, np.matmul(b_bar, a_bar)) + np.outer(np.matmul(b_bar, a_bar), a_bar) - 2/3 * I5_bar * np.eye(3, 3)
    print('I5 ', I5_bar)
    print('tens1')
    print(tens1)
    print("tens2")
    print(tens2)
    sigma_bar = (2.0/J) * ((k1*(I4_bar - 1)*np.e**(k2*(I4_bar - 1)**2) - 2*k3*I4_bar*np.e**(k4*(I5_bar - I4_bar**2))) * tens1
                           + k3*np.e**(k4*(I5_bar - I4_bar**2)) * tens2)

    return sigma_bar


def sigma_bar_iso(F, mus, alphas):
    J = np.linalg.det(F)
    b_bar = np.matmul(F, F.T) * J ** (-2/3)
    lambdas_bars, ns = np.linalg.eig(b_bar)

    sigma_bar = np.zeros([3, 3])
    lamba_sums = np.outer(lambdas_bars, np.ones(np.alen(mus)))
    lamba_sums = -1/3 * np.sum(lamba_sums**alphas, axis=0)

    for i in range(3):
        coeff = np.sum(mus * (lambdas_bars[i] ** alphas + lamba_sums))
        sigma_bar += coeff * np.outer(ns[i], ns[i])

    sigma_bar /= J
    return sigma_bar


def sigma_vol(F, K):
    J = np.linalg.det(F)
    sigma = K * (J - 1) * np.eye(3, 3)
    return sigma


def sigma(F, a0, k1, k2, k3, k4, K, mus, alphas):
    iso = sigma_bar_iso(F, mus, alphas)
    aniso = sigma_bar_aniso(F, a0, k1, k2, k3, k4)
    vol = sigma_vol(F, K)
    # print(aniso)

    result = iso + aniso + vol
    return result
