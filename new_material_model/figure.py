import material_model as mm
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['text.usetex'] = True
F = np.eye(3, 3)
end = 0.3
a0 = np.array([1, 0, 0])
k1 = 1000
k2 = 2
k3 = 1000
k4 = 2 * 8
K = 0
mus = np.array([0])
alphas = np.array([0])

F[0, 0] += 0.5
F[1, 1] = F[0, 0] ** (-1/2)
F[2, 2] = F[0, 0] ** (-1/2)

n = 10
x = np.linspace(0, end, n)
sig_stretch_n = np.empty(n, dtype=float)
sig_stretch_o = np.empty(n, dtype=float)
for i in range(n):
    F[0, 0] = 1 + x[i]
    F[1, 1] = F[0, 0] ** (-1 / 2)
    F[2, 2] = F[0, 0] ** (-1 / 2)

    sig_n = mm.sigma(F, a0, k1, k2, k3, k4, K, mus, alphas)
    sig_o = mm.sigma(F, a0, k1, k2, 0, 0, K, mus, alphas)
    sig_n -= sig_n[2, 2] * np.eye(3, 3)
    sig_o -= sig_o[2, 2] * np.eye(3, 3)
    print('stretch, i = ', i)
    print('sig_n')
    print(sig_n)
    print('sig_o')
    print(sig_o)
    sig_stretch_n[i] = sig_n[0, 0]
    sig_stretch_o[i] = sig_o[0, 0]

F = np.eye(3, 3)
sig_shear_n = np.empty(n, dtype=float)
sig_shear_o = np.empty(n, dtype=float)
sig_shear_n_11 = np.empty(n, dtype=float)
sig_shear_o_11 = np.empty(n, dtype=float)
for i in range(n):
    F[0, 1] = x[i]
    F_inv = np.linalg.inv(F)
    sig_n = mm.sigma(F, a0, k1, k2, k3, k4, K, mus, alphas)
    sig_o = mm.sigma(F, a0, k1, k2, 0, 0, K, mus, alphas)
    sig_n -= sig_n[2, 2] * np.eye(3, 3)
    sig_o -= sig_o[2, 2] * np.eye(3, 3)
    S_n = np.matmul(F_inv, np.matmul(sig_n, F_inv.T))
    print('shear, i = ', i)
    print('F')
    print(F)
    # print('S_n')
    # print(S_n)
    print('sig_n')
    print(sig_n)
    print('sig_o')
    print(sig_o)
    sig_shear_n[i] = sig_n[0, 1]
    sig_shear_o[i] = sig_o[0, 1]
    sig_shear_n_11[i] = sig_n[0, 0]
    sig_shear_o_11[i] = sig_o[0, 0]



plt.plot(x, sig_stretch_n, linewidth=3, label='\\textbf{N}, Uniaxial Tension, $i=j=1$')
plt.plot(x, sig_stretch_o, color='yellow', linestyle='--', label='\\textbf{O}, Uniaxial Tension, $i=j=1$')
plt.plot(x, sig_shear_n, label='\\textbf{N}, Shear, $i=1, j=2$')
plt.plot(x, sig_shear_o, linewidth=3, label='\\textbf{O}, Shear, $i=1, j=2$')
plt.plot(x, sig_shear_n_11, label='\\textbf{N}, Shear, $i=1, j=1$')
plt.plot(x, sig_shear_o_11, color='black', linestyle='--', label='\\textbf{O}, Shear, $i=1, j=1$')
plt.grid()
plt.ylabel('$\sigma_{i, j}$', fontsize=14)
plt.xlabel('$\\alpha$', fontsize=14)
plt.legend()
plt.show()
