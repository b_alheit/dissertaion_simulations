import numpy as np
import matplotlib.pyplot as plt

title = 'Uni-axial Loading Using Modified HGO Model'
x_axis = '$\lambda_1$'
y_axis = '$\sigma_{11}$'

plots = [
         ('new_pull_strain.dat', 'new_pull_stress.dat', "new pull", 3   , 'orange'),
         ('old_pull_strain.dat', 'old_pull_stress.dat', "old pull", 1, 'blue'),
         ('new_shear_strain.dat', 'new_shear_stress.dat', "new shear", 1, 'black'),
         ('old_shear_strain.dat', 'old_shear_stress.dat', "old shear", 1, 'red')]
         # ('stretch_aba2.dat', 'stress_aba2.dat', "Abaqus $a_0 = [0,1,0]^T$", 0, 'red')]
         # ('nh stretch1i.txt', 'nh stress11i.txt', "Direction 1 - isotropic", 5, 'green'),
         # ('nh stretch2.txt', 'nh stress22.txt', "Direction 2 - anisotropic - compressive fibres", 3, 'red'),
         # ('og stretch1i.txt', 'og stress11i.txt', "Ogden - isotropic", 1, 'black')]


def load_data(file_name):
    data = np.loadtxt(fname=file_name,
                        dtype=str,
                        delimiter='\n ')
    return data[data != ''].astype(np.float)

for i in range(len(plots)):
    if i<len(plots)-2:
    # if True:
        x = load_data(plots[i][0])
        conv = np.abs(x[1:] - x[:-1]) > 1e-5
        conv = np.append(conv, True)
        y = load_data(plots[i][1])
        x = x[conv]
        y = y[conv]
        name = plots[i][2]
        plt.plot(x, y, label=name, linewidth=plots[i][3], color=plots[i][4])
    else:
        x = load_data(plots[i][0])
        conv = np.abs(x[1:] - x[:-1]) > 1e-5
        conv = np.append(conv, True)
        y = load_data(plots[i][1])
        x = x[conv]
        y = y[conv]
        name = plots[i][2]
        x += 1
        plt.plot(x, y, label=name, linewidth=plots[i][3], color=plots[i][4])
        # x = load_data(plots[i][0])
        # conv = np.abs(x[1:] - x[:-1]) > 4e-4
        # conv = np.append(conv, True)
        # y = load_data(plots[i][1])
        # x = x[conv]
        # y = y[conv]
        # name = plots[i][2]
        # plt.plot(x, y, label=name, linewidth=plots[i][3], color=plots[i][4], marker='s')


plt.xlabel(x_axis)
plt.ylabel(y_axis)
plt.title(title)
plt.legend()
plt.grid()
plt.show()
