SUBROUTINE UFIELD(FIELD,KFIELD,NSECPT,KSTEP,KINC,TIME,NODE,&
      COORDS,TEMP,DTEMP,NFIELD)

      INCLUDE 'ABA_PARAM.INC'

      DIMENSION FIELD(NSECPT,NFIELD), TIME(2), COORDS(3),&
      TEMP(NSECPT), DTEMP(NSECPT)



      print*, 'In field'


      RETURN
      END

SUBROUTINE USDFLD(FIELD,STATEV,PNEWDT,DIRECT,T,CELENT, &
      TIME,DTIME,CMNAME,ORNAME,NFIELD,NSTATV,NOEL,NPT,LAYER, &
      KSPT,KSTEP,KINC,NDI,NSHR,COORD,JMAC,JMATYP,MATLAYO,LACCFLA)

      INCLUDE 'ABA_PARAM.INC'

      CHARACTER*80 CMNAME,ORNAME
      CHARACTER*3  FLGRAY(15)
      DIMENSION FIELD(NFIELD),STATEV(NSTATV),DIRECT(3,3), &
      T(3,3),TIME(2)
      DIMENSION ARRAY(15),JARRAY(15),JMAC(*),JMATYP(*),COORD(*)

      CALL GETVRM('UT',ARRAY,JARRAY,FLGRAY,JRCD,JMAC,JMATYP,MATLAYO, &
      LACCFLA)

      print*, 'Array', ARRAY
      print*, 'in'

      FIELD(1) = 1.0

      !user coding to define FIELD and, if necessary, STATEV and PNEWDT


      RETURN
      END


SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD, &
  RPL,DDSDDT,DRPLDE,DRPLDT, &
  STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,CMNAME, &
  NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT, &
  CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,JSTEP,KINC)

INCLUDE 'ABA_PARAM.INC'

  CHARACTER*80 CMNAME

  DIMENSION STRESS(NTENS),STATEV(NSTATV), &
  DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS), &
  STRAN(NTENS),DSTRAN(NTENS),TIME(2),PREDEF(1),DPRED(1), &
  PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3), &
  JSTEP(4)

  double precision :: sigma(3,3), a0(3), eps, k1, k2, det, G, D, mus(1), alphas(1)
  double precision :: mu
  real :: x, y, z
  integer :: n
!		     ELASTIC USER SUBROUTINE

  logical :: exist
  !CALL GETVRM('UT',ARRAY,JARRAY,FLGRAY,JRCD,JMAC,JMATYP,MATLAYO, &
  !LACCFLA)

  !print*, 'Array', ARRAY
  ! print*, 'KINC ', KINC
  x = COORDS(1)
  y = COORDS(2)
  z = COORDS(3)
  if ((STATEV(4) < 1.0))then
    STATEV = 0

    ! print*, 'NPT ', NPT
    ! print*, 'COORDS ', COORDS

    !STATEV(1:2) = COORDS(1:2) *1.0
    !STATEV(1:3) = [x, 0.0*y, 0.0]
    STATEV(1:3) = [1.0, 0.0, 0.0]
    STATEV(1:3) = ABS(STATEV(1:3))
    ! print*, 'STATEV(1:3) ', STATEV(1:3)
    if(norm2(STATEV(1:3)) > 1e-6) then
      STATEV(1:3) = STATEV(1:3) / norm2(STATEV(1:3))
    else
      STATEV(1:3) = [1.0, 0.0, 0.0]
    end if
    ! print*, 'STATEV(1:3) ', STATEV(1:3)
    STATEV(4) = 2.0
  end if

  sigma = 0


  n = 1
  ! alphas = [2.7971, -2.7188, 10.505, 0.33382]
  ! mus = [0.77817, -0.011229, 1.268e-7, 16.169]
  alphas = [2.0]
  mus = [2.0*80.0e3]
  ! mus = 0
  mu = sum(mus)

  k1 = 1.56e3
  k2 = 0.4465
  ! k1 = 0
  ! k2 = 0.0
  ! a0 = [5, 2, 2]
  a0 = STATEV(1:3)
  !a0 = a0 /(norm2(a0))

  D = 2.0/(mu * 1e2)

  call hgo_sigma(DFGRD1, mus, alphas, n, k1, k2, a0, D, sigma)
  call hgo_apprx_C(DFGRD1, mus, alphas, n,  k1, k2, a0, D, DDSDDE)
  call matrix_to_voignt(sigma, STRESS)

  print*, '****************************'

  print*, 'NPT ', NPT
  print*, 'KSPT ', KSPT
  print*, 'JSTEP ', JSTEP
  print*, 'KINC ', KINC
  print*, 'STATEV ', STATEV
  print*, 'a0 ', a0
  print*, 'FIELD ', FIELD
  print*, 'TIME ', TIME
  print*, 'sigma'
  call print_matrix(sigma, 3, 3)
  print*,

  print*, 'F'
  call print_matrix(DFGRD1, 3, 3)
  print*,

  print*, 'J', det(DFGRD1)
  print*,
  print*, '****************************'

  if (NPT .eq. 1)then
    !inquire(file="stress.dat", exist=exist)

    if (KINC.eq.1)then
          !print*, '************** Yes!! 1 **************'
      open(1, file="/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/aba_test_hgo/stress.dat", FORM="FORMATTED",STATUS="REPLACE", ACTION="WRITE")
      open(2, file="/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/aba_test_hgo/stretch.dat", FORM="FORMATTED",STATUS="REPLACE", ACTION="WRITE")
    else
      ! print*, '************** Yes!! 2 **************'

      open(1, file="/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/aba_test_hgo/stress.dat", form='FORMATTED', status="old", position="append", action="write")
      open(2, file="/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/aba_test_hgo/stretch.dat", form='FORMATTED', status="old", position="append", action="write")
    end if
  end if


  if (NPT.eq.1)then
    ! print*, '************** Yes!! 3 **************'

    write(unit=1, fmt=*) sigma(1, 1)
    write(unit=2, fmt=*) DFGRD1(1, 1)
    close(1)
    close(2)
  end if
  RETURN
  END

  !********************************************
  !************** All together ****************
  !********************************************

  subroutine hgo_sigma(F, mus, alphas, n_order, k1, k2, a0, D, sigma)
    ! All working hundreds
    double precision :: F(3, 3), mus(n_order), alphas(n_order), k1, k2, a0(3), D
    integer :: n_order
    double precision :: sigma(3,3)

    sigma = 0
    call iso_ogden_sigma(F, mus, alphas, n_order, sigma)
    call aniso_bar_sigma(F, k1, k2, a0, sigma)
    call vol_sigma(F, D, sigma)
  end

  subroutine hgo_apprx_C(F, mus, alphas, n,  k1, k2, a0, D, C)
    double precision :: F(3,3), C(6,6), mus(n), alphas(n), eps,  k1, k2, a0(3), D
    double precision :: F_hat(3,3), k_stress(6), k_stress_hat(6), k_stress_mat(3,3), jacobian
    double precision :: Id(3,3), det, es(3, 3, 1)
    integer :: is(6), js(6), i, i_mat, j_mat, n

    is = [1, 2, 3, 2, 1, 1]
    js = [1, 2, 3, 3, 3, 2]
    call identity(Id, 3)
    jacobian = det(F)

    eps = 1.0e-9

    es = 0
    es(1, 1, 1) = 1
    es(2, 2, 1) = 1
    es(3, 3, 1) = 1

    k_stress_mat = 0
    ! call iso_ogden_sigma(F, mus, alphas, n, k_stress_mat)
    call hgo_sigma(F, mus, alphas, n, k1, k2, a0, D, k_stress_mat)
    k_stress_mat = k_stress_mat * jacobian
    call matrix_to_voignt(k_stress_mat, k_stress)

    do i=1,6
      i_mat = is(i)
      j_mat = js(i)


      F_hat = F + (eps/2.0)*(matmul(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), F) &
              + matmul(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), F))

      ! call kirchhoff_stress(F_hat, G, D, k1, k2, n0, k_stress_mat)
      ! call iso_ogden_sigma(F_hat, mus, alphas, n, k_stress_mat)
      k_stress_mat = 0
      call hgo_sigma(F_hat, mus, alphas, n, k1, k2, a0, D, k_stress_mat)
      k_stress_mat = k_stress_mat * det(F_hat)
      call matrix_to_voignt(k_stress_mat, k_stress_hat)

      C(:, i) = (1.0/(jacobian * eps)) * (k_stress_hat - k_stress)
    enddo
  end

  subroutine hgo_C(F, mus, alphas, n_order, k1, k2, a0, D, C)
    double precision :: F(3, 3), mus(n_order), alphas(n_order), k1, k2, a0(3), D
    double precision :: C_og(6,6), C_an(6,6), C_vol(6,6), eps
    integer :: n_order
    double precision :: C(6,6)

    C = 0
    C_og = 0
    C_an = 0
    C_vol = 0

    eps = 1.0e-9

    call apprx_ogden_C(F, mus, alphas, n_order, C_og, eps)
    call aniso_C_bar(F, C_an, k1, k2, a0)
    call vol_C(F, D, C_vol)

    C = C_og + C_an + C_vol
  end

  !********************************************
  !************** Ogden ***********************
  !********************************************

  subroutine iso_ogden_sigma(F, mus, alphas, n_order, sigma)
    !*****************************************************
    !******************* Routine initiations **************
    !*****************************************************
    ! inputs
     double precision :: F(3,3), mus(n_order), alphas(n_order)
     integer :: n_order
     ! used inside
     double precision :: jacobian, trace_comp
     double precision :: lambda_bars(3), n_s(3, 3), WORK(16)
     integer :: i, j, p, INFO
     ! output
     double precision :: sigma(3,3)
     !function
     double precision :: det
     !*****************************************************
     !************** Actual code **************************
     !*****************************************************

     jacobian = det(F)
     n_s = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))
     call dsyev('V', 'U', 3, n_s, 3, lambda_bars, WORK, 16, INFO)

     do i=1,3
       lambda_bars(i) = lambda_bars(i) ** 0.5
     enddo

     do p =1,n_order
       trace_comp = 0
       do j = 1,3
         trace_comp = trace_comp + lambda_bars(j)**alphas(p)
       enddo
       trace_comp = - trace_comp * (1.0/3.0)
       do i=1,3
         sigma = sigma + (1.0/jacobian) * mus(p) * (lambda_bars(i)**alphas(p) + trace_comp) &
         * spread(n_s(:, i),dim=2,ncopies=3)*spread(n_s(:, i),dim=1,ncopies=3)
       enddo
     enddo
  end

  subroutine apprx_ogden_C(F, mus, alphas, n, C, eps)
    double precision :: F(3,3), C(6,6), mus(n), alphas(n), eps
    double precision :: F_hat(3,3), k_stress(6), k_stress_hat(6), k_stress_mat(3,3), jacobian
    double precision :: Id(3,3), det, es(3, 3, 1)
    integer :: is(6), js(6), i, i_mat, j_mat, n

    is = [1, 2, 3, 2, 1, 1]
    js = [1, 2, 3, 3, 3, 2]
    call identity(Id, 3)
    jacobian = det(F)

    es = 0
    es(1, 1, 1) = 1
    es(2, 2, 1) = 1
    es(3, 3, 1) = 1


    k_stress_mat = 0
    call iso_ogden_sigma(F, mus, alphas, n, k_stress_mat)
    k_stress_mat = k_stress_mat * jacobian
    ! print*, 'F'
    ! call print_matrix(F, 3, 3)
    ! print*,
    ! print*, 'J ', jacobian
    ! print*,
    ! print*, 'mus '
    ! call print_vector(mus, n)
    ! print*,
    ! print*, 'alphas '
    ! call print_vector(alphas, n)
    ! print*,
    !
    ! print*, 'k_stress'
    ! call print_matrix(k_stress_mat, 3, 3)
    ! print*,
    call matrix_to_voignt(k_stress_mat, k_stress)

    do i=1,6
      i_mat = is(i)
      j_mat = js(i)

      F_hat = F + (eps/2.0)*(matmul(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), F) &
              + matmul(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), F))

      k_stress_mat = 0
      call iso_ogden_sigma(F_hat, mus, alphas, n, k_stress_mat)
      k_stress_mat = k_stress_mat * det(F_hat)

      call matrix_to_voignt(k_stress_mat, k_stress_hat)

      C(:, i) = (1.0/(jacobian * eps)) * (k_stress_hat - k_stress)
    enddo
  end

  !********************************************
  !************** Aniso ***********************
  !********************************************

  subroutine aniso_bar_sigma(F, k1, k2, a0, sigma)
     double precision :: F(3,3), k1, k2, a0(3)
     double precision :: C_bar(3,3), jacobian, I4, psi4, Id(3,3)
     double precision :: sigma(3,3)
     double precision :: det

     call identity(Id, 3)

     jacobian = det(F)
     C_bar = matmul(transpose(F), F) * (jacobian ** (-2.0/3.0))
     I4 = dot_product(a0, matmul(C_bar, a0))
     psi4 =  (I4-1) * k1 * exp(k2 * (I4-1)**2)
     sigma = sigma + (2.0/jacobian) * psi4 &
     * (matmul(F, matmul(spread(a0,dim=2,ncopies=3)*spread(a0,dim=1,ncopies=3), transpose(F))) &
     * jacobian ** (-2.0/3.0) - (1.0/3.0) * I4 * Id)
  end

  subroutine aniso_C_bar(F, C, k1, k2, a0)
    double precision :: F(3, 3), a0(3), k1, k2
    integer :: i, j, k, l
    integer :: ci, cj
    integer :: is(6), js(6)
    double precision :: jacobian
    double precision :: det, tr, aniso_C_bar_ijkl
    double precision :: C(6,6)

    is = [1, 2, 3, 2, 1, 1]
    js = [1, 2, 3, 3, 3, 2]

    jacobian = det(F)

    C = 0

    do ci = 1,6
      do cj=1,6
        i = is(ci)
        j = js(ci)
        k = is(cj)
        l = js(cj)


        C(ci, cj) = aniso_C_bar_ijkl(F, jacobian, k1, k2, a0, i, j, k, l)
        ! print*, aniso_C_ijkl(F, jacobian, k1, k2, a0, i, j, k, l)
      enddo
    enddo
  end

  function aniso_C_bar_ijkl(F, jacobian, k1, k2, a0, i, j, k, l) result(c)
     double precision :: F(3,3), jacobian, G, D, k1, k2, a0(3)
     integer :: i, j, k, l
     integer :: delta
     double precision :: tr
     double precision :: c, a(3), C_bar(3,3), I4

     a = matmul(F,a0) * jacobian **(-1.0/3.0)
     C_bar = matmul(transpose(F),F) * jacobian **(-1.0/3.0)
     I4 = dot_product(a0,matmul(C_bar,a0))

     c = (2.0*k1/jacobian)*exp(k2*(I4-1)**2) * (2.0*(1+2.0*k2*(I4-1)**2) &
     *(a(i)*a(j)-(1.0/3.0)*I4*delta(i,j))*(a(k)*a(l)-(1.0/3.0)*I4*delta(k,l))&
     +(I4-1)*(delta(i,k)*a(j)*a(l)+delta(j,k)*a(i)*a(l) - (2.0/3.0)*(delta(k,l)*a(i)*a(j)+delta(i,j)*a(l)*a(k)) &
     ! +(I4-1)*(0.5*(delta(i,k)*a(j)*a(l)+delta(j,l)*a(i)*a(k)) - (2.0/3.0)*(delta(k,l)*a(i)*a(j)+delta(i,j)*a(l)*a(k)) &
     + (2.0/9.0)*(I4*delta(i,j)*delta(k,l))))
  end function aniso_C_bar_ijkl

  !********************************************
  !************** Vol ***********************
  !********************************************

  subroutine vol_sigma(F, D, sigma)
     double precision :: F(3,3), D
     double precision :: jacobian, Id(3,3)
     double precision :: sigma(3,3)
     double precision :: det

     call identity(Id, 3)
     jacobian = det(F)

     sigma = sigma + (2.0/D)*(jacobian - 1.0) *Id
  end

  subroutine vol_C(F, D, C)
    double precision :: F(3, 3), D
    integer :: i, j, k, l
    integer :: ci, cj
    integer :: is(6), js(6)
    double precision :: jacobian
    double precision :: det, tr, vol_C_ijkl
    double precision :: C(6,6)

    is = [1, 2, 3, 2, 1, 1]
    js = [1, 2, 3, 3, 3, 2]

    jacobian = det(F)

    C = 0

    do ci = 1,6
      do cj=1,6
        i = is(ci)
        j = js(ci)
        k = is(cj)
        l = js(cj)

        C(ci, cj) = vol_C_ijkl(jacobian, D, i, j, k, l)
      enddo
    enddo
  end

  function vol_C_ijkl(jacobian, D, i, j, k, l) result(c)
     double precision :: jacobian, D
     integer :: i, j, k, l
     integer :: delta
     double precision :: c

     c = (2.0/D) * (2*jacobian - 1) * delta(i,j) * delta(k,l)
  end function vol_C_ijkl


  subroutine voignt_to_matrix(in, out)
    double precision :: in(6), out(3, 3)

    out(1,1) = in(1)
    out(2,2) = in(2)
    out(3,3) = in(3)

    out(2,3) = in(4)
    out(1,3) = in(5)
    out(1,2) = in(6)

    out(3,2) = in(4)
    out(3,1) = in(5)
    out(2,1) = in(6)

  end


  subroutine  tensor_product_2(mat_A, mat_B, len_a, len_b, out)
    integer :: len_a, len_b
    real:: mat_A(len_a, 1), mat_B(len_b, 1)
    real, dimension(len_a,len_b) :: out
    integer :: i,j

    do i = 1,len_a
      do j = 1,len_b
          out(i,j) = mat_A(i,1) * mat_B(j,1)
      enddo
    enddo
  end

  subroutine  tensor_product_1(mat_A, mat_B, len_a, len_b, out)
    integer :: len_a, len_b
    double precision:: mat_A(len_a), mat_B(len_b)
    double precision, dimension(len_a,len_b) :: out
    integer :: i,j

    do i = 1,len_a
      do j = 1,len_b
          out(i,j) = mat_A(i) * mat_B(j)
      enddo
    enddo
  end


  function delta(i, j) result(d)
     integer, intent(in) :: i! input
     integer :: d
     ! integer             :: j ! output
     if (i==j)then
       d = 1
     else
       d = 0
     endif
  end function delta

  function det(mat) result(d)
    double precision, intent(in) :: mat(3,3)
    double precision :: d

    d = mat(1,1) * (mat(2,2)*mat(3,3) - mat(2, 3) * mat(3,2))
    d = d - mat(1,2) * (mat(2,1)*mat(3,3) - mat(2, 3) * mat(3,1))
    d = d + mat(1,3) * (mat(2,1)*mat(3,2) - mat(2, 2) * mat(3,1))
  end function det

  function tr(mat) result(d)
    double precision, intent(in) :: mat(3,3)
    double precision :: d

    d = mat(1,1) + mat(2,2) + mat(3,3)
  end function tr

  function heaviside(x) result(d)
    double precision, intent(in) :: x
    double precision :: d, a

    a= 1.0
    d = 0.5*(sign(a,x)+1.0)
  end function heaviside

  subroutine identity(in_mat, n)
    integer :: n, i
    double precision :: in_mat(n,n)
    in_mat = 0
    do i = 1,n
        in_mat(i,i) = 1.0
    enddo
  end

  subroutine matrix_to_voignt(in, out)
    double precision :: in(3,3), out(6)
    out(1) = in(1,1)
    out(2) = in(2,2)
    out(3) = in(3,3)

    out(4) = in(2,3)
    out(5) = in(1,3)
    out(6) = in(1,2)
  end

  subroutine print_matrix(mat, rows, collumns)
    integer :: rows, collumns
    double precision :: mat(rows, collumns)
    integer :: i, j

    do i =1,rows
        print*,mat(i,:)
    enddo
  end

  subroutine print_vector(mat, rows)
    integer :: rows
    double precision :: mat(rows)
    integer :: i

    do i =1,rows
        print*,mat(i)
    enddo
  end
