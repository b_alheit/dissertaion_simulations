import material_model as mm
import numpy as np

F = np.eye(3, 3)
a0 = np.array([1, 0, 0])
k1 = 1000
k2 = 2
k3 = 1000
k4 = 2 * 8
K = 0
F[0,1] = 0.2
mus = np.array([0])
alphas = np.array([0])
sig = mm.sigma_bar_aniso(F, a0, k1, k2, k3, k4)
print('sig')
print(sig)
