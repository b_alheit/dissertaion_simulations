from abaqus import *
from abaqusConstants import *
import __main__

def make_ArcByCenterEnds(sketch, center, point1, point2, direction):
    # sketch.ArcByCenterEnds(center=center, point1=point1, point2=point2, direction=direction)
    # return arc, center, point1, point2
    # return sketch.geometry[len(sketch.geometry)-1], sketch.vertices[len(sketch.vertices)-1], sketch.vertices[len(sketch.vertices)-3], sketch.vertices[len(sketch.vertices)-2]
    return sketch.ArcByCenterEnds(center=center, point1=point1, point2=point2, direction=direction), sketch.vertices[len(sketch.vertices)-1], sketch.vertices[len(sketch.vertices)-3], sketch.vertices[len(sketch.vertices)-2]

def make_Line(sketch, point1, point2, message=""):
        line = sketch.Line(point1=point1, point2=point2)
        # return line, point1, point2
        if message != "":
            print(message, "\tv1 index returned: ", len(sketch.vertices)-2, "\tv2 index returned: ", len(sketch.vertices)-1)
        # return sketch.geometry[len(sketch.geometry)-1], sketch.vertices[len(sketch.vertices)-2], sketch.vertices[len(sketch.vertices)-1]
        return line, sketch.vertices[len(sketch.vertices)-2], sketch.vertices[len(sketch.vertices)-1]

def make_pause(message):
    a = getInput("Press enter to continue.\n message: " +message)

def break_point(bool_debug, message=""):
    if bool_debug:
        make_pause(message)
