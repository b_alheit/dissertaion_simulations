# -*- coding: mbcs -*-
# Do not delete the following import lines
from abaqus import *
from abaqusConstants import *
import __main__
from helpful_functions import *
import numpy as np
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior

def make_bone():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=30.0)
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=STANDALONE)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=24.1341,
        farPlane=32.4344, width=31.1622, height=15.7475, cameraPosition=(
        0.75823, -1.01516, 28.2843), cameraTarget=(0.75823, -1.01516, 0))
    s1.CircleByCenterPerimeter(center=(0.0, -7.0), point1=(0.0, 0.0))
    s1.CircleByCenterPerimeter(center=(0.0, -7.0), point1=(0.0, 2.0))
    s1.ConstructionLine(point1=(0.0, 4.0), point2=(0.0, -1.375))
    s1.VerticalConstraint(entity=g[4], addUndoState=False)
    s1.FixedConstraint(entity=g[2])
    s1.FixedConstraint(entity=g[3])
    s1.FixedConstraint(entity=g[4])
    s1.Line(point1=(-9.5, 3.125), point2=(-8.125, 1.25))
    s1.DistanceDimension(entity1=v[3], entity2=g[4], textPoint=(-3.12233829498291,
        5.54019975662231), value=9.5)
    s1.CoincidentConstraint(entity1=v[4], entity2=g[2])
    s1.CoincidentConstraint(entity1=v[3], entity2=g[3])
    s1.CoincidentConstraint(entity1=v[3], entity2=g[3])
    s1.delete(objectList=(d[0], ))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=25.0276,
        farPlane=31.541, width=24.4535, height=12.3573, cameraPosition=(
        -0.985246, -0.632308, 28.2843), cameraTarget=(-0.985246, -0.632308, 0))
    s1.dragEntity(entity=v[3], points=((-6.5788270833035, -0.858417613677044), (
        -6.625, -0.875), (-4.875, 0.0), (-4.5, 0.25), (-5.75, -0.125), (-6.625,
        -1.0), (-6.625, -1.0), (-6.25, -0.75), (-6.5, -1.0), (-6.5, -1.125), (
        -6.375, -1.0), (-5.375, -0.625), (-4.875, -0.625), (-5.375, -0.625), (
        -6.625, -1.0), (-7.0, -1.25)))
    s1.PerpendicularConstraint(entity1=g[5], entity2=g[2])
    s1.PerpendicularConstraint(entity1=g[5], entity2=g[3])
    s1.dragEntity(entity=g[5], points=((-6.04696101744262, -1.9410523068423), (
        -6.125, -2.0), (-5.125, -1.375), (-5.25, -1.5)))
    s1.DistanceDimension(entity1=v[3], entity2=g[4], textPoint=(-3.29217624664307,
        4.0956449508667), value=5.0)
    s1.dragEntity(entity=g[5], points=((-7.35678433734457, -3.74037521940317), (
        -7.25, -3.625), (-7.0, -3.0), (-6.0, -2.5), (-4.25, -0.875), (-3.75,
        -0.125)))
    s1.dragEntity(entity=g[5], points=((-4.42213506057031, -0.38155427412203), (
        -4.375, -0.375), (-3.5, 0.125)))
    s1.FixedConstraint(entity=g[5])
    s1.Line(point1=(-2.0, -1.625), point2=(-0.625, -1.625))
    s1.HorizontalConstraint(entity=g[6], addUndoState=False)
    s1.ObliqueDimension(vertex1=v[5], vertex2=v[6], textPoint=(-1.26207637786865,
        -3.21013998985291), value=1.0)
    s1.CoincidentConstraint(entity1=v[6], entity2=g[4])
    s1.CoincidentConstraint(entity1=v[5], entity2=g[2])
    s1.Line(point1=(-3.0, 3.0), point2=(-1.375, 3.0))
    s1.HorizontalConstraint(entity=g[7], addUndoState=False)
    s1.ObliqueDimension(vertex1=v[7], vertex2=v[8], textPoint=(-2.25405693054199,
        4.65051460266113), value=1.0)
    s1.CoincidentConstraint(entity1=v[8], entity2=g[4])
    s1.CoincidentConstraint(entity1=v[7], entity2=g[3])
    session.viewports['Viewport: 1'].view.setValues(nearPlane=27.3536,
        farPlane=29.2149, width=6.71136, height=3.39151, cameraPosition=(
        0.798775, 0.523861, 28.2843), cameraTarget=(0.798775, 0.523861, 0))
    s1.Line(point1=(0.0, -0.0717967697244917), point2=(0.0, 1.94427190999916))
    s1.VerticalConstraint(entity=g[8], addUndoState=False)
    s1.PerpendicularConstraint(entity1=g[6], entity2=g[8], addUndoState=False)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=21.2314,
        farPlane=35.3372, width=52.9578, height=26.7617, cameraPosition=(
        9.15494, -1.48083, 28.2843), cameraTarget=(9.15494, -1.48083, 0))
    s1.autoTrimCurve(curve1=g[2], point1=(-5.98299884796143, -3.8090386390686))
    s1.autoTrimCurve(curve1=g[10], point1=(6.25724792480469, -3.35841703414917))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=25.8175,
        farPlane=30.751, width=17.7885, height=8.98922, cameraPosition=(
        3.19903, -0.434426, 28.2843), cameraTarget=(3.19903, -0.434426, 0))
    s1.autoTrimCurve(curve1=g[9], point1=(-0.375451326370239, 0.0785273611545563))
    s1.autoTrimCurve(curve1=g[3], point1=(3.01442933082581, 1.40716063976288))
    s1.autoTrimCurve(curve1=g[12], point1=(-5.49383449554443, 0.0953458845615387))
    s1.autoTrimCurve(curve1=g[13], point1=(-0.174072027206421, 2.11352300643921))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=25.1408,
        farPlane=31.4278, width=23.6034, height=11.9277, cameraPosition=(
        4.29362, 0.605402, 28.2843), cameraTarget=(4.29362, 0.605402, 0))
    s1.delete(objectList=(d[1], ))
    p = mdb.models['Model-1'].Part(name='bone', dimensionality=THREE_D,
        type=DEFORMABLE_BODY)
    p = mdb.models['Model-1'].parts['bone']
    p.BaseSolidExtrude(sketch=s1, depth=3.0)
    s1.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts['bone']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    del mdb.models['Model-1'].sketches['__profile__']

def A_main():
    flat_width = 3
    depth = 3
    t_h = 2
    t_w = 1
    c_thick = 0.2
    c_trans = 0

    c_membran_thickness = 0.1
    k_membran_thickness = 0.1

    E_bone = 210
    v_bone = 0.25
    E_keratin = 100
    v_keratin = 0.4

    maxInc = 0.05
    maxNumInc = 1000

    dy = -0.15

    # Mesh Values
    coll_el_size = 0.3
    coll_deviation_factor = 0.1
    coll_min_size_factor = 0.1

    ker_el_size = 0.3
    ker_deviation_factor = 0.1
    ker_min_size_factor = 0.1

    mer_el_size = 0.22
    mer_deviation_factor = 0.1
    mer_min_size_factor = 0.1

    # Part Names
    bone_name = "bone"
    collagen_block = "coll-block"
    collagen_membrane = "coll-membrane"
    collagen_internal = "coll-int"
    keratin_shell = "keratin-shell"
    merged_part_name = "merged-shell"

    # Material Names
    bone_material_name = "bone"
    keratin_material_name = "keratin"
    collagen_material_name = "collagen"

    # Section Names
    bone_section_name = bone_material_name
    collagen_int_section_name = "collagen-int"
    collagen_membrane_name = "collagen-membrane"
    keratin_section_name = "keratin-shell"

    # Region Names
    bone_region_name = "bones"
    collagen_region_name = 'collagen-int'
    collagen_membrane_region = 'collagen-membrane'
    keratin_region_name = "keratin-shell"

    # Instance Names
    bone_instance_name = "bone-1"
    merged_instance_name = merged_part_name +"-1"
    collagen_membrane_instance = collagen_membrane + "-1"
    keratin_shell_instance = keratin_shell + "-1"

    # Step Names
    fibre_step_name = "assign_fibre_direction"
    load_step_name = "load"

    # Surface Names
    merged_top = 'merged-top'
    coll_membrane_bottom = 'coll-membrane-bottom'
    keratin_shell_bottom = 'keratin-shell-bottom'

    # Tie Names
    coll_membrane_to_merge = "coll-membrane-to-merge"
    ker_shell_to_merge = "ker-shell-to-merge"

    # Set Names
    all = "all"
    bottom_left_edge = "bottom-left-edge"
    bottom_right_edge = "bottom-right-edge"
    keratin_top = "keratin-top"

    # Load Names
    lock_all = "lock-all"
    left_pin = "left-pin"
    right_slider = "right-slider"
    bottom_back_points = "bottom-back-points"
    z_control = "z-control"
    top_push = "top-push"

    radius_to_flat_length_scale = 30
    total_length_to_flat_length = 3
    flat_length_teeth_length = 1.2
    a = 2
    n_points = 100

    t_h /= 2.
    x_path = np.linspace(0, depth*1.05, n_points)
    x_path -= (x_path[-1] - depth)/2.
    y_path = t_h/2. * np.sin(2.*np.pi * x_path / t_w)

    x_cut = np.linspace(0, flat_width*1.05, n_points)
    x_cut -= (x_cut[-1] - flat_width)/2.
    y_cut = t_h/2. * np.sin(2.*np.pi * x_cut / t_w)

    theta = lambda x: np.pi/2. - np.pi/4. * np.cos(2.*np.pi * x / t_w)

    y_cut_top = y_cut + c_thick/2 * np.sin(theta(x_cut)) + c_trans/2.
    x_cut_top = x_cut - c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

    y_cut_bottom = y_cut - c_thick/2 * np.sin(theta(x_cut)) - c_trans/2.
    x_cut_bottom = x_cut + c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

    t_total_length = np.max(y_path) - np.min(y_path) + np.max(y_cut_top) - np.min(y_cut_bottom)
    flat_length = t_total_length*flat_length_teeth_length/2
    apprx_radius = flat_length * radius_to_flat_length_scale
    total_length = flat_length * total_length_to_flat_length

    apprx_size = total_length*2.5

    # path = np.array([x_path, y_path]).T
    path = tuple(map(tuple, np.array([x_path, y_path]).T))
    cut_top = tuple(map(tuple, np.array([x_cut_top, y_cut_top]).T))
    cut_bottom = tuple(map(tuple, np.array([x_cut_bottom, y_cut_bottom]).T))
    # print(path)

    delete_all()

    make_block(bone_name, flat_length, flat_width, apprx_radius, total_length, depth, apprx_size, b_points=False)
    make_block(collagen_block, flat_length, flat_width, apprx_radius, total_length, depth, apprx_size, b_points=False)
    make_block(collagen_membrane, flat_length, flat_width, apprx_radius, total_length, depth, apprx_size, b_points=False)
    cut_bone(bone_name, path, cut_top, cut_bottom, flat_width, depth, False)
    make_membrane(collagen_membrane)
    copy_membrane(collagen_membrane, keratin_shell)
    cut_collagen(collagen_internal, bone_instance_name, bone_name, collagen_block)
    merge_col_bone(merged_part_name, bone_instance_name, collagen_internal)

    make_LE_material(bone_material_name, E_bone, v_bone)
    make_LE_material(keratin_material_name, E_keratin, v_keratin)
    make_HGO_material(collagen_material_name)

    creating_sections(bone_section_name, bone_material_name,
        collagen_int_section_name, collagen_material_name,
        collagen_membrane_name, c_membran_thickness,
        keratin_section_name, keratin_material_name, k_membran_thickness)

    assign_sections(merged_part_name, bone_region_name, bone_section_name,
        collagen_region_name, collagen_int_section_name,
        collagen_membrane, collagen_membrane_region, collagen_membrane_name,
        keratin_shell, keratin_region_name, keratin_section_name)

    assemble(collagen_membrane, collagen_membrane_instance,
        keratin_shell, keratin_shell_instance)

    make_steps(fibre_step_name,
        load_step_name, maxInc, maxNumInc)

    interactions(coll_membrane_to_merge, merged_top, merged_instance_name,
        collagen_membrane_instance, coll_membrane_bottom,
        keratin_shell_instance, keratin_shell_bottom, ker_shell_to_merge)

    lock_all_load(all, fibre_step_name,
        merged_instance_name, collagen_membrane_instance, keratin_shell_instance,
        lock_all, load_step_name)
    bend_test_load(merged_instance_name, bottom_left_edge, left_pin, load_step_name,
        bottom_right_edge, right_slider,
        bottom_back_points, z_control,
        keratin_shell_instance, keratin_top, top_push, dy)

    mesh_coll_membrane(collagen_membrane, coll_el_size, coll_deviation_factor,
        coll_min_size_factor)
    mesh_keratin(keratin_shell, ker_el_size, ker_deviation_factor,
        ker_min_size_factor)
    mesh_merged(merged_part_name, mer_el_size, mer_deviation_factor,
        mer_min_size_factor)

    # create_job()

def make_block(part_name, flat_length, flat_width, apprx_radius, total_length, depth, apprx_size, b_points=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=apprx_size)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=STANDALONE)
    s.rectangle(point1=(0.0, 0.0), point2=(-flat_length, flat_width))
    s.FixedConstraint(entity=g[5])
    s.FixedConstraint(entity=g[4])
    s.FixedConstraint(entity=g[3])
    s.FixedConstraint(entity=g[2])
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=20.3084,
    #     farPlane=36.2602, width=59.8883, height=30.2639, cameraPosition=(
    #     10.3183, -5.24793, 28.2843), cameraTarget=(10.3183, -5.24793, 0))
    s.CircleByCenterPerimeter(center=(0.0, -apprx_radius), point1=(-flat_length, 0.0))
    s.CircleByCenterPerimeter(center=(0.0, -apprx_radius), point1=(-flat_length, flat_width))
    s.ConstructionLine(point1=(-total_length, 6.5), point2=(-total_length, 3.125))
    # s.VerticalConstraint(entity=g[8], addUndoState=False)
    s.FixedConstraint(entity=g[8])
    s.Line(point1=(-7.75, 3.375), point2=(-6.625, 2.0))
    s.CoincidentConstraint(entity1=v[6], entity2=g[6])
    s.CoincidentConstraint(entity1=v[5], entity2=g[7])
    s.CoincidentConstraint(entity1=v[5], entity2=g[8])
    s.PerpendicularConstraint(entity1=g[9], entity2=g[6])
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=24.1058,
    #     farPlane=32.4627, width=30.1321, height=15.2269, cameraPosition=(
    #     1.25103, -3.15898, 28.2843), cameraTarget=(1.25103, -3.15898, 0))
    s.PerpendicularConstraint(entity1=g[9], entity2=g[7])
    break_point(b_points, "1")
    s.autoTrimCurve(curve1=g[7], point1=(-(total_length+5), flat_width))
    break_point(b_points, "2")
    s.autoTrimCurve(curve1=g[6], point1=(-(total_length+5), -4.65462303161621))
    break_point(b_points, "3")
    # s.autoTrimCurve(curve1=g[12], point1=(-(total_length*0.99), -flat_width))
    s.autoTrimCurve(curve1=g[12], point1=(-(total_length*0.999), np.sqrt(apprx_radius**2-(total_length*0.99)**2)-apprx_radius))
    break_point(b_points, "4")
    s.autoTrimCurve(curve1=g[13], point1=(-0.0168264961242676, 0.0302357912063599))
    break_point(b_points, "5")
    s.autoTrimCurve(curve1=g[15], point1=(0.90483856201172, 0.302357912063599))
    break_point(b_points, "6")
    s.autoTrimCurve(curve1=g[11], point1=(0.01, flat_width*1.05))
    break_point(b_points, "7")
    s.autoTrimCurve(curve1=g[4], point1=(-flat_length, flat_width/2.))
    break_point(b_points, "8")
    p = mdb.models['Model-1'].Part(name=part_name, dimensionality=THREE_D,
        type=DEFORMABLE_BODY)
    p = mdb.models['Model-1'].parts[part_name]
    p.BaseSolidExtrude(sketch=s, depth=depth)
    s.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    del mdb.models['Model-1'].sketches['__profile__']
    p = mdb.models['Model-1'].parts[part_name]
    f = p.faces
    p.Mirror(mirrorPlane=f[4], keepOriginal=ON)

def cut_bone(part_name, path, cut_top, cut_bottom, flat_width, depth, b_points):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f[6], sketchUpEdge=e[21],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, flat_width, 0.0))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__', sheetSize=depth*3.,
        gridSpacing=depth/10., transform=t)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=9.34539,
    #     farPlane=14.0878, width=4.72059, height=2.3855, cameraPosition=(
    #     -0.283552, 12, 2.13615), cameraTarget=(-0.283552, 2, 2.13615))
    s.Spline(points=path)
    break_point(b_points, "1")
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=9.21486,
    #     farPlane=14.2183, width=5.89536, height=2.97916, cameraPosition=(
    #     -0.318505, 12, 1.86906), cameraTarget=(-0.318505, 2, 1.86906))
    s.unsetPrimaryObject()
    s.unsetPrimaryObject()
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=33.3508,
    #     farPlane=54.7613, width=21.1243, height=10.1162, cameraPosition=(
    #     -25.6409, 29.4637, -19.2963), cameraUpVector=(0.914255, -0.00632095,
    #     -0.40509), cameraTarget=(-0.664081, -0.017676, 1.44713))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=33.8794,
    #     farPlane=54.1226, width=21.4591, height=10.2766, cameraPosition=(
    #     -24.3944, 34.1074, -12.5509), cameraUpVector=(0.869373, -0.0265511,
    #     -0.493442), cameraTarget=(-0.658399, 0.00349182, 1.47788))
    p = mdb.models['Model-1'].parts[part_name]
    f1, e1 = p.faces, p.edges
    t = p.MakeSketchTransform(sketchPlane=f1[9], sketchUpEdge=e1[22],
        sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0, 0.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
        sheetSize=40.58, gridSpacing=1.01, transform=t)
    g1, v1, d1, c1 = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=39.2367,
    #     farPlane=44.9334, width=9.72343, height=4.91363, cameraPosition=(
    #     -0.583853, 0.115076, -40.5851), cameraTarget=(-0.583853, 0.115076, 0))
    top_start = cut_top[0]
    top_end = cut_top[-1]
    bottom_start = cut_bottom[0]
    bottom_end = cut_bottom[-1]

    break_point(b_points, "2")
    s1.Spline(points=cut_top)
    break_point(b_points, "3")
    s1.Spline(points=cut_bottom)
    break_point(b_points, "4")
    s1.Line(point1=top_start, point2=bottom_start)
    break_point(b_points, "5")
    s1.Line(point1=top_end, point2=bottom_end)
    break_point(b_points, "6")
    # s1.VerticalConstraint(entity=g1[15], addUndoState=False)
    s1.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    p.CutSweep(pathPlane=f[6], pathUpEdge=e[21], sketchPlane=f[9],
        sketchUpEdge=e[22], pathOrientation=RIGHT, path=s,
        sketchOrientation=RIGHT, profile=s1, profileNormal=ON)
    del mdb.models['Model-1'].sketches['__sweep__']
    del mdb.models['Model-1'].sketches['__profile__']

def make_membrane(part_name):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts[part_name]
    c = p.cells
    p.RemoveCells(cellList = c[0:1])
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=14.2761,
    #     farPlane=26.9659, width=12.413, height=5.94447, cameraPosition=(
    #     -19.6601, -2.27603, -3.98845), cameraUpVector=(0.446304, 0.60402,
    #     -0.660282), cameraTarget=(0.300893, 0.425684, 1.44317))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=14.2549,
    #     farPlane=27.0944, width=12.3945, height=5.93563, cameraPosition=(
    #     -18.8663, -6.58538, -2.85417), cameraUpVector=(0.292716, 0.628072,
    #     -0.721001), cameraTarget=(0.291597, 0.476148, 1.42989))
    p = mdb.models['Model-1'].parts[part_name]
    f = p.faces
    p.RemoveFaces(faceList = f[0:2]+f[3:5]+f[7:10], deleteCells=False)

def copy_membrane(part_name_1, part_name_2):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p1 = mdb.models['Model-1'].parts[part_name_1]
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    p = mdb.models['Model-1'].Part(name=part_name_2,
        objectToCopy=mdb.models['Model-1'].parts[part_name_1])
    session.viewports['Viewport: 1'].setValues(displayedObject=p)

def cut_collagen(part_name, bone_instance_name, bone_name, coll_block_name):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.suppressFeatures(featureNames=('internal-collagen-1', ))
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts[bone_name]
    a1.Instance(name=bone_instance_name, part=p, dependent=ON)
    p = mdb.models['Model-1'].parts[coll_block_name]
    a1.Instance(name='coll-block-1', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanCut(name='coll-int',
        instanceToBeCut=mdb.models['Model-1'].rootAssembly.instances['coll-block-1'],
        cuttingInstances=(a1.instances[bone_instance_name], ),
        originalInstances=SUPPRESS)

def merge_col_bone(merged_part_name, bone_instance, coll_name):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=15.838,
    #     farPlane=23.3217, width=13.2756, height=6.35756, viewOffsetX=0.022118,
    #     viewOffsetY=0.00801414)
    coll_instance = coll_name +'-1'
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a = mdb.models['Model-1'].rootAssembly
    a.features[bone_instance].resume()
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name=merged_part_name, instances=(
        a1.instances[bone_instance], a1.instances[coll_instance], ),
        keepIntersections=ON, originalInstances=SUPPRESS, domain=GEOMETRY)

def make_LE_material(bone_mat_name, youngs_mod, poi):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    mdb.models['Model-1'].Material(name=bone_mat_name)
    mdb.models['Model-1'].materials[bone_mat_name].Elastic(table=((youngs_mod, poi), ))

def make_HGO_material(name):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    mdb.models['Model-1'].Material(name=name)
    mdb.models['Model-1'].materials[name].Depvar(n=3)
    mdb.models['Model-1'].materials[name].UserMaterial(mechanicalConstants=(
        0.0, ))

def creating_sections(bone_section_name, bone_mat,
    collagen_int_section_name, coll_mat,
    collagen_membrane_name, coll_t,
    keratin_section_name, keratin_mat, ker_t):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
    #     engineeringFeatures=ON)
    # p = mdb.models['Model-1'].parts['keratin-shell']
    # session.viewports['Viewport: 1'].setValues(displayedObject=p)
    # p = mdb.models['Model-1'].parts['merged-shell']
    # session.viewports['Viewport: 1'].setValues(displayedObject=p)
    mdb.models['Model-1'].HomogeneousSolidSection(name=bone_section_name, material=bone_mat,
        thickness=None)
    mdb.models['Model-1'].HomogeneousSolidSection(name=collagen_int_section_name,
        material=coll_mat, thickness=None)
    mdb.models['Model-1'].MembraneSection(name=collagen_membrane_name,
        material=coll_mat, thicknessType=UNIFORM, thickness=coll_t,
        thicknessField='', poissonDefinition=DEFAULT)
    mdb.models['Model-1'].HomogeneousShellSection(name=keratin_section_name,
        preIntegrate=OFF, material=keratin_mat, thicknessType=UNIFORM,
        thickness=ker_t, thicknessField='', idealization=NO_IDEALIZATION,
        poissonDefinition=DEFAULT, thicknessModulus=None, temperature=GRADIENT,
        useDensity=OFF, integrationRule=GAUSS, numIntPts=3)

def assign_sections(merged_part_name, bone_region_name, bone_section,
    collagen_region_name, collagen_int_section_name,
    collagen_membrane, collagen_membrane_region, collagen_membrane_name,
    keratin_shell, keratin_region_name, keratin_section_name):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    p = mdb.models['Model-1'].parts[merged_part_name]
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#3 ]', ), )
    region = p.Set(cells=cells, name=bone_region_name)
    p = mdb.models['Model-1'].parts[merged_part_name]
    p.SectionAssignment(region=region, sectionName=bone_section, offset=0.0,
        offsetType=MIDDLE_SURFACE, offsetField='',
        thicknessAssignment=FROM_SECTION)

    p = mdb.models['Model-1'].parts[merged_part_name]
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#4 ]', ), )
    region = p.Set(cells=cells, name=collagen_region_name)
    p = mdb.models['Model-1'].parts[merged_part_name]
    p.SectionAssignment(region=region, sectionName=collagen_int_section_name, offset=0.0,
        offsetType=MIDDLE_SURFACE, offsetField='',
        thicknessAssignment=FROM_SECTION)

    p = mdb.models['Model-1'].parts[collagen_membrane]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p = mdb.models['Model-1'].parts[collagen_membrane]
    f = p.faces
    faces = f.getSequenceFromMask(mask=('[#7 ]', ), )
    region = p.Set(faces=faces, name=collagen_membrane_region)
    p = mdb.models['Model-1'].parts[collagen_membrane]
    p.SectionAssignment(region=region, sectionName=collagen_membrane_name, offset=0.0,
        offsetType=MIDDLE_SURFACE, offsetField='',
        thicknessAssignment=FROM_SECTION)

    p = mdb.models['Model-1'].parts[keratin_shell]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p = mdb.models['Model-1'].parts[keratin_shell]
    f = p.faces
    faces = f.getSequenceFromMask(mask=('[#7 ]', ), )
    region = p.Set(faces=faces, name=keratin_region_name)
    p = mdb.models['Model-1'].parts[keratin_shell]
    p.SectionAssignment(region=region, sectionName=keratin_section_name, offset=0.0,
        offsetType=MIDDLE_SURFACE, offsetField='',
        thicknessAssignment=FROM_SECTION)

def assemble(collagen_membrane, collagen_membrane_instance,
    keratin_shell, keratin_shell_instance):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].view.setValues(nearPlane=13.684,
        farPlane=25.5201, width=10.6487, height=5.09957, viewOffsetX=0.927563,
        viewOffsetY=-0.329588)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts[collagen_membrane]
    a1.Instance(name=collagen_membrane_instance, part=p, dependent=ON)
    p = mdb.models['Model-1'].parts[keratin_shell]
    a1.Instance(name=keratin_shell_instance, part=p, dependent=ON)

def make_steps(fibre_step_name,
    load_step_name, maxInc, maxNumInc):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        adaptiveMeshConstraints=ON)
    mdb.models['Model-1'].StaticStep(name=fibre_step_name,
        previous='Initial', nlgeom=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        step=fibre_step_name)
    mdb.models['Model-1'].StaticStep(name=load_step_name,
        previous=fibre_step_name, maxNumInc=maxNumInc, initialInc=maxInc,
        maxInc=maxInc)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step=load_step_name)

def interactions(coll_membrane_to_merge, merged_top, merged_instance_name,
    collagen_membrane_instance, coll_membrane_bottom,
    keratin_shell_instance, keratin_shell_bottom, ker_shell_to_merge):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # session.viewports['Viewport: 1'].assemblyDisplay.hideInstances(instances=(
    #     'coll-membrane-1', 'keratin-shell-1', ))
    # session.viewports['Viewport: 1'].assemblyDisplay.hideInstances(instances=(
    #     'merged-shell-1', ))
    # session.viewports['Viewport: 1'].assemblyDisplay.showInstances(instances=(
    #     'coll-membrane-1', ))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=14.6544,
    #     farPlane=25.0773, width=11.5027, height=5.50856, cameraPosition=(
    #     10.6324, -2.61129, 17.7751), cameraUpVector=(-0.222484, 0.974301,
    #     -0.0351907), cameraTarget=(0.328122, 1.17818, 1.4447))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=14.8058,
    #     farPlane=24.4056, width=11.6215, height=5.56545, cameraPosition=(
    #     6.33746, 13.761, 15.4283), cameraUpVector=(-0.573711, 0.490621,
    #     -0.655856), cameraTarget=(0.287498, 1.33304, 1.4225))
    a = mdb.models['Model-1'].rootAssembly
    s1 = a.instances[merged_instance_name].faces
    side1Faces1 = s1.getSequenceFromMask(mask=('[#84244 ]', ), )
    region1=a.Surface(side1Faces=side1Faces1, name=merged_top)
    a = mdb.models['Model-1'].rootAssembly
    s1 = a.instances[collagen_membrane_instance].faces
    side2Faces1 = s1.getSequenceFromMask(mask=('[#7 ]', ), )
    region2=a.Surface(side2Faces=side2Faces1, name=coll_membrane_bottom)
    mdb.models['Model-1'].Tie(name=coll_membrane_to_merge, master=region1,
        slave=region2, positionToleranceMethod=COMPUTED, adjust=ON,
        tieRotations=ON, thickness=ON)

    # session.viewports['Viewport: 1'].assemblyDisplay.hideInstances(instances=(
    #     'coll-membrane-1', ))
    # session.viewports['Viewport: 1'].assemblyDisplay.showInstances(instances=(
    #     'keratin-shell-1', ))

    a = mdb.models['Model-1'].rootAssembly
    region1=a.surfaces[merged_top]
    a = mdb.models['Model-1'].rootAssembly
    s1 = a.instances[keratin_shell_instance].faces
    side2Faces1 = s1.getSequenceFromMask(mask=('[#7 ]', ), )
    region2=a.Surface(side2Faces=side2Faces1, name=keratin_shell_bottom)
    mdb.models['Model-1'].Tie(name=ker_shell_to_merge, master=region1,
        slave=region2, positionToleranceMethod=COMPUTED, adjust=ON,
        tieRotations=ON, thickness=ON)

def lock_all_load(all, fibre_step_name,
    merged_instance_name, collagen_membrane_instance, keratin_shell_instance,
    lock_all, load_step_name):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        step=fibre_step_name)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=14.1102,
        farPlane=25.1933, width=16.9284, height=8.10688, viewOffsetX=1.37389,
        viewOffsetY=-0.120434)
    a = mdb.models['Model-1'].rootAssembly
    c1 = a.instances[merged_instance_name].cells
    cells1 = c1.getSequenceFromMask(mask=('[#7 ]', ), )
    f1 = a.instances[merged_instance_name].faces
    faces1 = f1.getSequenceFromMask(mask=('[#fffff ]', ), )
    e1 = a.instances[merged_instance_name].edges
    edges1 = e1.getSequenceFromMask(mask=('[#ffffffff #fff ]', ), )
    v1 = a.instances[merged_instance_name].vertices
    verts1 = v1.getSequenceFromMask(mask=('[#fffffff ]', ), )

    f2 = a.instances[collagen_membrane_instance].faces
    faces2 = f2.getSequenceFromMask(mask=('[#7 ]', ), )
    e2 = a.instances[collagen_membrane_instance].edges
    edges2 = e2.getSequenceFromMask(mask=('[#fff ]', ), )
    v2 = a.instances[collagen_membrane_instance].vertices
    verts2 = v2.getSequenceFromMask(mask=('[#3ff ]', ), )

    f3 = a.instances[keratin_shell_instance].faces
    faces3 = f3.getSequenceFromMask(mask=('[#7 ]', ), )
    e3 = a.instances[keratin_shell_instance].edges
    edges3 = e3.getSequenceFromMask(mask=('[#fff ]', ), )
    v3 = a.instances[keratin_shell_instance].vertices
    verts3 = v3.getSequenceFromMask(mask=('[#3ff ]', ), )

    region = a.Set(vertices=verts1+verts2+verts3, edges=edges1+edges2+edges3,
        faces=faces1+faces2+faces3, cells=cells1, name=all)

    mdb.models['Model-1'].DisplacementBC(name=lock_all, createStepName='Initial',
        region=region, u1=SET, u2=SET, u3=SET, ur1=UNSET, ur2=UNSET, ur3=UNSET,
        amplitude=UNSET, distributionType=UNIFORM, fieldName='',
        localCsys=None)
    mdb.models['Model-1'].boundaryConditions[lock_all].deactivate(load_step_name)

def bend_test_load(merged_instance_name, bottom_left_edge, left_pin, load_step_name,
    bottom_right_edge, right_slider,
    bottom_back_points, z_control,
    keratin_shell_instance, keratin_top, top_push, dy):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(step=load_step_name)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=11.4209,
    #     farPlane=23.8011, width=13.702, height=6.56178, cameraPosition=(
    #     -11.952, -1.8971, 14.0632), cameraUpVector=(-0.18096, 0.887404,
    #     -0.42399), cameraTarget=(0.641923, 0.969537, -0.782508),
    #     viewOffsetX=1.11204, viewOffsetY=-0.0974805)

    a = mdb.models['Model-1'].rootAssembly
    e1 = a.instances[merged_instance_name].edges
    edges1 = e1.getSequenceFromMask(mask=('[#8000000 ]', ), )
    region = a.Set(edges=edges1, name=bottom_left_edge)
    mdb.models['Model-1'].DisplacementBC(name=left_pin, createStepName=load_step_name,
        region=region, u1=0.0, u2=0.0, u3=UNSET, ur1=UNSET, ur2=UNSET,
        ur3=UNSET, amplitude=UNSET, fixed=OFF, distributionType=UNIFORM,
        fieldName='', localCsys=None)

    a = mdb.models['Model-1'].rootAssembly
    e1 = a.instances[merged_instance_name].edges
    edges1 = e1.getSequenceFromMask(mask=('[#0 #400 ]', ), )
    region = a.Set(edges=edges1, name=bottom_right_edge)
    mdb.models['Model-1'].DisplacementBC(name=right_slider,
        createStepName=load_step_name, region=region, u1=UNSET, u2=0.0, u3=UNSET,
        ur1=UNSET, ur2=UNSET, ur3=UNSET, amplitude=UNSET, fixed=OFF,
        distributionType=UNIFORM, fieldName='', localCsys=None)

    a = mdb.models['Model-1'].rootAssembly
    v1 = a.instances[merged_instance_name].vertices
    verts1 = v1.getSequenceFromMask(mask=('[#480000 ]', ), )
    region = a.Set(vertices=verts1, name=bottom_back_points)
    mdb.models['Model-1'].DisplacementBC(name=z_control, createStepName=load_step_name,
        region=region, u1=UNSET, u2=UNSET, u3=0.0, ur1=UNSET, ur2=UNSET,
        ur3=UNSET, amplitude=UNSET, fixed=OFF, distributionType=UNIFORM,
        fieldName='', localCsys=None)

    # session.viewports['Viewport: 1'].view.setValues(nearPlane=12.9514,
    #     farPlane=22.5081, width=15.5382, height=7.44112, cameraPosition=(
    #     -4.41486, 11.3792, 15.5607), cameraUpVector=(-0.223768, 0.475806,
    #     -0.85061), cameraTarget=(-0.779719, -0.461318, 0.269863),
    #     viewOffsetX=1.26106, viewOffsetY=-0.110544)
    # session.viewports['Viewport: 1'].assemblyDisplay.hideInstances(instances=(
    #     'merged-shell-1', 'coll-membrane-1', ))

    a = mdb.models['Model-1'].rootAssembly
    f1 = a.instances[keratin_shell_instance].faces
    faces1 = f1.getSequenceFromMask(mask=('[#4 ]', ), )
    region = a.Set(faces=faces1, name=keratin_top)
    mdb.models['Model-1'].DisplacementBC(name=top_push, createStepName=load_step_name,
        region=region, u1=UNSET, u2=dy, u3=UNSET, ur1=UNSET, ur2=UNSET,
        ur3=UNSET, amplitude=UNSET, fixed=OFF, distributionType=UNIFORM,
        fieldName='', localCsys=None)

    # session.viewports['Viewport: 1'].assemblyDisplay.showInstances(instances=(
    #     'merged-shell-1', 'coll-membrane-1', ))

def mesh_coll_membrane(collagen_membrane, coll_el_size, coll_deviation_factor,
    coll_min_size_factor):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts[collagen_membrane]
    e = p.edges
    edges = e.getSequenceFromMask(mask=('[#28 ]', ), )
    v = p.vertices
    verts = v.getSequenceFromMask(mask=('[#69 ]', ), )
    pickedEntities =(verts, edges, )
    p.ignoreEntity(entities=pickedEntities)
    p = mdb.models['Model-1'].parts[collagen_membrane]
    f = p.faces
    pickedRegions = f.getSequenceFromMask(mask=('[#1 ]', ), )
    p.setMeshControls(regions=pickedRegions, elemShape=TRI)
    elemType1 = mesh.ElemType(elemCode=M3D8R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=M3D6, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts[collagen_membrane]
    f = p.faces
    faces = f.getSequenceFromMask(mask=('[#1 ]', ), )
    pickedRegions =(faces, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2))
    p = mdb.models['Model-1'].parts[collagen_membrane]
    p.seedPart(size=coll_el_size, deviationFactor=coll_deviation_factor, minSizeFactor=coll_min_size_factor)
    p = mdb.models['Model-1'].parts[collagen_membrane]
    p.generateMesh()

def mesh_keratin(keratin_shell, ker_el_size, ker_deviation_factor,
    ker_min_size_factor):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts[keratin_shell]
    f = p.faces
    pickedRegions = f.getSequenceFromMask(mask=('[#7 ]', ), )
    p.setMeshControls(regions=pickedRegions, elemShape=TRI)
    elemType1 = mesh.ElemType(elemCode=S8R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=STRI65, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['keratin-shell']
    f = p.faces
    faces = f.getSequenceFromMask(mask=('[#7 ]', ), )
    pickedRegions =(faces, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2))
    p = mdb.models['Model-1'].parts[keratin_shell]
    p.seedPart(size=ker_el_size, deviationFactor=ker_deviation_factor, minSizeFactor=ker_min_size_factor)
    p = mdb.models['Model-1'].parts[keratin_shell]
    p.generateMesh()

def mesh_merged(merged_part_name, mer_el_size, mer_deviation_factor,
    mer_min_size_factor):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts[merged_part_name]
    e = p.edges
    edges = e.getSequenceFromMask(mask=('[#0 #10 ]', ), )
    v = p.vertices
    verts = v.getSequenceFromMask(mask=('[#1100000 ]', ), )
    pickedEntities =(verts, edges, )
    p.ignoreEntity(entities=pickedEntities)
    p = mdb.models['Model-1'].parts[merged_part_name]
    e = p.edges
    edges = e.getSequenceFromMask(mask=('[#800000 ]', ), )
    v = p.vertices
    verts = v.getSequenceFromMask(mask=('[#21000 ]', ), )
    pickedEntities =(verts, edges, )
    p.ignoreEntity(entities=pickedEntities)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=14.7707,
        farPlane=24.7603, width=11.5503, height=5.55229, cameraPosition=(
        4.12666, -11.7556, 16.308), cameraUpVector=(0.0437957, 0.941862,
        0.333134), cameraTarget=(0.161892, 0.6195, 1.53108))
    p = mdb.models['Model-1'].parts[merged_part_name]
    e = p.edges
    edges = e.getSequenceFromMask(mask=('[#80000 ]', ), )
    v = p.vertices
    verts = v.getSequenceFromMask(mask=('[#c000 ]', ), )
    pickedEntities =(verts, edges, )
    p.ignoreEntity(entities=pickedEntities)
    p = mdb.models['Model-1'].parts[merged_part_name]
    e = p.edges
    edges = e.getSequenceFromMask(mask=('[#4000000 ]', ), )
    v = p.vertices
    verts = v.getSequenceFromMask(mask=('[#c0000 ]', ), )
    pickedEntities =(verts, edges, )
    p.ignoreEntity(entities=pickedEntities)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=14.447,
        farPlane=25.1478, width=11.2972, height=5.43063, cameraPosition=(
        8.31921, 8.26293, 17.7828), cameraUpVector=(-0.26404, 0.741799,
        -0.616455), cameraTarget=(0.180469, 0.708199, 1.53762))
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    pickedRegions = c.getSequenceFromMask(mask=('[#7 ]', ), )
    p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    elemType1 = mesh.ElemType(elemCode=C3D20R)
    elemType2 = mesh.ElemType(elemCode=C3D15)
    elemType3 = mesh.ElemType(elemCode=C3D10)
    p = mdb.models['Model-1'].parts[merged_part_name]
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#7 ]', ), )
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
        elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts[merged_part_name]
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#6 ]', ), )
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
        elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10H, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts[merged_part_name]
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#1 ]', ), )
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
        elemType3))
    p = mdb.models['Model-1'].parts[merged_part_name]
    p.seedPart(size=mer_el_size, deviationFactor=mer_deviation_factor, minSizeFactor=mer_min_size_factor)
    p = mdb.models['Model-1'].parts[merged_part_name]
    # p.generateMesh()

def create_job():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    mdb.save()
    mdb.Job(name='Job-1', model='Model-1', description='', type=ANALYSIS,
        atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90,
        memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True,
        explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF,
        modelPrint=OFF, contactPrint=OFF, historyPrint=OFF,
        userSubroutine='umat_hgo.f', scratch='', resultsFormat=ODB,
        multiprocessingMode=DEFAULT, numCpus=4, numDomains=4, numGPUs=0)
    mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)
def delete_all():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    a = mdb.models['Model-1'].rootAssembly
    a.regenerate()
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=OFF, loads=ON,
        bcs=ON, predefinedFields=ON, connectors=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
        meshTechnique=OFF)
    mdb.models['Model-1'].boundaryConditions.delete(('left-pin', 'lock-all',
        'right-slider', 'top-push', 'z-control', ))
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
        predefinedFields=OFF, interactions=ON, constraints=ON,
        engineeringFeatures=ON)
    mdb.models['Model-1'].constraints.delete(('coll-membrane-to-merge',
        'ker-shell-to-merge', ))
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        step='assign_fibre_direction')
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(interactions=OFF,
        constraints=OFF, connectors=OFF, engineeringFeatures=OFF,
        adaptiveMeshConstraints=ON)
    del mdb.models['Model-1'].steps['load']
    del mdb.models['Model-1'].steps['assign_fibre_direction']
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='Initial')
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        adaptiveMeshConstraints=OFF)
    a = mdb.models['Model-1'].rootAssembly
    a.deleteFeatures(('bone-1', 'coll-block-1', 'coll-int-1', 'merged-shell-1',
        'coll-membrane-1', 'keratin-shell-1', ))
    mdb.models['Model-1'].rootAssembly.deleteSets(setNames=('all',
        'bottom-back-points', 'bottom-left-edge', 'bottom-right-edge',
        'keratin-top', ))
    mdb.models['Model-1'].rootAssembly.deleteSurfaces(surfaceNames=(
        'coll-membrane-bottom', 'keratin-shell-bottom', 'merged-top', ))
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
        engineeringFeatures=ON, mesh=OFF)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=OFF)
    p1 = mdb.models['Model-1'].parts['keratin-shell']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    del mdb.models['Model-1'].sections['bone']
    del mdb.models['Model-1'].sections['collagen-int']
    del mdb.models['Model-1'].sections['collagen-membrane']
    del mdb.models['Model-1'].sections['keratin-shell']
    del mdb.models['Model-1'].materials['bone']
    del mdb.models['Model-1'].materials['collagen']
    del mdb.models['Model-1'].materials['keratin']
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF,
        engineeringFeatures=OFF)
    p1 = mdb.models['Model-1'].parts['bone']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    del mdb.models['Model-1'].parts['bone']
    del mdb.models['Model-1'].parts['coll-block']
    del mdb.models['Model-1'].parts['coll-int']
    del mdb.models['Model-1'].parts['coll-membrane']
    del mdb.models['Model-1'].parts['keratin-shell']
    del mdb.models['Model-1'].parts['merged-shell']

def re_mesh_merged():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts['merged-shell']
    e = p.edges
    print(e)
    edges = e.findAt(((-2, 3.0, 1.75), ))
    v = p.vertices
    verts = v.findAt(((-1.31987, 3.0, 3.0), ), ((-1.31987, 3.0, 0.0), ))
    edge = p.sets['Set-3']
    # pickedEntities =(verts, edges, )
    pickedEntities =(edge, )
    p.ignoreEntity(entities=pickedEntities)

def call_set():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    a = mdb.models['Model-1'].rootAssembly
    region = a.instances['merged-shell-1'].sets['Set-3']
    mdb.models['Model-1'].DisplacementBC(name='BC-6',
        createStepName='assign_fibre_direction', region=region, u1=0.0,
        u2=UNSET, u3=UNSET, ur1=UNSET, ur2=UNSET, ur3=UNSET, amplitude=UNSET,
        fixed=OFF, distributionType=UNIFORM, fieldName='', localCsys=None)
def make_sets():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    a1 = mdb.models['Model-1'].rootAssembly
    a1.regenerate()
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=OFF, loads=ON,
        bcs=ON, predefinedFields=ON, connectors=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
        meshTechnique=OFF)
    p = mdb.models['Model-1'].parts['merged-shell']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=13.7065,
        farPlane=24.156, width=8.51996, height=4.08014, cameraPosition=(
        6.51991, 13.2951, 14.0184), cameraUpVector=(-0.522233, 0.492231,
        -0.696406), cameraTarget=(0.0027765, 1.24493, 1.50844))
    p = mdb.models['Model-1'].parts['merged-shell']
    f = p.faces
    faces = f.findAt(((0.079719, 3.0, 2.971589), ), ((-3.666878, 2.862395, 1.0), ),
        ((-0.634312, 3.0, 2.980004), ), ((0.721657, 3.0, 0.044911), ), ((
        1.613607, 2.989884, 1.0), ))
    p.Set(faces=faces, name='merged-top')


def make_surfaces():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts['merged-shell']
    s = p.faces
    side1Faces = s.findAt(((0.079719, 3.0, 2.971589), ), ((-3.666878, 2.862395,
        1.0), ), ((-0.634312, 3.0, 2.980004), ), ((0.721657, 3.0, 0.044911), ),
        ((1.613607, 2.989884, 1.0), ))
    p.Surface(side1Faces=side1Faces, name='mer')
