subroutine linear_elastic_stress(stress, strain, lambda, mu)
  double precision :: stress(6), strain(6)
  double precision :: lambda, mu
  integer :: delta
  integer :: i
  double precision :: e_kk


  e_kk = strain(1)+strain(2)+strain(3)

  do i=1,6
    ! print*, "i = ", i
    ! print*, "delta(i) = ", delta(i)
    ! print*, "e_kk = ", e_kk
    ! print*, "mu = ", mu
    ! print*, "strain(i) = ", lambda
    ! print*, "stress(i) = ", stress(i)
    stress(i) = lambda * delta(i) * e_kk + 2.0 * mu * strain(i)
    ! print*, "stress(i) = ", stress(i)
  enddo

end

subroutine calc_stress(stress, F, mu, lambda)
  double precision :: stress(6), F(3,3), strain(6), jac, stress_mat(3,3)
  double precision :: lambda, mu


  call determinant(F, jac)
  call strain_from_F(strain, F)
  call linear_elastic_stress(stress, strain, lambda, mu)
  print*, 'S', stress
  call voignt_to_matrix(stress, stress_mat)
  print*, 'S_mat'
  call print_matrix(stress_mat, 3, 3)
  print*, 'F'
  call print_matrix(F, 3, 3)
  stress_mat = matmul(F,matmul(stress_mat,transpose(F)))
  print*, 'Tau_mat'
  call print_matrix(stress_mat, 3, 3)
  call matrix_to_voignt(stress_mat, stress)
  print*, 'Tau', stress
  ! stress = stress * jac

end

program test
  double precision :: stress(6), strain(6), F(3,3), C(6,6), eps
  double precision :: lambda, mu, jac, e, nu

  ! lambda = 1
  eps = 1e-9
  ! mu = 2
  ! stress = 0
  ! strain = 0
  e = 201e9
  nu =0.33


  lambda = e*nu/(1.0+nu)/(1.0-2.0*nu)
  mu = e/2.0/(1.0+nu)

  F = 0

  ! print *, "F before"
  ! call print_matrix(F, 3, 3)

  F(1,1) = 1.1
  ! print *, "F before"
  ! call print_matrix(F, 3, 3)
  F(2,2) = 1.0
  F(3,3) = 1.0
  ! F(1,2) = 2.0
  ! F(2,1) = 2.0

  ! F(1,2) = 0.02
  ! F(2,1) = 1.0

  ! call linear_elastic_stress(stress, strain, F, lambda, mu)
  ! print *, "F before"
  ! call print_matrix(F, 3, 3)
  ! call strain_from_F(strain, F)
  ! print*, "Stress", stress

  ! print*, "Strain", strain

  call approximate_C(F, C, eps, mu, lambda)



end program test

subroutine approximate_C(F, C, eps, mu, lambda)
  double precision :: F(3, 3), jac, dF(3,3), F_hat(3,3), Id(3,3), eps, C(6,6), stress_hat(6)
  double precision :: stress(6), strain(6), J, stens(3,3), e_i(3), e_j(3), mu, lambda, err(6,6)
  integer :: i, is(6), js(6)

  C = 0
  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]

  err = 0
  do i=1,3
  err(i,i) = 2*mu
  err(i+3, i+3) = mu
 enddo

  err(1:3, 1:3) = err(1:3, 1:3) + lambda

  call determinant(F, jac)
  print*, "jac ", jac

  Id = 0
  Id(1,1) = 1
  Id(2,2) = 1
  Id(3,3) = 1

  call calc_stress(stress, F, mu, lambda)

  do i = 1,6
    print*, '***********************************************'
    print*, 'i = ', i

    e_i = Id(is(i),:)
    e_j = Id(js(i),:)

    call tensor_product_1(e_i, e_j, 3, 3, stens)
    F_hat = F + (eps/2.0) * (matmul(stens, F) + matmul(transpose(stens), F) )
    call calc_stress(stress_hat, F_hat, mu, lambda)

    C(i, :) = (1/(jac*eps)) * (stress_hat - stress)


    print*, 'stress', stress
    print*, 'stress_hat', stress_hat
    print*, 'diff', stress_hat - stress


    print*, '***********************************************'
    print*,

  enddo
  print*, "Approximate C"
  call print_matrix(C, 6, 6)

  print*, "Exact C"
  call print_matrix(err, 6, 6)

  print*, "Error %"
  err = 100*(C-err)/err
  call print_matrix(err, 6, 6)

end

subroutine strain_from_F(strain, F)
  double precision :: strain(6)
  double precision :: F(3,3), s(3,3)

  s = matmul(transpose(F), F)

  s(1,1) = s(1,1) - 1
  s(2,2) = s(2,2) - 1
  s(3,3) = s(3,3) -1

  s = s * 0.5

  ! print *, "s"
  ! call print_matrix(s, 3, 3)

  call matrix_to_voignt(s, strain)

end
