program test
  double precision :: C(6,6), C_apprx(6,6), G, D, F(3,3), k_stress(3,3), sigma(3,3), eps, det
  double precision :: k1, k2, n0(3, 1)
  integer :: delta
  double precision :: c_stress_voigt(6), start, end_x

  G = 80.0e3
  D = 0.2

  ! k1 = 1.56e3
  ! k2 = 0.4465
  k1 = 0
  k2 = 0
  ! k2 = 2.4
  n0 = 0
  n0(1,1) = 1.0

  eps = 1.0e-8

  call identity(F, 3)


  ! call stresses_and_stiffness_matrix(F, G, D, c_stress, k_stress, k1, k2, n0, C, C_apprx, eps)
  start = 0.5
  end_x = 3.0
  call  stretch_v_stress_1(F, G, D, sigma, k_stress, k1, k2, n0, start, end_x, 100)
  ! call  stretch_v_stress_2(F, G, D, sigma, k_stress, k1, k2, n0, start, end_x, 100)

end

subroutine stretch_v_stress_2(F, G, D, sigma, k_stress, k1, k2, n0, start, end_x, n_pts)
  double precision :: C(6,6), C_apprx(6,6), G, D, F(3,3), k_stress(3,3), sigma(3,3), eps, det
  double precision :: k1, k2, n0(3, 1), start, end_x, dx
  integer :: n_pts, i

  OPEN(UNIT=1,FILE="nh stress22.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
  OPEN(UNIT=2,FILE="nh stretch2.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")

  dx = (end_x - start)/(n_pts - 1)

  call identity(F, 3)
  do i=1,n_pts
    F(2, 2) = start + dx *(i-1)
    F(1, 1) = F(2, 2) ** (-0.5)
    F(3, 3) = F(2, 2) ** (-0.5)

    print*, "jacobian = ", det(F)

    call cauchy_stress(F, G, D, k1, k2, n0, sigma)
    write(unit=1, fmt=*) sigma(2, 2)
    write(unit=2, fmt=*) F(2, 2)
  enddo
  close(unit=1)
  close(unit=2)
end

subroutine stretch_v_stress_1(F, G, D, sigma, k_stress, k1, k2, n0, start, end_x, n_pts)
  double precision :: C(6,6), C_apprx(6,6), G, D, F(3,3), k_stress(3,3), sigma(3,3), eps, det
  double precision :: k1, k2, n0(3, 1), start, end_x, dx
  integer :: n_pts, i

  OPEN(UNIT=1,FILE="nh stress11_ox.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
  OPEN(UNIT=2,FILE="nh stretch1_ox.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")

  dx = (end_x - start)/(n_pts - 1)

  call identity(F, 3)
  do i=1,n_pts
    F(1, 1) = start + dx *(i-1)
    F(2, 2) = F(1, 1) ** (-0.5)
    F(3, 3) = F(1, 1) ** (-0.5)

    print*, "jacobian = ", det(F)

    call cauchy_stress(F, G, D, k1, k2, n0, sigma)
    write(unit=1, fmt=*) sigma(1, 1)
    write(unit=2, fmt=*) F(1, 1)
  enddo
  close(unit=1)
  close(unit=2)
end

subroutine stresses_and_stiffness_matrix(F, G, D, c_stress, k_stress, k1, k2, n0, C, C_apprx, eps)
  double precision :: C(6,6), C_apprx(6,6), G, D, F(3,3), k_stress(3,3), c_stress(3,3), eps, det
  double precision :: k1, k2, n0(3, 1)

  call cauchy_stress(F, G, D, k1, k2, n0, c_stress)
  call kirchhoff_stress(F, G, D, k1, k2, n0, k_stress)

  print*, "cauchy_stress"
  call print_matrix(c_stress, 3, 3)

  print*, ""

  print*, "kirchhoff_stress"
  call print_matrix(k_stress, 3, 3)

  call neo_hookean_C(F, G, D, C)
  call approximate_C(F, G, D, k1, k2, n0, C_apprx, eps)

  print*, "Stiffness matrix"
  call print_matrix(C, 6, 6)

  print*,
  print*, "Approximate Stiffness matrix"
  call print_matrix(C_apprx, 6, 6)

  print*,
  print*, "Error %"
  call print_matrix(100*(C_apprx - C)/C, 6, 6)
end

subroutine cauchy_stress(F, G, D, k1, k2, n0, sigma)
   double precision :: F(3,3), G, D, k1, k2, n0(3,1)
   double precision :: b_bar(3,3), C_bar(3,3), jacobian, I4(1, 1), psi4
   double precision :: sigma(3,3), Id(3,3)
   double precision :: det, tr, heaviside

   call identity(Id, 3)
   jacobian = det(F)
   b_bar = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))
   C_bar = matmul(transpose(F), F) * (jacobian ** (-2.0/3.0))
   I4 = matmul(transpose(n0), matmul(C_bar, n0))
   psi4 = heaviside(I4(1, 1)-1) * (I4(1, 1)-1) * k1 * exp(k2 * (I4(1, 1)-1)**2)



   sigma = (2.0/jacobian) * G * (b_bar - (1.0/3.0) *tr(b_bar) * Id) + (2.0/D)*(jacobian - 1.0) *Id
   sigma = sigma + (2.0/jacobian) * psi4 * matmul(F, matmul(matmul(n0, transpose(n0)), transpose(F)))
end

subroutine kirchhoff_stress(F, G, D, k1, k2, n0, tau)
   double precision :: F(3,3), G, D, k1, k2, n0(3,1)
   double precision :: jacobian
   double precision :: det
   double precision :: tau(3,3)

   jacobian = det(F)
   call cauchy_stress(F, G, D, k1, k2, n0, tau)
   tau = jacobian * tau
end

subroutine approximate_C(F, G, D, k1, k2, n0, C, eps)
  double precision :: F(3,3), C(6,6), G, D, eps, k1, k2, n0(3,1)
  double precision :: F_hat(3,3), k_stress(6), k_stress_hat(6), k_stress_mat(3,3), jacobian
  double precision :: Id(3,3), det, es(3, 3, 1)
  integer :: is(6), js(6), i, i_mat, j_mat

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]
  call identity(Id, 3)
  jacobian = det(F)

  es = 0
  es(1, 1, 1) = 1
  es(2, 2, 1) = 1
  es(3, 3, 1) = 1


  call kirchhoff_stress(F, G, D, k1, k2, n0, k_stress_mat)
  call matrix_to_voignt(k_stress_mat, k_stress)

  do i=1,6
    i_mat = is(i)
    j_mat = js(i)

    ! print*, 'i, j: ', i_mat, ', ', j_mat
    ! print*, "ei tens ej"
    ! call print_matrix(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), 3, 3)
    ! print*, "ej tens ei"
    ! call print_matrix(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), 3, 3)

    F_hat = F + (eps/2.0)*(matmul(matmul(es(i_mat, :, :), transpose(es(j_mat, :, :))), F) &
            + matmul(matmul(es(j_mat, :, :), transpose(es(i_mat, :, :))), F))

    call kirchhoff_stress(F_hat, G, D, k1, k2, n0, k_stress_mat)
    call matrix_to_voignt(k_stress_mat, k_stress_hat)

    C(:, i) = (1.0/(jacobian * eps)) * (k_stress_hat - k_stress)
  enddo
end

subroutine neo_hookean_C(F, G, D, C)
  double precision :: F(3, 3), G, D
  integer :: i, j, k, l
  integer :: ci, cj
  integer :: is(6), js(6)
  double precision :: b_bar(3,3), jacobian
  double precision :: det, tr, neo_hookean_C_ijkl
  double precision :: C(6,6)

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]

  jacobian = det(F)
  b_bar = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))

  C = 0

  do ci = 1,6
    do cj=1,6
      i = is(ci)
      j = js(ci)
      k = is(cj)
      l = js(cj)

      C(ci, cj) = neo_hookean_C_ijkl(b_bar, jacobian, G, D, i, j, k, l)
    enddo
  enddo
end

function neo_hookean_C_ijkl(b_bar, jacobian, G, D, i, j, k, l) result(c)
   double precision :: b_bar(3,3), jacobian, G, D
   integer :: i, j, k, l
   integer :: delta
   double precision :: tr
   double precision :: c

   c = (2.0/jacobian) * G * (0.5 * (delta(i,k)*b_bar(j,l) + b_bar(i,k)*delta(j,l) + delta(i,l)*b_bar(j,k) + b_bar(i,l)*delta(j,k)) &
       - (2.0/3.0) * (delta(i,j)*b_bar(k,l) + b_bar(i, j) * delta(k,l)) + (2.0/9.0)*delta(i,j)*delta(k,l)*tr(b_bar)) &
       + (2.0/D)*(2*jacobian-1)*delta(i,j)*delta(k,l)
end function neo_hookean_C_ijkl

function delta(i, j) result(d)
   integer, intent(in) :: i! input
   integer :: d
   ! integer             :: j ! output
   if (i==j)then
     d = 1
   else
     d = 0
   endif
end function delta

function det(mat) result(d)
  double precision, intent(in) :: mat(3,3)
  double precision :: d

  d = mat(1,1) * (mat(2,2)*mat(3,3) - mat(2, 3) * mat(3,2))
  d = d - mat(1,2) * (mat(2,1)*mat(3,3) - mat(2, 3) * mat(3,1))
  d = d + mat(1,3) * (mat(2,1)*mat(3,2) - mat(2, 2) * mat(3,1))
end function det

function tr(mat) result(d)
  double precision, intent(in) :: mat(3,3)
  double precision :: d

  d = mat(1,1) + mat(2,2) + mat(3,3)
end function tr

function heaviside(x) result(d)
  double precision, intent(in) :: x
  double precision :: d, a

  a= 1.0
  d = 0.5*(sign(a,x)+1.0)
end function heaviside

subroutine identity(in_mat, n)
  integer :: n, i
  double precision :: in_mat(n,n)
  in_mat = 0
  do i = 1,n
      in_mat(i,i) = 1.0
  enddo
end

subroutine matrix_to_voignt(in, out)
  double precision :: in(3,3), out(6)
  out(1) = in(1,1)
  out(2) = in(2,2)
  out(3) = in(3,3)

  out(4) = in(2,3)
  out(5) = in(1,3)
  out(6) = in(1,2)
end

subroutine print_matrix(mat, rows, collumns)
  integer :: rows, collumns
  double precision :: mat(rows, collumns)
  integer :: i, j

  do i =1,rows
      print*,mat(i,:)
  enddo
end

subroutine print_vector(mat, rows)
  integer :: rows
  double precision :: mat(rows)
  integer :: i

  do i =1,rows
      print*,mat(i)
  enddo
end
