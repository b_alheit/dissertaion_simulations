SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD, &
  RPL,DDSDDT,DRPLDE,DRPLDT, &
  STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,CMNAME, &
  NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT, &
  CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,JSTEP,KINC)

INCLUDE 'ABA_PARAM.INC'

  CHARACTER*80 CMNAME

  DIMENSION STRESS(NTENS),STATEV(NSTATV), &
  DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS), &
  STRAN(NTENS),DSTRAN(NTENS),TIME(2),PREDEF(1),DPRED(1), &
  PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3), &
  JSTEP(4)
  double precision :: eps

!		     ELASTIC USER SUBROUTINE
  PARAMETER (ONE=1.0D0, TWO=2.0D0)
  E=PROPS(1)
  ANU=PROPS(2)
  eps = 1.0e-8

  ALAMBDA=E*ANU/(ONE+ANU)/(ONE-TWO*ANU)
  AMU=E/TWO/(ONE+ANU)
  DO I=1,NTENS
    DO J=1,NTENS
      DDSDDE(I,J)=0.0D0
    ENDDO
  ENDDO

  call approximate_C(DFGRD0, DDSDDE, eps, AMU, ALAMBDA)

  call linear_elastic_stress(STRESS, STRAN+DSTRAN, alambda, amu)

  RETURN
  END

  subroutine linear_elastic_stress(stress, strain, lambda, mu)
    double precision :: stress(6), strain(6)
    double precision :: lambda, mu
    integer :: delta
    integer :: i
    double precision :: e_kk


    e_kk = strain(1)+strain(2)+strain(3)

    do i=1,6
      ! print*, "i = ", i
      ! print*, "delta(i) = ", delta(i)
      ! print*, "e_kk = ", e_kk
      ! print*, "mu = ", mu
      ! print*, "strain(i) = ", lambda
      ! print*, "stress(i) = ", stress(i)
      stress(i) = lambda * delta(i) * e_kk + 2.0 * mu * strain(i)
      ! print*, "stress(i) = ", stress(i)
    enddo

  end

  subroutine calc_stress(stress, F, mu, lambda)
    double precision :: stress(6), F(3,3), strain(6), jac, stress_mat(3,3)
    double precision :: lambda, mu


    call determinant(F, jac)
    call strain_from_F(strain, F)
    call linear_elastic_stress(stress, strain, lambda, mu)
    print*, 'S', stress
    call voignt_to_matrix(stress, stress_mat)
    print*, 'S_mat'
    call print_matrix(stress_mat, 3, 3)
    print*, 'F'
    call print_matrix(F, 3, 3)
    stress_mat = matmul(F,matmul(stress_mat,transpose(F)))
    print*, 'Tau_mat'
    call print_matrix(stress_mat, 3, 3)
    call matrix_to_voignt(stress_mat, stress)
    print*, 'Tau', stress
    ! stress = stress * jac

  end

  program test
    double precision :: stress(6), strain(6), F(3,3), C(6,6), eps
    double precision :: lambda, mu, jac, e, nu

    ! lambda = 1
    eps = 1e-9
    ! mu = 2
    ! stress = 0
    ! strain = 0
    e = 201e9
    nu =0.33


    lambda = e*nu/(1.0+nu)/(1.0-2.0*nu)
    mu = e/2.0/(1.0+nu)

    F = 0

    ! print *, "F before"
    ! call print_matrix(F, 3, 3)

    F(1,1) = 1.1
    ! print *, "F before"
    ! call print_matrix(F, 3, 3)
    F(2,2) = 1.0
    F(3,3) = 1.0
    ! F(1,2) = 2.0
    ! F(2,1) = 2.0

    ! F(1,2) = 0.02
    ! F(2,1) = 1.0

    ! call linear_elastic_stress(stress, strain, F, lambda, mu)
    ! print *, "F before"
    ! call print_matrix(F, 3, 3)
    ! call strain_from_F(strain, F)
    ! print*, "Stress", stress

    ! print*, "Strain", strain

    call approximate_C(F, C, eps, mu, lambda)



  end program test

  subroutine approximate_C(F, C, eps, mu, lambda)
    double precision :: F(3, 3), jac, dF(3,3), F_hat(3,3), Id(3,3), eps, C(6,6), stress_hat(6)
    double precision :: stress(6), strain(6), J, stens(3,3), e_i(3), e_j(3), mu, lambda, err(6,6)
    integer :: i, is(6), js(6)

    C = 0
    is = [1, 2, 3, 2, 1, 1]
    js = [1, 2, 3, 3, 3, 2]

    err = 0
    do i=1,3
    err(i,i) = 2*mu
    err(i+3, i+3) = mu
   enddo

    err(1:3, 1:3) = err(1:3, 1:3) + lambda

    call determinant(F, jac)
    print*, "jac ", jac

    Id = 0
    Id(1,1) = 1
    Id(2,2) = 1
    Id(3,3) = 1

    call calc_stress(stress, F, mu, lambda)

    do i = 1,6
      print*, '***********************************************'
      print*, 'i = ', i

      e_i = Id(is(i),:)
      e_j = Id(js(i),:)

      call tensor_product_1(e_i, e_j, 3, 3, stens)
      F_hat = F + (eps/2.0) * (matmul(stens, F) + matmul(transpose(stens), F) )
      call calc_stress(stress_hat, F_hat, mu, lambda)

      C(:, i) = (1/(jac*eps)) * (stress_hat - stress)


      print*, 'stress', stress
      print*, 'stress_hat', stress_hat
      print*, 'diff', stress_hat - stress


      print*, '***********************************************'
      print*,

    enddo
    print*, "Approximate C"
    call print_matrix(C, 6, 6)

    print*, "Exact C"
    call print_matrix(err, 6, 6)

    print*, "Error %"
    err = 100*(C-err)/err
    call print_matrix(err, 6, 6)

  end

  subroutine strain_from_F(strain, F)
    double precision :: strain(6)
    double precision :: F(3,3), s(3,3)

    s = matmul(transpose(F), F)

    s(1,1) = s(1,1) - 1
    s(2,2) = s(2,2) - 1
    s(3,3) = s(3,3) -1

    s = s * 0.5

    ! print *, "s"
    ! call print_matrix(s, 3, 3)

    call matrix_to_voignt(s, strain)

  end



  subroutine  mult_matricies(mat_A, mat_B, a_rows, a_cols_b_rows, b_cols, out)
    integer :: a_rows, a_cols_b_rows, b_cols
    real:: mat_A(a_rows, a_cols_b_rows), mat_B(a_cols_b_rows, b_cols)
    real, dimension(a_rows,b_cols) :: out
    integer :: i,j,k

    out = 0

    do i = 1,a_rows
      do j = 1,a_cols_b_rows
        do k = 1,b_cols
          out(i,k) = out(i,k) + mat_A(i,j) * mat_B(j,k)
        enddo
      enddo
    enddo
  end

  subroutine mult(a, b, out)
    real :: a(3,3), b(3,3), out(3,3)
    call mult_matricies(a, b, 3, 3, 3, out)
  end

  subroutine matrix_to_voignt(in, out)
    double precision :: in(3,3), out(6)
    out(1) = in(1,1)
    out(2) = in(2,2)
    out(3) = in(3,3)

    out(4) = in(2,3)
    out(5) = in(1,3)
    out(6) = in(1,2)

  end

  subroutine voignt_to_matrix(in, out)
    double precision :: in(6), out(3, 3)

    out(1,1) = in(1)
    out(2,2) = in(2)
    out(3,3) = in(3)

    out(2,3) = in(4)
    out(1,3) = in(5)
    out(1,2) = in(6)

    out(3,2) = in(4)
    out(3,1) = in(5)
    out(2,1) = in(6)

  end


  subroutine determinant(mat, det)
    DOUBLE PRECISION :: mat(3,3), det
    det = mat(1,1) * (mat(2,2)*mat(3,3) - mat(2, 3) * mat(3,2))
    det = det - mat(1,2) * (mat(2,1)*mat(3,3) - mat(2, 3) * mat(3,1))
    det = det + mat(1,3) * (mat(2,1)*mat(3,2) - mat(2, 2) * mat(3,1))
  end

  subroutine  tensor_product_2(mat_A, mat_B, len_a, len_b, out)
    integer :: len_a, len_b
    real:: mat_A(len_a, 1), mat_B(len_b, 1)
    real, dimension(len_a,len_b) :: out
    integer :: i,j

    do i = 1,len_a
      do j = 1,len_b
          out(i,j) = mat_A(i,1) * mat_B(j,1)
      enddo
    enddo

  end

  subroutine  tensor_product_1(mat_A, mat_B, len_a, len_b, out)
    integer :: len_a, len_b
    double precision:: mat_A(len_a), mat_B(len_b)
    double precision, dimension(len_a,len_b) :: out
    integer :: i,j

    do i = 1,len_a
      do j = 1,len_b
          out(i,j) = mat_A(i) * mat_B(j)
      enddo
    enddo

  end

  function delta(i) result(d)
     integer, intent(in) :: i! input
     integer :: d
     ! integer             :: j ! output
     if (i<=3)then
       d = 1
     else
       d = 0
     endif
  end function delta

  subroutine transpose(in, out)
    double precision :: in(3,3), out(3,3)
    integer :: i, j

    do i =1,3
      do j = 1,3
        out(i,j) = in(j,i)
      enddo
    enddo

  end

  subroutine print_matrix(mat, rows, collumns)
    integer :: rows, collumns
    double precision :: mat(rows, collumns)
    integer :: i, j

    do i =1,rows
        print*,mat(i,:)
    enddo

  end

  subroutine print_vector(mat, rows)
    integer :: rows
    double precision :: mat(rows)
    integer :: i

    do i =1,rows
        print*,mat(i)
    enddo

  end
