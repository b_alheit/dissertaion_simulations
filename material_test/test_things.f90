program test
  double precision :: n(3), C(3, 3)

  C = 0
  C(1, 1) = 1
  C(2, 2) = 1
  C(3, 3) = 1

  n = 0
  n(1) = 1.0

  print*, 'matrix_mult', (n,n)
end
