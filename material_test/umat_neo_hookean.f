SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD, &
  RPL,DDSDDT,DRPLDE,DRPLDT, &
  STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,CMNAME, &
  NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT, &
  CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,JSTEP,KINC)

INCLUDE 'ABA_PARAM.INC'

  CHARACTER*80 CMNAME

  DIMENSION STRESS(NTENS),STATEV(NSTATV), &
  DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS), &
  STRAN(NTENS),DSTRAN(NTENS),TIME(2),PREDEF(1),DPRED(1), &
  PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3), &
  JSTEP(4)

  double precision :: c_stress(3,3)
!		     ELASTIC USER SUBROUTINE
  PARAMETER (ONE=1.0D0, TWO=2.0D0)
  G=PROPS(1)
  D=PROPS(2)

  call cauchy_stress(DFGRD1, G, D, c_stress)
  call matrix_to_voignt(c_stress, STRESS)

  call neo_hookean_C(DFGRD1, G, D, DDSDDE)

  RETURN
  END


subroutine cauchy_stress(F, G, D, s)
   double precision :: F(3,3), G, D
   double precision :: b_bar(3,3), jacobian
   double precision :: s(3,3), Id(3,3)
   double precision :: det, tr

   call identity(Id, 3)
   jacobian = det(F)
   b_bar = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))
   print*, "b_bar"
   call print_matrix(b_bar, 3, 3)
   print*,
   print*,"jacobian = ", jacobian

   s = (2.0/jacobian) * G * (b_bar - (1.0/3.0) *tr(b_bar) * Id) + (2.0/D)*(jacobian - 1.0) *Id
end

subroutine kirchhoff_stress(F, G, D, s)
   double precision :: F(3,3), G, D
   double precision :: jacobian
   double precision :: det
   double precision :: s(3,3)

   jacobian = det(F)
   call cauchy_stress(F, G, D, s)
   s = jacobian * s
end

subroutine neo_hookean_C(F, G, D, C)
  double precision :: F(3, 3), G, D
  integer :: i, j, k, l
  integer :: ci, cj
  integer :: is(6), js(6)
  double precision :: b_bar(3,3), jacobian
  double precision :: det, tr, neo_hookean_C_ijkl
  double precision :: C(6,6)

  is = [1, 2, 3, 2, 1, 1]
  js = [1, 2, 3, 3, 3, 2]

  jacobian = det(F)
  b_bar = matmul(F, transpose(F)) * (jacobian ** (-2.0/3.0))

  C = 0

  do ci = 1,6
    do cj=1,6
      i = is(ci)
      j = js(ci)
      k = is(cj)
      l = js(cj)

      C(ci, cj) = neo_hookean_C_ijkl(b_bar, jacobian, G, D, i, j, k, l)
    enddo
  enddo
end

function neo_hookean_C_ijkl(b_bar, jacobian, G, D, i, j, k, l) result(c)
   double precision :: b_bar(3,3), jacobian, G, D
   integer :: i, j, k, l
   integer :: delta
   double precision :: tr
   double precision :: c

   c = (2.0/jacobian) * G * (0.5 * (delta(i,k)*b_bar(j,l) + b_bar(i,k)*delta(j,l) + delta(i,l)*b_bar(j,k) + b_bar(i,l)*delta(j,k)) &
       - (2.0/3.0) * (delta(i,j)*b_bar(k,l) + b_bar(i, j) * delta(k,l)) + (2.0/9.0)*delta(i,j)*delta(k,l)*tr(b_bar)) &
       + (2.0/D)*(2*jacobian-1)*delta(i,j)*delta(k,l)
end function neo_hookean_C_ijkl

function delta(i, j) result(d)
   integer, intent(in) :: i! input
   integer :: d
   ! integer             :: j ! output
   if (i==j)then
     d = 1
   else
     d = 0
   endif
end function delta

function det(mat) result(d)
  double precision, intent(in) :: mat(3,3)
  double precision :: d

  d = mat(1,1) * (mat(2,2)*mat(3,3) - mat(2, 3) * mat(3,2))
  d = d - mat(1,2) * (mat(2,1)*mat(3,3) - mat(2, 3) * mat(3,1))
  d = d + mat(1,3) * (mat(2,1)*mat(3,2) - mat(2, 2) * mat(3,1))
end function det

function tr(mat) result(d)
  double precision, intent(in) :: mat(3,3)
  double precision :: d

  d = mat(1,1) + mat(2,2) + mat(3,3)
end function tr

subroutine identity(in_mat, n)
  integer :: n, i
  double precision :: in_mat(n,n)
  in_mat = 0
  do i = 1,n
      in_mat(i,i) = 1.0
  enddo
end

subroutine matrix_to_voignt(in, out)
  double precision :: in(3,3), out(6)
  out(1) = in(1,1)
  out(2) = in(2,2)
  out(3) = in(3,3)

  out(4) = in(2,3)
  out(5) = in(1,3)
  out(6) = in(1,2)
end

subroutine voignt_to_matrix(in, out)
  double precision :: in(6), out(3, 3)

  out(1,1) = in(1)
  out(2,2) = in(2)
  out(3,3) = in(3)

  out(2,3) = in(4)
  out(1,3) = in(5)
  out(1,2) = in(6)

  out(3,2) = in(4)
  out(3,1) = in(5)
  out(2,1) = in(6)
end


subroutine print_matrix(mat, rows, collumns)
  integer :: rows, collumns
  double precision :: mat(rows, collumns)
  integer :: i, j

  do i =1,rows
      print*,mat(i,:)
  enddo
end

subroutine print_vector(mat, rows)
  integer :: rows
  double precision :: mat(rows)
  integer :: i

  do i =1,rows
      print*,mat(i)
  enddo
end
