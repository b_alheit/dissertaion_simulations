
subroutine  mult_matricies(mat_A, mat_B, a_rows, a_cols_b_rows, b_cols, out)
  integer :: a_rows, a_cols_b_rows, b_cols
  real:: mat_A(a_rows, a_cols_b_rows), mat_B(a_cols_b_rows, b_cols)
  real, dimension(a_rows,b_cols) :: out
  integer :: i,j,k

  out = 0

  do i = 1,a_rows
    do j = 1,a_cols_b_rows
      do k = 1,b_cols
        out(i,k) = out(i,k) + mat_A(i,j) * mat_B(j,k)
      enddo
    enddo
  enddo
end

subroutine mult(a, b, out)
  real :: a(3,3), b(3,3), out(3,3)
  call mult_matricies(a, b, 3, 3, 3, out)
end

subroutine matrix_to_voignt(in, out)
  double precision :: in(3,3), out(6)
  out(1) = in(1,1)
  out(2) = in(2,2)
  out(3) = in(3,3)

  out(4) = in(2,3)
  out(5) = in(1,3)
  out(6) = in(1,2)

end

subroutine voignt_to_matrix(in, out)
  double precision :: in(6), out(3, 3)

  out(1,1) = in(1)
  out(2,2) = in(2)
  out(3,3) = in(3)

  out(2,3) = in(4)
  out(1,3) = in(5)
  out(1,2) = in(6)

  out(3,2) = in(4)
  out(3,1) = in(5)
  out(2,1) = in(6)

end


subroutine determinant(mat, det)
  DOUBLE PRECISION :: mat(3,3), det
  det = mat(1,1) * (mat(2,2)*mat(3,3) - mat(2, 3) * mat(3,2))
  det = det - mat(1,2) * (mat(2,1)*mat(3,3) - mat(2, 3) * mat(3,1))
  det = det + mat(1,3) * (mat(2,1)*mat(3,2) - mat(2, 2) * mat(3,1))
end

subroutine  tensor_product_2(mat_A, mat_B, len_a, len_b, out)
  integer :: len_a, len_b
  real:: mat_A(len_a, 1), mat_B(len_b, 1)
  real, dimension(len_a,len_b) :: out
  integer :: i,j

  do i = 1,len_a
    do j = 1,len_b
        out(i,j) = mat_A(i,1) * mat_B(j,1)
    enddo
  enddo

end

subroutine  tensor_product_1(mat_A, mat_B, len_a, len_b, out)
  integer :: len_a, len_b
  double precision:: mat_A(len_a), mat_B(len_b)
  double precision, dimension(len_a,len_b) :: out
  integer :: i,j

  do i = 1,len_a
    do j = 1,len_b
        out(i,j) = mat_A(i) * mat_B(j)
    enddo
  enddo

end

function delta(i) result(d)
   integer, intent(in) :: i! input
   integer :: d
   ! integer             :: j ! output
   if (i<=3)then
     d = 1
   else
     d = 0
   endif
end function delta

subroutine transpose(in, out)
  double precision :: in(3,3), out(3,3)
  integer :: i, j

  do i =1,3
    do j = 1,3
      out(i,j) = in(j,i)
    enddo
  enddo

end

subroutine print_matrix(mat, rows, collumns)
  integer :: rows, collumns
  double precision :: mat(rows, collumns)
  integer :: i, j

  do i =1,rows
      print*,mat(i,:)
  enddo
end

subroutine print_vector(mat, rows)
  integer :: rows
  double precision :: mat(rows)
  integer :: i

  do i =1,rows
      print*,mat(i)
  enddo
end
