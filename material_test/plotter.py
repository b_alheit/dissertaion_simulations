import numpy as np
import matplotlib.pyplot as plt

title = 'Stress vs strech in the same direction'
x_axis = '$\lambda_i$'
y_axis = '$\sigma_{ii}$'

plots = [('nh stretch1_ox.txt', 'nh stress11_ox.txt', "Direction 1 - no compression", 3, 'blue')]
         # ('nh stretch1.txt', 'nh stress11.txt', "Direction 1 - no compression", 3, 'blue'),
         # ('nh stretch1i.txt', 'nh stress11i.txt', "Direction 1 - isotropic", 3, 'green'),
         # ('nh stretch2.txt', 'nh stress22.txt', "Direction 2", 1.5, 'red'),
         # ('nh stretch1c.txt', 'nh stress11c.txt', "Direction 1 - compression", 1.5, 'orange')]


def load_data(file_name):
    data = np.loadtxt(fname=file_name,
                        dtype=str,
                        delimiter='\n ')
    return data[data != ''].astype(np.float)

for i in range(len(plots)):
    x = load_data(plots[i][0])
    y = load_data(plots[i][1])/1000
    name = plots[i][2]
    plt.plot(x, y, label=name, linewidth=plots[i][3], color=plots[i][4])

plt.xlabel(x_axis)
plt.ylabel(y_axis)
plt.title(title)
plt.legend()
plt.grid()
plt.show()
